#version 430

in vec2 tc;

layout (binding=0) uniform sampler2D samp;

out vec4 color;

void main(void) {
	color = texture(samp, tc);
}
