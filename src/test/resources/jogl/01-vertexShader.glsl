#version 430

uniform float offset;

mat4 createRotZ(float rad) {
    mat4 rotz = mat4(cos(rad), -sin(rad), 0.0, 0.0,
        sin(rad), cos(rad), 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0);
    return rotz;
}

void main(void) {
    mat4 rotz = createRotZ(0.3);
    if (gl_VertexID == 0) gl_Position = rotz * vec4(0.25 + offset, -0.25, 0.0, 1.0);
    else if (gl_VertexID == 1) gl_Position = rotz * vec4(-0.25 + offset, -0.25, 0.0, 1.0);
    else if (gl_VertexID == 2) gl_Position = rotz * vec4(0 + offset, 0.25, 0.0, 1.0);
}
