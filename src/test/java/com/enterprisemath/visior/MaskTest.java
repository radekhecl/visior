package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.ToStringBuilder;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Mask test case.
 *
 * @author radek.hecl
 */
public class MaskTest {

    /**
     * Creates new instance.
     */
    public MaskTest() {
    }

    /**
     * Tests pixel retrieval.
     */
    @Test
    public void testGet() {
        Mask mask = Mask.create(3, 2,
                true, false, true,
                true, false, false);

        assertEquals(true, mask.get(0, 0));
        assertEquals(false, mask.get(1, 0));
        assertEquals(true, mask.get(2, 0));
        assertEquals(true, mask.get(0, 1));
        assertEquals(false, mask.get(1, 1));
        assertEquals(false, mask.get(2, 1));

        assertEquals(true, mask.get(0));
        assertEquals(false, mask.get(1));
        assertEquals(true, mask.get(2));
        assertEquals(true, mask.get(3));
        assertEquals(false, mask.get(4));
        assertEquals(false, mask.get(5));
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
