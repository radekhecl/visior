package com.enterprisemath.visior;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Matrix test case.
 *
 * @author radek.hecl
 */
public class Matrix33Test {

    /**
     * Creates new instance.
     */
    public Matrix33Test() {
    }

    /**
     * Tests determinant.
     */
    @Test
    public void testDet() {
        assertEquals(Float.valueOf(74f), Float.valueOf(Matrix33.create(1, 5, 3, 2, 4, 7, 4, 6, 2).det()));
    }

    /**
     * Tests inversion matrix.
     */
    @Test
    public void testInv() {
        Matrix33 input = null;
        Matrix33 expected = null;

        input = Matrix33.create(1, 2, 3, 0, 1, 4, 5, 6, 0);
        expected = Matrix33.create(-24, 18, 5, 20, -15, -4, -5, 4, 1);
        assertEquals(expected, input.inv());
    }

    /**
     * Tests multiplication.
     */
    @Test
    public void testMul() {
        Matrix33 a = null;
        Matrix33 b = null;
        Matrix33 expected = null;

        a = Matrix33.create(4, 6, 0, 2, 1, 4, 6, 9, 3);
        b = Matrix33.create(1, 5, 8, 4, 3, 2, 7, 6, 5);
        expected = Matrix33.create(28, 38, 44, 34, 37, 38, 63, 75, 81);
        assertEquals(expected, a.mul(b));
    }

    /**
     * Tests multiplication with 3 times 4 matrix.
     */
    @Test
    public void testMul_34() {
        Matrix33 a = null;
        Matrix34 b = null;
        Matrix34 expected = null;

        a = Matrix33.create(
                4, 6, 0,
                2, 1, 4,
                6, 9, 3);
        b = Matrix34.create(
                1, 5, 8, 3,
                4, 3, 2, 4,
                7, 6, 5, 2);
        expected = Matrix34.create(
                28, 38, 44, 36,
                34, 37, 38, 18,
                63, 75, 81, 60);
        assertEquals(expected, a.mul(b));
    }

}
