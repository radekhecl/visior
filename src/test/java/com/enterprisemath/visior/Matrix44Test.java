package com.enterprisemath.visior;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Matrix test case.
 *
 * @author radek.hecl
 */
public class Matrix44Test {

    /**
     * Creates new instance.
     */
    public Matrix44Test() {
    }

    /**
     * Tests vector multiplication.
     */
    @Test
    public void testMul_vec() {
        Matrix44 mat = Matrix44.create(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 16);

        assertEquals(Vector4.create(50, 114, 178, 242), mat.mul(Vector4.create(1, 3, 5, 7)));
    }

    /**
     * Tests multiplication.
     */
    @Test
    public void testMul() {
        Matrix44 a = null;
        Matrix44 b = null;
        Matrix44 expected = null;

        a = Matrix44.create(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 16);
        b = Matrix44.create(
                7, 12, 3, 1,
                2, 5, 16, 4,
                8, 14, 13, 9,
                10, 11, 15, 6);
        expected = Matrix44.create(
                75, 108, 134, 60,
                183, 276, 322, 140,
                291, 444, 510, 220,
                399, 612, 698, 300);
        assertEquals(expected, a.mul(b));
    }

    /**
     * Tests conversion to buffer.
     */
    @Test
    public void testToBufCol() {
        Matrix44 mat = Matrix44.create(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12,
                13, 14, 15, 16);
        float[] res = new float[16];
        float[] expected = {1, 5, 9, 13, 2, 6, 10, 14, 3, 7, 11, 15, 4, 8, 12, 16};
        assertArrayEquals(expected, mat.toBufCol(res), 0.0f);
    }

}
