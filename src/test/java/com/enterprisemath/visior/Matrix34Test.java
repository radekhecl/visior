package com.enterprisemath.visior;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Matrix test case.
 *
 * @author radek.hecl
 */
public class Matrix34Test {

    /**
     * Creates new instance.
     */
    public Matrix34Test() {
    }

    /**
     * Tests vector multiplication.
     */
    @Test
    public void testMul_vec() {
        Matrix34 mat = Matrix34.create(
                1, 2, 3, 4,
                5, 6, 7, 8,
                9, 10, 11, 12);

        assertEquals(Vector3.create(50, 114, 178), mat.mul(Vector4.create(1, 3, 5, 7)));
    }

}
