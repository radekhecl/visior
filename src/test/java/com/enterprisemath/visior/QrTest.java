package com.enterprisemath.visior;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Tests utilities for working with QR codes.
 *
 * @author radek.hecl
 */
public class QrTest {

    /**
     * Creates new instance.
     */
    public QrTest() {
    }

    /**
     * Tests QR code detection.
     */
    @Test
    public void testDetect() {
        List<Contour2> expected = null;
        List<Contour2> actual = null;

        // tests for 1 QR code on the image
        expected = Arrays.asList(Contour2.create(Vector2.create(111, 292), Vector2.create(152, 471), Vector2.create(331, 427), Vector2.create(289, 250)));
        actual = processFile("src/test/resources/qr/qr-single-01.jpg");
        assertMatch(expected, actual, 6f);

        expected = Arrays.asList(Contour2.create(Vector2.create(237, 318), Vector2.create(390, 336), Vector2.create(400, 160), Vector2.create(252, 164)));
        actual = processFile("src/test/resources/qr/qr-single-02.jpg");
        assertMatch(expected, actual, 3f);

        expected = Arrays.asList(Contour2.create(Vector2.create(390, 197), Vector2.create(249, 214), Vector2.create(257, 322), Vector2.create(378, 303)));
        actual = processFile("src/test/resources/qr/qr-single-03.jpg");
        assertMatch(expected, actual, 3f);

        expected = Arrays.asList(Contour2.create(Vector2.create(181, 333), Vector2.create(173, 523), Vector2.create(347, 466), Vector2.create(381, 305)));
        actual = processFile("src/test/resources/qr/qr-single-04.jpg");
        assertMatch(expected, actual, 4f);

        expected = Arrays.asList(Contour2.create(Vector2.create(235, 132), Vector2.create(274, 256), Vector2.create(403, 210), Vector2.create(358, 87)));
        actual = processFile("src/test/resources/qr/qr-single-05.jpg");
        assertMatch(expected, actual, 4f);

        expected = Arrays.asList(Contour2.create(Vector2.create(352, 445), Vector2.create(318, 369), Vector2.create(243, 394), Vector2.create(271, 474)));
        actual = processFile("src/test/resources/qr/qr-single-06.jpg");
        assertMatch(expected, actual, 5f);

        expected = Arrays.asList(Contour2.create(Vector2.create(511, 246), Vector2.create(409, 245), Vector2.create(406, 343), Vector2.create(508, 355)));
        actual = processFile("src/test/resources/qr/qr-single-07.jpg");
        assertMatch(expected, actual, 3f);

//        expected = Arrays.asList(Contour2.create(Vector2.create(306, 318), Vector2.create(257, 399), Vector2.create(357, 442), Vector2.create(420, 367)));
//        actual = processFile("src/test/resources/qr/qr-single-08.jpg");
//        assertMatch(expected, actual, 3f);
        expected = Arrays.asList(Contour2.create(Vector2.create(214, 227), Vector2.create(210, 296), Vector2.create(280, 328), Vector2.create(296, 260)));
        actual = processFile("src/test/resources/qr/qr-single-09.jpg");
        assertMatch(expected, actual, 3f);

        expected = Arrays.asList(Contour2.create(Vector2.create(290, 310), Vector2.create(319, 237), Vector2.create(241, 202), Vector2.create(203, 272)));
        actual = processFile("src/test/resources/qr/qr-single-10.jpg");
        assertMatch(expected, actual, 3f);

        // tests that no QR in the image is ok
        actual = processFile("src/test/resources/qr/qr-zero-01.jpg");
        assertTrue(actual.isEmpty());

        actual = processFile("src/test/resources/qr/qr-zero-02.jpg");
        assertTrue(actual.isEmpty());

        actual = processFile("src/test/resources/qr/qr-zero-03.jpg");
        assertTrue(actual.isEmpty());

        actual = processFile("src/test/resources/qr/qr-zero-04.jpg");
        assertTrue(actual.isEmpty());
    }

    /**
     * Loads the masks.
     *
     * @param file path to the file
     * @return detected QR codes
     */
    private List<Contour2> processFile(String file) {
        Matrix hgauss = Matrix.gausHor(3, 1);
        Matrix vgauss = Matrix.gausVert(3, 1);
        VolumeBuffer input = Images.load(file);
        PlaneBuffer avg = PlaneBuffer.create(input.getWidth(), input.getHeight());
        PlaneBuffer smooth1 = PlaneBuffer.create(input.getWidth(), input.getHeight());
        PlaneBuffer smooth2 = PlaneBuffer.create(input.getWidth(), input.getHeight());
        MaskBuffer mask = MaskBuffer.create(input.getWidth(), input.getHeight());
        Operators.avg(input, avg);
        Operators.conv(avg, hgauss, BorderType.REFLECT, smooth1);
        Operators.conv(avg, vgauss, BorderType.REFLECT, smooth2);
        Operators.thres(smooth2, 0.35f, mask);
        return Qr.detect(mask);
    }

    /**
     * Asserts 2 shapes to be similar.
     *
     * @param expected expected shape
     * @param actual actual shape
     * @param pointTolerance tolerance per point
     */
    private void assertMatch(List<Contour2> expected, List<Contour2> actual, float pointTolerance) {
        assertEquals("Number of shapes doesn't match: expected = " + expected + "; actual = " + actual, expected.size(), actual.size());
        for (int s = 0; s < expected.size(); ++s) {
            Contour2 es = expected.get(s);
            Contour2 as = actual.get(s);
            assertEquals("Number of points in shape #" + s + " doesn't match: expected = " + expected + "; actual = " + actual, es.getNumPoints(), as.getNumPoints());
            for (int i = 0; i < es.getNumPoints(); ++i) {
                assertTrue("Points #" + i + " in shape #" + s + " are too far from each other: expected = " + expected + "; actual = " + actual,
                        es.getPoint(i).dist(as.getPoint(i)) <= pointTolerance);
            }
        }
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
