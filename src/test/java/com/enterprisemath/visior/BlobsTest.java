package com.enterprisemath.visior;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test case for blobs.
 *
 * @author radek.hecl
 */
public class BlobsTest {

    /**
     * Creates new instance.
     */
    public BlobsTest() {
    }

    /**
     * Tests blob-4 extraction.
     */
    @Test
    public void testBlob4() {
        MaskBuffer mask = null;
        PlaneBuffer res = null;
        Plane expected = null;

        mask = MaskBuffer.create(2, 2);
        mask.setAll(false, false,
                false, false);
        expected = Plane.create(2, 2,
                -1, -1,
                -1, -1);
        res = PlaneBuffer.create(2, 2);
        assertEquals(0, Blobs.blob4(mask, res));
        assertEquals(expected, res.toPlane());

        mask = MaskBuffer.create(2, 2);
        mask.setAll(false, false,
                false, true);
        expected = Plane.create(2, 2,
                -1, -1,
                -1, 0);
        res = PlaneBuffer.create(2, 2);
        assertEquals(1, Blobs.blob4(mask, res));
        assertEquals(expected, res.toPlane());

        mask = MaskBuffer.create(4, 4);
        mask.setAll(false, true, true, false,
                true, false, false, true,
                true, true, true, true,
                true, true, true, true);
        expected = Plane.create(4, 4,
                -1, 0, 0, -1,
                1, -1, -1, 1,
                1, 1, 1, 1,
                1, 1, 1, 1);
        res = PlaneBuffer.create(4, 4);
        assertEquals(2, Blobs.blob4(mask, res));
        assertEquals(expected, res.toPlane());

        mask = MaskBuffer.create(4, 4);
        mask.setAll(false, true, true, false,
                true, false, false, true,
                true, true, false, true,
                true, true, false, true);
        expected = Plane.create(4, 4,
                -1, 0, 0, -1,
                1, -1, -1, 2,
                1, 1, -1, 2,
                1, 1, -1, 2);
        res = PlaneBuffer.create(4, 4);
        assertEquals(3, Blobs.blob4(mask, res));
        assertEquals(expected, res.toPlane());

        mask = MaskBuffer.create(4, 4);
        mask.setAll(false, true, true, true,
                true, false, false, true,
                true, true, true, true,
                true, true, false, true);
        expected = Plane.create(4, 4,
                -1, 0, 0, 0,
                0, -1, -1, 0,
                0, 0, 0, 0,
                0, 0, -1, 0);
        res = PlaneBuffer.create(4, 4);
        assertEquals(1, Blobs.blob4(mask, res));
        assertEquals(expected, res.toPlane());

        mask = MaskBuffer.create(7, 2);
        mask.setAll(true, false, true, false, true, false, true,
                true, true, true, true, true, true, true);
        expected = Plane.create(7, 2,
                0, -1, 0, -1, 0, -1, 0,
                0, 0, 0, 0, 0, 0, 0);
        res = PlaneBuffer.create(7, 2);
        assertEquals(1, Blobs.blob4(mask, res));
        assertEquals(expected, res.toPlane());

        mask = MaskBuffer.create(7, 4);
        mask.setAll(true, false, true, false, true, false, true,
                true, true, true, true, true, true, true,
                false, false, false, false, false, false, false,
                true, true, true, false, true, true, true);
        expected = Plane.create(7, 4,
                0, -1, 0, -1, 0, -1, 0,
                0, 0, 0, 0, 0, 0, 0,
                -1, -1, -1, -1, -1, -1, -1,
                1, 1, 1, -1, 2, 2, 2);
        res = PlaneBuffer.create(7, 4);
        assertEquals(3, Blobs.blob4(mask, res));
        assertEquals(expected, res.toPlane());
    }

}
