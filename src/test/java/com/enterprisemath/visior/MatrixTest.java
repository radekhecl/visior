package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.ToStringBuilder;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Matrix test case.
 *
 * @author radek.hecl
 */
public class MatrixTest {

    /**
     * Creates new instance.
     */
    public MatrixTest() {
    }

    /**
     * Tests pixel retrieval.
     */
    @Test
    public void testGet() {
        Matrix matrix = Matrix.create(3, 2,
                1, 2, 3,
                4, 5, 6);

        assertEquals(1d, matrix.get(0, 0), 0d);
        assertEquals(2d, matrix.get(0, 1), 0d);
        assertEquals(3d, matrix.get(0, 2), 0d);
        assertEquals(4d, matrix.get(1, 0), 0d);
        assertEquals(5d, matrix.get(1, 1), 0d);
        assertEquals(6d, matrix.get(1, 2), 0d);

        assertEquals(1d, matrix.get(0), 0d);
        assertEquals(2d, matrix.get(1), 0d);
        assertEquals(3d, matrix.get(2), 0d);
        assertEquals(4d, matrix.get(3), 0d);
        assertEquals(5d, matrix.get(4), 0d);
        assertEquals(6d, matrix.get(5), 0d);

    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
