package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Images;
import com.google.common.util.concurrent.AtomicDouble;
import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.io.FileUtils;

/**
 * Application for converting video to png.
 *
 * @author radek.hecl
 */
public class VideoToPngConvertorApp {

    /**
     * Test application.
     */
    private VideoToPngConvertorApp() {
    }

    /**
     * Main function.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        String outDir = "tmp/video";
        String videoPath = "src/test/resources/markers/paper-2.mp4";
        File dir = new File(outDir);
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                FileUtils.deleteQuietly(f);
            }
        }
        if (!dir.isDirectory()) {
            if (!dir.mkdirs()) {
                throw new RuntimeException("unable to set up parent dirctory");
            }
        }
        AtomicInteger last = new AtomicInteger(0);
        double freq = 0.2;
        AtomicDouble lastT = new AtomicDouble(-freq);
        VideoUtils.processVideoImages(videoPath, (img, t) -> {
            if (t >= lastT.get() + freq) {
                Images.save(img, outDir + "/image-" + last.incrementAndGet() + ".png");
                lastT.addAndGet(freq);
            }
            return null;
        });

        System.out.println("-------------------");
        System.out.println("JOB DONE!");
        System.out.println("-------------------");

    }

}
