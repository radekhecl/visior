package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.BorderType;
import com.enterprisemath.visior.Contour2;
import com.enterprisemath.visior.Drawing;
import com.enterprisemath.visior.Images;
import com.enterprisemath.visior.MaskBuffer;
import com.enterprisemath.visior.Matrix;
import com.enterprisemath.visior.Operators;
import com.enterprisemath.visior.PlaneBuffer;
import com.enterprisemath.visior.Qr;
import com.enterprisemath.visior.RgbColor;
import com.enterprisemath.visior.VolumeBuffer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 * Test application.
 *
 * @author radek.hecl
 */
public class QrImagesTestApp {

    /**
     * Test application.
     */
    private QrImagesTestApp() {
    }

    /**
     * Main function.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        String outDir = "tmp/qr";
        File dir = new File(outDir);
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                FileUtils.deleteQuietly(f);
            }
        }
        if (!dir.isDirectory()) {
            if (!dir.mkdirs()) {
                throw new RuntimeException("unable to set up parent dirctory");
            }
        }

        Matrix hgauss = Matrix.gausHor(5, 2);
        Matrix vgauss = Matrix.gausVert(5, 2);
        File srcDir = new File("src/test/resources/qr");
        for (File file : srcDir.listFiles()) {
            String fname = file.getName();
            if (!fname.startsWith("qr-single") && !fname.startsWith("qr-zero")) {
                continue;
            }
            String fbase = file.getName().split("\\.")[0];
            System.out.println("Procesing " + file.getName());

            //
            // preprocessing
            VolumeBuffer input = Images.load(file);
            //Images.save(input, outDir + "/" + fbase + "-01.png");
            PlaneBuffer avg = PlaneBuffer.create(input.getWidth(), input.getHeight());
            PlaneBuffer smooth1 = PlaneBuffer.create(input.getWidth(), input.getHeight());
            PlaneBuffer smooth2 = PlaneBuffer.create(input.getWidth(), input.getHeight());
            MaskBuffer mask = MaskBuffer.create(input.getWidth(), input.getHeight());

            Operators.avg(input, avg);
            Operators.conv(avg, hgauss, BorderType.REFLECT, smooth1);
            Operators.conv(avg, vgauss, BorderType.REFLECT, smooth2);

            Operators.thres(smooth2, 0.35f, mask);
            //Images.save(mask, outDir + "/" + fbase + "-03.png");

            //
            // detection
            List<Contour2> qrs = Qr.detect(mask);

            //
            // present results
            BufferedImage res = Images.toBuffered(mask);
            for (Contour2 qr : qrs) {
                System.out.println(qr);
                Drawing.contour(res, qr, RgbColor.GREEN, 3);
            }
            Images.save(res, outDir + "/" + fbase + "-04.png");

            System.out.println("Processed " + file.getName());
            System.out.println("----");
        }

        System.out.println("-------------------");
        System.out.println("JOB DONE!");
        System.out.println("-------------------");

    }

}
