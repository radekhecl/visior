package com.enterprisemath.visior.examples;

import java.awt.image.BufferedImage;

/**
 * Video player listener.
 *
 * @author radek.hecl
 */
public interface VideoPlayerListener {

    /**
     * Called when video starts.
     */
    public void onStart();

    /**
     * Called when there is an error during play.
     * 
     * @param t throwable object
     */
    public void onError(Throwable t);

    /**
     * Called when video play stops by successful reaching of the end.
     */
    public void onFinish();

    /**
     * Callback when new frame is available.
     *
     * @param img frame image
     * @param t timestamp
     */
    public void onFrame(BufferedImage img, double t);

}
