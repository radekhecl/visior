package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Ar;
import com.enterprisemath.visior.Contour2;
import com.enterprisemath.visior.CorrespondingPair;
import com.enterprisemath.visior.Images;
import com.enterprisemath.visior.Matrix33;
import com.enterprisemath.visior.Matrix34;
import com.enterprisemath.visior.Matrix44;
import com.enterprisemath.visior.Vector2;
import com.enterprisemath.visior.VolumeBuffer;
import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import static com.jogamp.opengl.GL.GL_ARRAY_BUFFER;
import static com.jogamp.opengl.GL.GL_COLOR_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_TEST;
import static com.jogamp.opengl.GL.GL_FLOAT;
import static com.jogamp.opengl.GL.GL_LEQUAL;
import static com.jogamp.opengl.GL.GL_NEAREST;
import static com.jogamp.opengl.GL.GL_RGB;
import static com.jogamp.opengl.GL.GL_STATIC_DRAW;
import static com.jogamp.opengl.GL.GL_TEXTURE0;
import static com.jogamp.opengl.GL.GL_TEXTURE_2D;
import static com.jogamp.opengl.GL.GL_TEXTURE_MAG_FILTER;
import static com.jogamp.opengl.GL.GL_TEXTURE_MIN_FILTER;
import static com.jogamp.opengl.GL.GL_TRIANGLES;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.Animator;
import java.awt.image.BufferedImage;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JFrame;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Test application.
 *
 * @author radek.hecl
 */
public final class CameraPoseJoglTestApp extends JFrame implements GLEventListener, VideoPlayerListener {

    /**
     * Video player.
     */
    private VideoPlayer videoPlayer;

    /**
     * Object for synchronization.
     */
    private final Object lock = new Object();

    /**
     * Marker tracker.
     */
    private MarkerTracker tracker = WhiteMarkerTracker.create();

    /**
     * Contains setup of the new world.
     */
    private Map<String, Object> newWorldSetup = new HashMap<>();

    /**
     * Canvas.
     */
    private GLCanvas canvas;

    /**
     * Program for rendering background.
     */
    private int bgProgram;

    /**
     * Reference to models program.
     */
    private int modelProgram;

    /**
     * Vertex array objects.
     */
    private int vao[] = new int[1];

    /**
     * Vertex buffer objects.
     */
    private int vbo[] = new int[4];

    /**
     * Background texture.
     */
    private int bgTexture;

    /**
     * Brick texture.
     */
    private int brickTexture;

    /**
     * Buffer for binding uniform matrices to the graphics card.
     */
    private float[] matBuf = new float[16];

    /**
     * Perspective matrix.
     */
    private Matrix44 pMat = null;

    /**
     * Model - view matrix.
     */
    private Matrix44 mvMat = null;

    /**
     * Executes program.
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        new CameraPoseJoglTestApp();
    }

    /**
     * Creates new instance.
     */
    private CameraPoseJoglTestApp() {
        setTitle("Test application");
        setSize(656, 519);
        setLocation(200, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        canvas = new GLCanvas();
        canvas.addGLEventListener(this);
        add(canvas);
        setVisible(true);
        Animator animtr = new Animator(canvas);
        animtr.start();
    }

    @Override
    public void display(GLAutoDrawable glad) {
        GL4 gl = (GL4) GLContext.getCurrentGL();
        gl.glClearColor(0f, 0f, 0f, 1f);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        synchronized (lock) {
            if (!newWorldSetup.isEmpty()) {
                pMat = (Matrix44) newWorldSetup.get("pMat");
                mvMat = (Matrix44) newWorldSetup.get("mvMat");
                VolumeBuffer frame = (VolumeBuffer) newWorldSetup.get("bg");
                gl.glBindTexture(GL_TEXTURE_2D, bgTexture);
                float[] rbuf = frame.getPlane(0).getBuf();
                float[] gbuf = frame.getPlane(1).getBuf();
                float[] bbuf = frame.getPlane(2).getBuf();
                float[] colors = new float[frame.getWidth() * frame.getHeight() * 3];
                for (int i = 0; i < rbuf.length; ++i) {
                    colors[i * 3] = rbuf[i];
                    colors[i * 3 + 1] = gbuf[i];
                    colors[i * 3 + 2] = bbuf[i];
                }
                FloatBuffer texBuffer = Buffers.newDirectFloatBuffer(colors);
                gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, frame.getWidth(), frame.getHeight(), 0, GL_RGB, GL.GL_FLOAT, texBuffer);
                newWorldSetup.clear();
            }
        }

        // background
        gl.glUseProgram(bgProgram);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        gl.glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(0);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        gl.glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(1);

        gl.glActiveTexture(GL_TEXTURE0);
        gl.glBindTexture(GL_TEXTURE_2D, bgTexture);

        gl.glEnable(GL_DEPTH_TEST);
        gl.glDepthFunc(GL_LEQUAL);

        gl.glDrawArrays(GL_TRIANGLES, 0, 6);

        // model
        if (pMat != null && mvMat != null) {
            gl.glClear(GL_DEPTH_BUFFER_BIT);
            gl.glUseProgram(modelProgram);

            int mvLoc = gl.glGetUniformLocation(modelProgram, "mv_matrix");
            int projLoc = gl.glGetUniformLocation(modelProgram, "proj_matrix");

            gl.glUniformMatrix4fv(mvLoc, 1, false, mvMat.toBufCol(matBuf), 0);
            gl.glUniformMatrix4fv(projLoc, 1, false, pMat.toBufCol(matBuf), 0);

            gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
            gl.glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
            gl.glEnableVertexAttribArray(0);

            gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
            gl.glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
            gl.glEnableVertexAttribArray(1);

            gl.glActiveTexture(GL_TEXTURE0);
            gl.glBindTexture(GL_TEXTURE_2D, brickTexture);

            gl.glEnable(GL_DEPTH_TEST);
            gl.glDepthFunc(GL_LEQUAL);

            gl.glDrawArrays(GL_TRIANGLES, 0, 36);
        }
    }

    @Override
    public void init(GLAutoDrawable glad) {
        try {
            modelProgram = GLUtils.loadShaderProgram("src/test/resources/jogl/04-vertexShader.glsl", "src/test/resources/jogl/04-fragmentShader.glsl");
            bgProgram = GLUtils.loadShaderProgram("src/test/resources/jogl/05-vertexShader.glsl", "src/test/resources/jogl/05-fragmentShader.glsl");
            setupVertices();
            brickTexture = GLUtils.loadTexture("src/test/resources/brick-1.jpg");
            GL4 gl = (GL4) GLContext.getCurrentGL();
            int[] tao = new int[1];
            gl.glGenTextures(1, tao, 0);
            bgTexture = tao[0];
            gl.glBindTexture(GL_TEXTURE_2D, bgTexture);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
            videoPlayer = VideoPlayer.create(this);
            playVideo();
        } catch (RuntimeException e) {
            System.err.print(ExceptionUtils.getStackTrace(e));
            System.exit(1);
        }
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
        GL4 gl = (GL4) GLContext.getCurrentGL();
        gl.glBindVertexArray(vao[0]);
        gl.glDeleteBuffers(vbo.length, vbo, 0);
        gl.glDeleteVertexArrays(vao.length, vao, 0);
        gl.glDeleteProgram(modelProgram);
        gl.glDeleteProgram(bgProgram);
        gl.glDeleteTextures(1, new int[]{bgTexture, brickTexture}, 0);
    }

    @Override
    public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {
        System.out.println(i + " - " + i1 + " - " + i2 + " - " + i3);
    }

    @Override
    public void onStart() {
        System.out.println("Starting");
    }

    @Override
    public void onError(Throwable t) {
        System.err.print(ExceptionUtils.getStackTrace(t));
        System.exit(1);
    }

    @Override
    public void onFinish() {
        System.out.println("Finished");
        playVideo();
    }

    @Override
    public void onFrame(BufferedImage img, double t) {
        VolumeBuffer input = Images.create(img);
        Contour2 marker = tracker.track(input);
        Matrix44 newpMat = null;
        Matrix44 newmvMat = null;
        if (marker != null) {
            float f = 800;
            float cx = input.getWidth() / 2f;
            float cy = input.getHeight() / 2f;
            float s = 0f;
            float sx3d = 2.97f;
            float sy3d = 2.10f;
            float near = 0.1f;
            float far = 1000.0f;
            Matrix33 k = Matrix33.create(f, s, cx, 0, f, cy, 0, 0, 1);
            Matrix33 kinv = k.inv();
            List<CorrespondingPair<Vector2, Vector2>> corrs = Arrays.asList(
                    CorrespondingPair.create(Vector2.create(-sx3d / 2, -sy3d / 2), marker.getPoint(0)),
                    CorrespondingPair.create(Vector2.create(-sx3d / 2, sy3d / 2), marker.getPoint(1)),
                    CorrespondingPair.create(Vector2.create(sx3d / 2, sy3d / 2), marker.getPoint(2)),
                    CorrespondingPair.create(Vector2.create(sx3d / 2, -sy3d / 2), marker.getPoint(3)));
            Matrix34 mv = Ar.estimateMvMatrix(kinv, corrs);
            newpMat = Ar.toGlProj(k, near, far, input.getWidth(), input.getHeight());
            newmvMat = Ar.toGlV(mv);
        }
        synchronized (lock) {
            newWorldSetup.put("bg", input);
            newWorldSetup.put("pMat", newpMat);
            newWorldSetup.put("mvMat", newmvMat);
        }
    }

    /**
     * Sets up vertices.
     */
    private void setupVertices() {
        GL4 gl = (GL4) GLContext.getCurrentGL();

        float[] bgPositions = {
            -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f, -1.0f, 1.0f, 0.0f
        };

        float[] bgTextureCoords = {
            0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f
        };

        float[] boxPositions = {
            -0.5f, 0.5f, 0.0f, -0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 0.0f,
            0.5f, -0.5f, 0.0f, 0.5f, 0.5f, 0.0f, -0.5f, 0.5f, 0.0f,
            0.5f, -0.5f, 0.0f, 0.5f, -0.5f, 1.0f, 0.5f, 0.5f, 0.0f,
            0.5f, -0.5f, 1.0f, 0.5f, 0.5f, 1.0f, 0.5f, 0.5f, 0.0f,
            0.5f, -0.5f, 1.0f, -0.5f, -0.5f, 1.0f, 0.5f, 0.5f, 1.0f,
            -0.5f, -0.5f, 1.0f, -0.5f, 0.5f, 1.0f, 0.5f, 0.5f, 1.0f,
            -0.5f, -0.5f, 1.0f, -0.5f, -0.5f, 0.0f, -0.5f, 0.5f, 1.0f,
            -0.5f, -0.5f, 0.0f, -0.5f, 0.5f, 0.0f, -0.5f, 0.5f, 1.0f,
            -0.5f, -0.5f, 1.0f, 0.5f, -0.5f, 1.0f, 0.5f, -0.5f, 0.0f,
            0.5f, -0.5f, 0.0f, -0.5f, -0.5f, 0.0f, -0.5f, -0.5f, 1.0f,
            -0.5f, 0.5f, 0.0f, 0.5f, 0.5f, 0.0f, 0.5f, 0.5f, 1.0f,
            0.5f, 0.5f, 1.0f, -0.5f, 0.5f, 1.0f, -0.5f, 0.5f, 0.0f
        };

        float[] boxTextureCoords = {
            0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
            1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
            1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
            0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
            0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f,
            1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f
        };

        gl.glGenVertexArrays(vao.length, vao, 0);
        gl.glBindVertexArray(vao[0]);
        gl.glGenBuffers(vbo.length, vbo, 0);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        FloatBuffer bgBuf = Buffers.newDirectFloatBuffer(bgPositions);
        gl.glBufferData(GL_ARRAY_BUFFER, bgBuf.limit() * 4, bgBuf, GL_STATIC_DRAW);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        FloatBuffer bgTexBuf = Buffers.newDirectFloatBuffer(bgTextureCoords);
        gl.glBufferData(GL_ARRAY_BUFFER, bgTexBuf.limit() * 4, bgTexBuf, GL_STATIC_DRAW);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
        FloatBuffer boxBuf = Buffers.newDirectFloatBuffer(boxPositions);
        gl.glBufferData(GL_ARRAY_BUFFER, boxBuf.limit() * 4, boxBuf, GL_STATIC_DRAW);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[3]);
        FloatBuffer texBuf = Buffers.newDirectFloatBuffer(boxTextureCoords);
        gl.glBufferData(GL_ARRAY_BUFFER, texBuf.limit() * 4, texBuf, GL_STATIC_DRAW);
    }

    /**
     * Plays video.
     */
    private void playVideo() {
        videoPlayer.playAsync("src/test/resources/markers/paper-2.mp4");
    }

}
