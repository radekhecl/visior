package com.enterprisemath.visior.examples;

import com.twelvemonkeys.lang.Validate;
import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.MediaListenerAdapter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.mediatool.event.IVideoPictureEvent;
import com.xuggle.xuggler.IError;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Video player definition.
 *
 * @author radek.hecl
 */
public class VideoPlayer {

    /**
     * Listener.
     */
    private VideoPlayerListener listener;

    /**
     * Creates new instance.
     */
    private VideoPlayer() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
        Validate.notNull(listener, "listener cannot be null");
    }

    /**
     * Plays video asynchronously.
     *
     * @param filePath file path
     */
    public void playAsync(String filePath) {
        Thread t = new Thread(new PlayTask(listener, filePath));
        t.start();
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param listener listener
     * @return created object
     */
    public static VideoPlayer create(VideoPlayerListener listener) {
        VideoPlayer res = new VideoPlayer();
        res.listener = listener;
        res.guardInvariants();
        return res;
    }

    /**
     * Task for playing video.
     */
    private static class PlayTask implements Runnable {

        /**
         * Listener.
         */
        private VideoPlayerListener listener;

        /**
         * File path.
         */
        private String filePath;

        /**
         * Start timestamp.
         */
        private long startTs;

        /**
         * Creates new instance.
         *
         * @param listener listener
         * @param filePath file path
         */
        public PlayTask(VideoPlayerListener listener, String filePath) {
            this.listener = listener;
            this.filePath = filePath;
            guardInvariants();
        }

        /**
         * Guards this object to be consistent.
         */
        private void guardInvariants() {
            Validate.notNull(listener, "listener cannot be null");
            Validate.notNull(filePath, "filePath cannot be null");
        }

        @Override
        public void run() {
            IMediaReader reader = null;
            try {
                VideoListener vl = new VideoListener(listener);
                reader = ToolFactory.makeReader(filePath);
                reader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);
                reader.addListener(vl);
                startTs = System.currentTimeMillis();
                listener.onStart();
                boolean done = false;
                while (!done) {
                    long ts = System.currentTimeMillis();
                    double t = (ts - startTs) / 1000d;
                    if (t >= vl.getLastT()) {
                        IError err = reader.readPacket();
                        if (err != null) {
                            done = true;
                        }
                    }
                }
                reader.close();
                reader = null;
                listener.onFinish();
            } catch (RuntimeException e) {
                listener.onError(e);
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (RuntimeException e) {
                    // supress as this is a silent close
                }
            }
        }

    }

    /**
     * Listener for video frames.
     */
    private static class VideoListener extends MediaListenerAdapter {

        /**
         * Listener.
         */
        private VideoPlayerListener listener;

        /**
         * Time of the last frame.
         */
        private double lastT = 0;

        /**
         * Creates new instance.
         *
         * @param listener listener
         */
        public VideoListener(VideoPlayerListener listener) {
            this.listener = listener;
            guardInvariants();
        }

        /**
         * Guards this object to be consistent. Throws exception if this is not the case.
         */
        private void guardInvariants() {
        }

        @Override
        public void onVideoPicture(IVideoPictureEvent event) {
            try {
                double t = event.getTimeStamp(TimeUnit.MICROSECONDS) * 1e-6;
                BufferedImage in = event.getImage();
                listener.onFrame(in, t);
                lastT = t;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        /**
         * Retruns t of the last frame.
         *
         * @return t of the last frame
         */
        public double getLastT() {
            return lastT;
        }

    }

}
