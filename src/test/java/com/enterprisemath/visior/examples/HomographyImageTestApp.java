package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Aabb2;
import com.enterprisemath.visior.Ar;
import com.enterprisemath.visior.Contour2;
import com.enterprisemath.visior.CorrespondingPair;
import com.enterprisemath.visior.Drawing;
import com.enterprisemath.visior.Images;
import com.enterprisemath.visior.Matrix33;
import com.enterprisemath.visior.Matrix34;
import com.enterprisemath.visior.RgbColor;
import com.enterprisemath.visior.Vector2;
import com.enterprisemath.visior.Vector3;
import com.enterprisemath.visior.Vector4;
import com.enterprisemath.visior.VolumeBuffer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 * Test application to estimate camera pose for an image.
 *
 * @author radek.hecl
 */
public class HomographyImageTestApp {

    /**
     * Test application.
     */
    private HomographyImageTestApp() {
    }

    /**
     * Main function.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        String outDir = "tmp/homography";
        File dir = new File(outDir);
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                FileUtils.deleteQuietly(f);
            }
        }
        if (!dir.isDirectory()) {
            if (!dir.mkdirs()) {
                throw new RuntimeException("unable to set up parent dirctory");
            }
        }

        File srcDir = new File("src/test/resources/markers");
        for (File file : srcDir.listFiles()) {
            String fname = file.getName();
            if (!fname.endsWith(".png")) {
                continue;
            }
            String fbase = file.getName().split("\\.")[0];
            System.out.println("----");
            System.out.println("Procesing " + file.getName());

            //
            // preprocessing
            List<Vector2> rectPos = Arrays.asList(Vector2.create(75, 75), Vector2.create(75, 25), Vector2.create(25, 25), Vector2.create(25, 75));
            BufferedImage top = Images.createBuffered(100, 100);
            Drawing.fillRect(top, 0, 0, top.getWidth(), top.getHeight(), RgbColor.WHITE);
            Drawing.aabb(top, Aabb2.create(Vector2.create(1, 1), Vector2.create(top.getWidth() - 2, top.getHeight() - 2)), RgbColor.GREEN, 3);
            for (int i = 0; i < rectPos.size(); ++i) {
                Vector2 v1 = rectPos.get(i);
                Vector2 v2 = rectPos.get(i == rectPos.size() - 1 ? 0 : i + 1);
                Drawing.line(top, v1, v2, RgbColor.BLUE, 3);
            }
            
            VolumeBuffer input = Images.load(file);
            BufferedImage res = Images.toBuffered(input);
            Drawing.image(res, top, 0, 0, top.getWidth(), top.getHeight());
            WhiteMarkerTracker tracker = WhiteMarkerTracker.create();
            Contour2 marker = tracker.track(input);

            if (marker != null) {
                Drawing.contour(res, marker, RgbColor.GREEN, 3);
                Matrix33 h = Ar.estimateHomography(Arrays.asList(
                        CorrespondingPair.create(Vector2.create(top.getWidth(), top.getHeight()), marker.getPoint(0)),
                        CorrespondingPair.create(Vector2.create(top.getWidth(), 0), marker.getPoint(1)),
                        CorrespondingPair.create(Vector2.create(0, 0), marker.getPoint(2)),
                        CorrespondingPair.create(Vector2.create(0, top.getHeight()), marker.getPoint(3))
                ));
                for (int i = 0; i < rectPos.size(); ++i) {
                    Vector2 v1 = rectPos.get(i);
                    Vector2 v2 = rectPos.get(i == rectPos.size() - 1 ? 0 : i + 1);
                    Vector2 v1t = h.mul(Vector3.create(v1.getX(), v1.getY(), 1)).dehomogenize();
                    Vector2 v2t = h.mul(Vector3.create(v2.getX(), v2.getY(), 1)).dehomogenize();
                    Drawing.line(res, v1t, v2t, RgbColor.BLUE, 3);
                }
                Vector2 p1 = Vector2.create(top.getWidth(), top.getHeight());
                Vector2 p1t = h.mul(Vector3.create(p1.getX(), p1.getY(), 1)).dehomogenize();
                Vector2 p2 = Vector2.create(top.getWidth(), 0);
                Vector2 p2t = h.mul(Vector3.create(p2.getX(), p2.getY(), 1)).dehomogenize();
                Vector2 p3 = Vector2.create(0, 0);
                Vector2 p3t = h.mul(Vector3.create(p3.getX(), p3.getY(), 1)).dehomogenize();
                Vector2 p4 = Vector2.create(0, top.getHeight());
                Vector2 p4t = h.mul(Vector3.create(p4.getX(), p4.getY(), 1)).dehomogenize();
                Drawing.line(res, p1, p1t, RgbColor.RED, 2);
                Drawing.line(res, p2, p2t, RgbColor.RED, 2);
                Drawing.line(res, p3, p3t, RgbColor.RED, 2);
                Drawing.line(res, p4, p4t, RgbColor.RED, 2);
            }

            //
            // present results
            Images.save(Images.toBuffered(input), outDir + "/" + fbase + "-in.png");
            Images.save(res, outDir + "/" + fbase + "-out.png");
        }

        System.out.println("-------------------");
        System.out.println("JOB DONE!");
        System.out.println("-------------------");

    }

}
