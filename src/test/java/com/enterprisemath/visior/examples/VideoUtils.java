package com.enterprisemath.visior.examples;

import com.twelvemonkeys.lang.Validate;
import com.xuggle.mediatool.IMediaReader;
import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.MediaListenerAdapter;
import com.xuggle.mediatool.ToolFactory;
import com.xuggle.mediatool.event.IVideoPictureEvent;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;

/**
 * Class with utility methods for video processing.
 *
 * @author radek.hecl
 */
public class VideoUtils {

    /**
     * Prevents construction.
     */
    private VideoUtils() {
    }

    /**
     * Processes video images.
     *
     * @param inPath input file path
     * @param outPath output file path
     * @param processor processor
     */
    public static void processVideoImages(String inPath, String outPath, VideoFrameProcessor processor) {
        IMediaReader reader = null;
        IMediaWriter writer = null;

        try {
            reader = ToolFactory.makeReader(inPath);
            writer = ToolFactory.makeWriter(outPath);
            reader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);
            reader.addListener(new VideoConvertAdapter(processor, writer));

            while (reader.readPacket() == null) {
                do {
                } while (false);
            }
            reader.close();
            reader = null;
            writer.close();
            writer = null;
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (RuntimeException e) {
                // supress as this is a silent close
            }
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (RuntimeException e) {
                // supress as this is a silent close
            }
        }
    }

    /**
     * Processes video images.
     *
     * @param inPath input file path
     * @param processor processor
     */
    public static void processVideoImages(String inPath, VideoFrameProcessor processor) {
        IMediaReader reader = null;
        try {
            reader = ToolFactory.makeReader(inPath);
            reader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR);
            reader.addListener(new VideoReadAapter(processor));

            while (reader.readPacket() == null) {
                do {
                } while (false);
            }
            reader.close();
            reader = null;
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (RuntimeException e) {
                // supress as this is a silent close
            }
        }
    }

    /**
     * Adapter for converting video.
     */
    private static class VideoConvertAdapter extends MediaListenerAdapter {

        /**
         * Processor for the images.
         */
        private VideoFrameProcessor processor;

        /**
         * Video writer.
         */
        private IMediaWriter writer;

        /**
         * Stream index.
         */
        private int streamIdx = -1;

        public VideoConvertAdapter(VideoFrameProcessor processor, IMediaWriter writer) {
            this.processor = processor;
            this.writer = writer;
            guardInvariants();
        }

        /**
         * Guards this object to be consistent. Throws exception if this is not the case.
         */
        private void guardInvariants() {
            Validate.notNull(processor, "processor cannot be null");
            Validate.notNull(writer, "writer cannot be null");
        }

        @Override
        public void onVideoPicture(IVideoPictureEvent event) {
            try {
                int idx = event.getStreamIndex();
                double t = event.getTimeStamp(TimeUnit.MICROSECONDS) * 1e-6;
                BufferedImage in = event.getImage();
                BufferedImage out = processor.process(in, t);
                if (out != null) {
                    if (streamIdx == -1) {
                        streamIdx = idx;
                        writer.addVideoStream(0, 0, out.getWidth(), out.getHeight());
                    }
                    else if (streamIdx != idx) {
                        throw new RuntimeException("only videos with 1 strem index are supported");
                    }
                    writer.encodeVideo(0, out, event.getTimeStamp(), TimeUnit.MICROSECONDS);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    }

    /**
     * Adapter for reading video.
     */
    private static class VideoReadAapter extends MediaListenerAdapter {

        /**
         * Processor for the images.
         */
        private VideoFrameProcessor processor;

        /**
         * Creates new instance.
         *
         * @param processor frame processor
         */
        public VideoReadAapter(VideoFrameProcessor processor) {
            this.processor = processor;
            guardInvariants();
        }

        /**
         * Guards this object to be consistent. Throws exception if this is not the case.
         */
        private void guardInvariants() {
            Validate.notNull(processor, "processor cannot be null");
        }

        @Override
        public void onVideoPicture(IVideoPictureEvent event) {
            try {
                int idx = event.getStreamIndex();
                double t = event.getTimeStamp(TimeUnit.MICROSECONDS) * 1e-6;
                BufferedImage in = event.getImage();
                processor.process(in, t);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    }

}
