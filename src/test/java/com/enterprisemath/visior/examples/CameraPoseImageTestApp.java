package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Aabb2;
import com.enterprisemath.visior.Ar;
import com.enterprisemath.visior.Contour2;
import com.enterprisemath.visior.CorrespondingPair;
import com.enterprisemath.visior.Drawing;
import com.enterprisemath.visior.Images;
import com.enterprisemath.visior.Matrix33;
import com.enterprisemath.visior.Matrix34;
import com.enterprisemath.visior.RgbColor;
import com.enterprisemath.visior.Vector2;
import com.enterprisemath.visior.Vector4;
import com.enterprisemath.visior.VolumeBuffer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 * Test application to estimate camera pose for an image.
 *
 * @author radek.hecl
 */
public class CameraPoseImageTestApp {

    /**
     * Test application.
     */
    private CameraPoseImageTestApp() {
    }

    /**
     * Main function.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        String outDir = "tmp/ar";
        File dir = new File(outDir);
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                FileUtils.deleteQuietly(f);
            }
        }
        if (!dir.isDirectory()) {
            if (!dir.mkdirs()) {
                throw new RuntimeException("unable to set up parent dirctory");
            }
        }

        File srcDir = new File("src/test/resources/markers");
        for (File file : srcDir.listFiles()) {
            String fname = file.getName();
            if (!fname.endsWith(".png")) {
                continue;
            }
            String fbase = file.getName().split("\\.")[0];
            System.out.println("----");
            System.out.println("Procesing " + file.getName());

            //
            // preprocessing
            VolumeBuffer input = Images.load(file);
            WhiteMarkerTracker tracker = WhiteMarkerTracker.create();
            Contour2 marker = tracker.track(input);
            Images.save(input, outDir + "/" + fbase + "-out-0-orig.png");
            Images.save(tracker.getMask(), outDir + "/" + fbase + "-out-1-mask.png");
            Images.save(tracker.getBiggestBlob(), outDir + "/" + fbase + "-out-2-blob.png");
            BufferedImage contour = Images.toBuffered(tracker.getBiggestBlob());
            Drawing.points(contour, tracker.getContourPts(), RgbColor.RED, 2, 1);
            Images.save(contour, outDir + "/" + fbase + "-out-3-contour.png");

            if (marker == null) {
                continue;
            }
            contour = Images.toBuffered(tracker.getBiggestBlob());
            Drawing.contour(contour, marker, RgbColor.GREEN, 3);
            Drawing.points(contour, marker.getPoints(), RgbColor.RED, 20, 5);
            for (int i = 0; i < marker.getNumPoints(); ++i) {
                Vector2 pos = marker.getPoint(i);
                String txt = "P" + (i + 1) + " = (" + (int) pos.getX() + ", " + (int) pos.getY() + ")";
                Drawing.text(contour, txt, pos.getX() - 50, pos.getY() - 30, RgbColor.RED, 20, true);
            }
            Images.save(contour, outDir + "/" + fbase + "-out-4-plane.png");

            float f = 400;
            float cx = input.getWidth() / 2f;
            float cy = input.getHeight() / 2f;
            float s = 0f;
            float sx3d = 29.7f;
            float sy3d = 21.0f;
            Matrix33 k = Matrix33.create(f, s, cx, 0, f, cy, 0, 0, 1);
            Matrix33 kinv = k.inv();
            List<CorrespondingPair<Vector2, Vector2>> corrs = Arrays.asList(
                    CorrespondingPair.create(Vector2.create(-sx3d / 2, -sy3d / 2), marker.getPoint(0)),
                    CorrespondingPair.create(Vector2.create(-sx3d / 2, sy3d / 2), marker.getPoint(1)),
                    CorrespondingPair.create(Vector2.create(sx3d / 2, sy3d / 2), marker.getPoint(2)),
                    CorrespondingPair.create(Vector2.create(sx3d / 2, -sy3d / 2), marker.getPoint(3)));
            Matrix34 mv = Ar.estimateMvMatrix(kinv, corrs);
            Matrix34 p = k.mul(mv);

            drawRect(contour, p, Math.min(sx3d, sy3d) / 2);
            Images.save(contour, outDir + "/" + fbase + "-out-5-rect.png");
            drawCube(contour, p, Math.min(sx3d, sy3d) / 2);
            Images.save(contour, outDir + "/" + fbase + "-out-6-cube.png");

            //
            // present results
            BufferedImage res = Images.toBuffered(input);
            Drawing.contour(res, marker, RgbColor.GREEN, 3);
            drawCube(res, p, Math.min(sx3d, sy3d) / 2);
            Images.save(res, outDir + "/" + fbase + "-out-final.png");
        }

        System.out.println("-------------------");
        System.out.println("JOB DONE!");
        System.out.println("-------------------");

    }

    /**
     * Draws rectangle.
     *
     * @param img image
     * @param p projection matrix
     * @param mv model view matrix
     * @param size cube size
     */
    private static void drawRect(BufferedImage img, Matrix34 p, float size) {
        float sizeh = size / 2;
        Vector2 v1 = project(p, Vector4.create(-sizeh, -sizeh, 0f, 1f));
        Vector2 v2 = project(p, Vector4.create(-sizeh, sizeh, 0f, 1f));
        Vector2 v3 = project(p, Vector4.create(sizeh, sizeh, 0f, 1f));
        Vector2 v4 = project(p, Vector4.create(sizeh, -sizeh, 0f, 1f));
        Drawing.line(img, v1, v2, RgbColor.BLUE, 4);
        Drawing.line(img, v2, v3, RgbColor.BLUE, 4);
        Drawing.line(img, v3, v4, RgbColor.BLUE, 4);
        Drawing.line(img, v4, v1, RgbColor.BLUE, 4);
    }

    /**
     * Draws cube.
     *
     * @param img image
     * @param p projection matrix
     * @param mv model view matrix
     * @param size cube size
     */
    private static void drawCube(BufferedImage img, Matrix34 p, float size) {
        float sizeh = size / 2;
        Vector2 v1 = project(p, Vector4.create(-sizeh, -sizeh, 0f, 1f));
        Vector2 v2 = project(p, Vector4.create(-sizeh, sizeh, 0f, 1f));
        Vector2 v3 = project(p, Vector4.create(sizeh, sizeh, 0f, 1f));
        Vector2 v4 = project(p, Vector4.create(sizeh, -sizeh, 0f, 1f));
        Vector2 v5 = project(p, Vector4.create(-sizeh, -sizeh, size, 1f));
        Vector2 v6 = project(p, Vector4.create(-sizeh, sizeh, size, 1f));
        Vector2 v7 = project(p, Vector4.create(sizeh, sizeh, size, 1f));
        Vector2 v8 = project(p, Vector4.create(sizeh, -sizeh, size, 1f));
        Drawing.line(img, v1, v2, RgbColor.BLUE, 4);
        Drawing.line(img, v2, v3, RgbColor.BLUE, 4);
        Drawing.line(img, v3, v4, RgbColor.BLUE, 4);
        Drawing.line(img, v4, v1, RgbColor.BLUE, 4);
        Drawing.line(img, v5, v6, RgbColor.BLUE, 4);
        Drawing.line(img, v6, v7, RgbColor.BLUE, 4);
        Drawing.line(img, v7, v8, RgbColor.BLUE, 4);
        Drawing.line(img, v8, v5, RgbColor.BLUE, 4);
        Drawing.line(img, v1, v5, RgbColor.BLUE, 4);
        Drawing.line(img, v2, v6, RgbColor.BLUE, 4);
        Drawing.line(img, v3, v7, RgbColor.BLUE, 4);
        Drawing.line(img, v4, v8, RgbColor.BLUE, 4);
    }

    /**
     * Projects vector.
     *
     * @param p projection matrix
     * @param mv model view matrix
     * @param vec vector to project
     * @return projected vector
     */
    private static Vector2 project(Matrix34 p, Vector4 vec) {
        return p.mul(vec).dehomogenize();
    }

}
