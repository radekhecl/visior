package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Ar;
import com.enterprisemath.visior.Contour2;
import com.enterprisemath.visior.CorrespondingPair;
import com.enterprisemath.visior.Drawing;
import com.enterprisemath.visior.Fitting;
import com.enterprisemath.visior.GeneralLine2;
import com.enterprisemath.visior.Images;
import com.enterprisemath.visior.Matrix33;
import com.enterprisemath.visior.Matrix34;
import com.enterprisemath.visior.RgbColor;
import com.enterprisemath.visior.Vector2;
import com.enterprisemath.visior.Vector4;
import com.enterprisemath.visior.VolumeBuffer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 * Test application to estimate camera pose for an image.
 *
 * @author radek.hecl
 */
public class CameraPoseImageVideoTestApp {

    /**
     * Test application.
     */
    private CameraPoseImageVideoTestApp() {
    }

    /**
     * Main function.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        String outDir = "tmp/ar";
        File dir = new File(outDir);
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                FileUtils.deleteQuietly(f);
            }
        }
        if (!dir.isDirectory()) {
            if (!dir.mkdirs()) {
                throw new RuntimeException("unable to set up parent dirctory");
            }
        }
        String srcFilePath = "src/test/resources/markers/paper-4.png";
        String outPath = outDir + "/exmaple.mp4";
        VolumeBuffer input = Images.load(new File(srcFilePath));
        VideoWriter videoWriter = VideoWriter.create(outPath, input.getWidth(), input.getHeight());
        WhiteMarkerDetector tracker = WhiteMarkerDetector.create();

        double t = 0;
        for (int i = 0; i < 200; ++i, t = t + 0.1) {
            Contour2 marker = tracker.track(input);
            if (marker == null) {
                continue;
            }
            float f = 400;
            float cx = input.getWidth() / 2f;
            float cy = input.getHeight() / 2f;
            float s = 0f;
            float sx3d = 29.7f;
            float sy3d = 21.0f;
            Matrix33 k = Matrix33.create(f, s, cx, 0, f, cy, 0, 0, 1);
            Matrix33 kinv = k.inv();
            List<CorrespondingPair<Vector2, Vector2>> corrs = Arrays.asList(
                    CorrespondingPair.create(Vector2.create(-sx3d / 2, -sy3d / 2), marker.getPoint(0)),
                    CorrespondingPair.create(Vector2.create(-sx3d / 2, sy3d / 2), marker.getPoint(1)),
                    CorrespondingPair.create(Vector2.create(sx3d / 2, sy3d / 2), marker.getPoint(2)),
                    CorrespondingPair.create(Vector2.create(sx3d / 2, -sy3d / 2), marker.getPoint(3)));
            Matrix34 mv = Ar.estimateMvMatrix(kinv, corrs);
            Matrix34 p = k.mul(mv);

            //
            // present results
            BufferedImage res = Images.createBuffered(input.getWidth(), input.getHeight());
            Drawing.points(res, tracker.getContourPts(), RgbColor.RED, 2, 1);
            List<GeneralLine2> lines = Fitting.linesRansac(tracker.getContourPts(), 4, 2000, tracker.getContourPts().size() / 10, 2f);
            for (GeneralLine2 line : lines) {
                Drawing.line(res, line, RgbColor.create(0.5f, 0.5f, 1f), 2);
            }
            
            //BufferedImage res = Images.toBufferedImage(tracker.getBiggestBlob());
            //drawCube(res, p, Math.min(sx3d, sy3d) / 2);
            videoWriter.addImage(res, t);
            System.out.println("written image for time " + t);
        }
        
        videoWriter.close();

        System.out.println("-------------------");
        System.out.println("JOB DONE!");
        System.out.println("-------------------");

    }

    /**
     * Draws rectangle.
     *
     * @param img image
     * @param p projection matrix
     * @param mv model view matrix
     * @param size cube size
     */
    private static void drawRect(BufferedImage img, Matrix34 p, float size) {
        float sizeh = size / 2;
        Vector2 v1 = project(p, Vector4.create(-sizeh, -sizeh, 0f, 1f));
        Vector2 v2 = project(p, Vector4.create(-sizeh, sizeh, 0f, 1f));
        Vector2 v3 = project(p, Vector4.create(sizeh, sizeh, 0f, 1f));
        Vector2 v4 = project(p, Vector4.create(sizeh, -sizeh, 0f, 1f));
        Drawing.line(img, v1, v2, RgbColor.BLUE, 4);
        Drawing.line(img, v2, v3, RgbColor.BLUE, 4);
        Drawing.line(img, v3, v4, RgbColor.BLUE, 4);
        Drawing.line(img, v4, v1, RgbColor.BLUE, 4);
    }

    /**
     * Draws cube.
     *
     * @param img image
     * @param p projection matrix
     * @param mv model view matrix
     * @param size cube size
     */
    private static void drawCube(BufferedImage img, Matrix34 p, float size) {
        float sizeh = size / 2;
        Vector2 v1 = project(p, Vector4.create(-sizeh, -sizeh, 0f, 1f));
        Vector2 v2 = project(p, Vector4.create(-sizeh, sizeh, 0f, 1f));
        Vector2 v3 = project(p, Vector4.create(sizeh, sizeh, 0f, 1f));
        Vector2 v4 = project(p, Vector4.create(sizeh, -sizeh, 0f, 1f));
        Vector2 v5 = project(p, Vector4.create(-sizeh, -sizeh, size, 1f));
        Vector2 v6 = project(p, Vector4.create(-sizeh, sizeh, size, 1f));
        Vector2 v7 = project(p, Vector4.create(sizeh, sizeh, size, 1f));
        Vector2 v8 = project(p, Vector4.create(sizeh, -sizeh, size, 1f));
        Drawing.line(img, v1, v2, RgbColor.BLUE, 4);
        Drawing.line(img, v2, v3, RgbColor.BLUE, 4);
        Drawing.line(img, v3, v4, RgbColor.BLUE, 4);
        Drawing.line(img, v4, v1, RgbColor.BLUE, 4);
        Drawing.line(img, v5, v6, RgbColor.BLUE, 4);
        Drawing.line(img, v6, v7, RgbColor.BLUE, 4);
        Drawing.line(img, v7, v8, RgbColor.BLUE, 4);
        Drawing.line(img, v8, v5, RgbColor.BLUE, 4);
        Drawing.line(img, v1, v5, RgbColor.BLUE, 4);
        Drawing.line(img, v2, v6, RgbColor.BLUE, 4);
        Drawing.line(img, v3, v7, RgbColor.BLUE, 4);
        Drawing.line(img, v4, v8, RgbColor.BLUE, 4);
    }

    /**
     * Projects vector.
     *
     * @param p projection matrix
     * @param mv model view matrix
     * @param vec vector to project
     * @return projected vector
     */
    private static Vector2 project(Matrix34 p, Vector4 vec) {
        return p.mul(vec).dehomogenize();
    }

}
