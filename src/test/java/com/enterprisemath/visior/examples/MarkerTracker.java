package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Contour2;
import com.enterprisemath.visior.VolumeBuffer;

/**
 * Marker tracking interface.
 * Implementations can be state full. This means that information from the first call of the track method might be reused
 * to track the object in the second invocations. Implementations are not required to be thread safe.
 * It's up to the user to ensure consistency.
 *
 * @author radek.hecl
 */
public interface MarkerTracker {

    /**
     * Performs marker tracking. Returns most recent marker, or null if marker is lost.
     *
     * @param input input volume
     * @return most recent marker, or null if marker is lost
     */
    public Contour2 track(VolumeBuffer input);
}
