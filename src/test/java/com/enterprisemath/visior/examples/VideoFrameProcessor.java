package com.enterprisemath.visior.examples;

import java.awt.image.BufferedImage;

/**
 * Video frame processor.
 *
 * @author radek.hecl
 */
public interface VideoFrameProcessor {

    /**
     * Processes frame.
     *
     * @param in input image
     * @param t time in seconds from the beginning of the video
     * @return processed image
     */
    public BufferedImage process(BufferedImage in, double t);

}
