package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Matrix44;
import com.jogamp.common.nio.Buffers;
import static com.jogamp.opengl.GL.GL_ARRAY_BUFFER;
import static com.jogamp.opengl.GL.GL_COLOR_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_TEST;
import static com.jogamp.opengl.GL.GL_FLOAT;
import static com.jogamp.opengl.GL.GL_LEQUAL;
import static com.jogamp.opengl.GL.GL_STATIC_DRAW;
import static com.jogamp.opengl.GL.GL_TEXTURE0;
import static com.jogamp.opengl.GL.GL_TEXTURE_2D;
import static com.jogamp.opengl.GL.GL_TRIANGLES;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.Animator;
import java.nio.FloatBuffer;
import javax.swing.JFrame;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Test application.
 *
 * @author radek.hecl
 */
public final class JoglTest03 extends JFrame implements GLEventListener {

    /**
     * Canvas.
     */
    private GLCanvas canvas;

    /**
     * Reference to program.
     */
    private int renderingProgram;

    /**
     * Vertex array objects.
     */
    private int vao[] = new int[1];

    /**
     * Vertex buffer objects.
     */
    private int vbo[] = new int[2];

    /**
     * Brick texture.
     */
    private int brickTexture;

    /**
     * Buffer for binding uniform matrices to the graphics card.
     */
    private float[] matBuf = new float[16];

    /**
     * Executes program.
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        new JoglTest03();
    }

    /**
     * Creates new instance.
     */
    private JoglTest03() {
        setTitle("Test application");
        setSize(640, 480);
        setLocation(200, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        canvas = new GLCanvas();
        canvas.addGLEventListener(this);
        add(canvas);
        setVisible(true);
        Animator animtr = new Animator(canvas);
        animtr.start();
    }

    @Override
    public void display(GLAutoDrawable glad) {

        GL4 gl = (GL4) GLContext.getCurrentGL();
        gl.glClearColor(0f, 0f, 0f, 1f);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        gl.glUseProgram(renderingProgram);

        int mvLoc = gl.glGetUniformLocation(renderingProgram, "mv_matrix");
        int projLoc = gl.glGetUniformLocation(renderingProgram, "proj_matrix");

        float aspect = (float) canvas.getWidth() / (float) canvas.getHeight();
        Matrix44 pMat = GLUtils.createPerspective((float) Math.toRadians(60.0f), aspect, 0.1f, 1000.0f);
        Matrix44 mvMat = null;
        
        Matrix44 vMat = GLUtils.createTranslation(-1.0f, 0.0f, -2.0f);

        //
        // draw pyramid
        mvMat = vMat.mul(GLUtils.createTranslation(0.0f, 0.0f, 0.0f));

        gl.glUniformMatrix4fv(mvLoc, 1, false, mvMat.toBufCol(matBuf), 0);
        gl.glUniformMatrix4fv(projLoc, 1, false, pMat.toBufCol(matBuf), 0);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        gl.glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(0);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        gl.glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(1);

        gl.glActiveTexture(GL_TEXTURE0);
        gl.glBindTexture(GL_TEXTURE_2D, brickTexture);

        gl.glEnable(GL_DEPTH_TEST);
        gl.glDepthFunc(GL_LEQUAL);

        gl.glDrawArrays(GL_TRIANGLES, 0, 18);
    }

    @Override
    public void init(GLAutoDrawable glad) {
        try {
            renderingProgram = GLUtils.loadShaderProgram("src/test/resources/jogl/04-vertexShader.glsl", "src/test/resources/jogl/04-fragmentShader.glsl");
            setupVertices();
            brickTexture = GLUtils.loadTexture("src/test/resources/brick-1.jpg");
        } catch (RuntimeException e) {
            System.err.print(ExceptionUtils.getStackTrace(e));
            System.exit(1);
        }
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
        GL4 gl = (GL4) GLContext.getCurrentGL();
        gl.glBindVertexArray(vao[0]);
        gl.glDeleteBuffers(vbo.length, vbo, 0);
        gl.glDeleteVertexArrays(vao.length, vao, 0);
        gl.glDeleteProgram(renderingProgram);
        gl.glDeleteTextures(1, new int[] {brickTexture}, 0);
    }

    @Override
    public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {
    }

    /**
     * Sets up vertices.
     */
    private void setupVertices() {
        GL4 gl = (GL4) GLContext.getCurrentGL();

        float[] pyramidPositions = {
            -0.5f, -0.5f, 0.5f, 0.5f, -0.5f, 0.5f, 0.0f, 0.5f, 0.0f, //front
            0.5f, -0.5f, 0.5f, 0.5f, -0.5f, -0.5f, 0.0f, 0.5f, 0.0f, //right
            0.5f, -0.5f, -0.5f, -0.5f, -0.5f, -0.5f, 0.0f, 0.5f, 0.0f, //back
            -0.5f, -0.5f, -0.5f, -0.5f, -0.5f, 0.5f, 0.0f, 0.5f, 0.0f, //left
            -0.5f, -0.5f, -0.5f, 0.5f, -0.5f, 0.5f, -0.5f, -0.5f, 0.5f, //LF
            0.5f, -0.5f, 0.5f, -0.5f, -0.5f, -0.5f, 0.5f, -0.5f, -0.5f //RR
        };
        float[] pyramidTextureCoords = {
            0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 1.0f,
            0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 1.0f,
            0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
            1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f
        };

        gl.glGenVertexArrays(vao.length, vao, 0);
        gl.glBindVertexArray(vao[0]);
        gl.glGenBuffers(vbo.length, vbo, 0);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        FloatBuffer pyrBuf = Buffers.newDirectFloatBuffer(pyramidPositions);
        gl.glBufferData(GL_ARRAY_BUFFER, pyrBuf.limit() * 4, pyrBuf, GL_STATIC_DRAW);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        FloatBuffer texBuf = Buffers.newDirectFloatBuffer(pyramidTextureCoords);
        gl.glBufferData(GL_ARRAY_BUFFER, texBuf.limit() * 4, texBuf, GL_STATIC_DRAW);
    }

}
