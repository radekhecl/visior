package com.enterprisemath.visior.examples;

import com.xuggle.mediatool.IMediaWriter;
import com.xuggle.mediatool.ToolFactory;
import java.awt.image.BufferedImage;
import java.util.concurrent.TimeUnit;

/**
 * Class responsible for writing video.
 * Usage is simple, create a writer by factory method.
 * Then add images as you want. Finally close the writer.
 * Writer becomes useless after calling the close method.
 *
 * @author radek.hecl
 */
public class VideoWriter {

    /**
     * Media writer.
     */
    private IMediaWriter writer;

    /**
     * Creates new instance.
     */
    private VideoWriter() {
    }

    /**
     * Adds image to the stream. Image must have same resolution as initially set up.
     *
     * @param img image
     * @param t timestamp, in seconds
     */
    public void addImage(BufferedImage img, double t) {
        long ms = (long) (t * 1e6);
        writer.encodeVideo(0, img, ms, TimeUnit.MICROSECONDS);
    }

    /**
     * Closes the writer. Writer is useless after returning from this method.
     */
    public void close() {
        writer.close();
        writer = null;
    }

    /**
     * Creates new instance.
     *
     * @param filePath path to the target file
     * @param width width
     * @param height height
     * @return created writer
     */
    public static VideoWriter create(String filePath, int width, int height) {
        VideoWriter res = new VideoWriter();
        res.writer = ToolFactory.makeWriter(filePath);
        res.writer.addVideoStream(0, 0, width, height);
        return res;
    }
}
