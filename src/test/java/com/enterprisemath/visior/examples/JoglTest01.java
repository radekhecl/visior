package com.enterprisemath.visior.examples;

import static com.jogamp.opengl.GL.GL_COLOR_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_TRIANGLES;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.Animator;
import javax.swing.JFrame;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Test application.
 *
 * @author radek.hecl
 */
public final class JoglTest01 extends JFrame implements GLEventListener {

    /**
     * Canvas.
     */
    private GLCanvas canvas;

    /**
     * Variable x.
     */
    private float x;

    /**
     * Pointer to the rendering program.
     */
    private int renderingProgram = 0;

    /**
     * Vertex array object.
     */
    private int[] vao = new int[1];

    /**
     * Executes program.
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        new JoglTest01();
    }

    /**
     * Creates new instance.
     */
    private JoglTest01() {
        setTitle("Test application");
        setSize(640, 480);
        setLocation(200, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        canvas = new GLCanvas();
        canvas.addGLEventListener(this);
        add(canvas);
        setVisible(true);
        Animator animtr = new Animator(canvas);
        animtr.start();
    }

    @Override
    public void display(GLAutoDrawable glad) {
        x = (float) Math.sin(System.currentTimeMillis() / 1800.0);
        GL4 gl = (GL4) GLContext.getCurrentGL();
        gl.glClearColor(0f, 0f, 0f, 1f);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        int offsetLoc = gl.glGetUniformLocation(renderingProgram, "offset");
        gl.glProgramUniform1f(renderingProgram, offsetLoc, x);
        gl.glUseProgram(renderingProgram);
        gl.glDrawArrays(GL_TRIANGLES, 0, 3);
    }

    @Override
    public void init(GLAutoDrawable glad) {
        try {
            GL4 gl = (GL4) GLContext.getCurrentGL();
            renderingProgram = GLUtils.loadShaderProgram("src/test/resources/jogl/01-vertexShader.glsl", "src/test/resources/jogl/01-fragmentShader.glsl");
            gl.glGenVertexArrays(vao.length, vao, 0);
            gl.glBindVertexArray(vao[0]);
        } catch (RuntimeException e) {
            System.err.print(ExceptionUtils.getStackTrace(e));
            System.exit(1);
        }
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
            GL4 gl = (GL4) GLContext.getCurrentGL();
            gl.glDeleteVertexArrays(vao.length, vao, 0);
            gl.glDeleteProgram(renderingProgram);
    }

    @Override
    public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {
    }

}
