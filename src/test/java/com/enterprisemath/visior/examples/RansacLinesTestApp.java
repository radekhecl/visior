package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Drawing;
import com.enterprisemath.visior.Fitting;
import com.enterprisemath.visior.GeneralLine2;
import com.enterprisemath.visior.Images;
import com.enterprisemath.visior.Matrix34;
import com.enterprisemath.visior.RgbColor;
import com.enterprisemath.visior.Vector2;
import com.enterprisemath.visior.Vector4;
import com.enterprisemath.visior.VolumeBuffer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 * Test application to show the lines estimated by RANSAC.
 *
 * @author radek.hecl
 */
public class RansacLinesTestApp {

    /**
     * Test application.
     */
    private RansacLinesTestApp() {
    }

    /**
     * Main function.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        String outDir = "tmp/ransac";
        File dir = new File(outDir);
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                FileUtils.deleteQuietly(f);
            }
        }
        if (!dir.isDirectory()) {
            if (!dir.mkdirs()) {
                throw new RuntimeException("unable to set up parent dirctory");
            }
        }

        File srcDir = new File("src/test/resources/markers");
        for (File file : srcDir.listFiles()) {
            String fname = file.getName();
            if (!fname.endsWith(".png")) {
                continue;
            }
            String fbase = file.getName().split("\\.")[0];
            System.out.println("----");
            System.out.println("Procesing " + file.getName());

            //
            // preprocessing
            VolumeBuffer input = Images.load(file);
            WhiteMarkerTracker tracker = WhiteMarkerTracker.create();
            tracker.track(input);
            Images.save(input, outDir + "/" + fbase + "-out-0-orig.png");
            Images.save(tracker.getMask(), outDir + "/" + fbase + "-out-1-mask.png");
            Images.save(tracker.getBiggestBlob(), outDir + "/" + fbase + "-out-2-blob.png");
            BufferedImage contour = Images.createBuffered(input.getWidth(), input.getHeight());//Images.toBufferedImage(tracker.getBiggestBlob());
            Drawing.points(contour, tracker.getContourPts(), RgbColor.RED, 2, 1);
            Images.save(contour, outDir + "/" + fbase + "-out-3-contour.png");

            List<GeneralLine2> lines = Fitting.linesRansac(tracker.getContourPts(), 4, 1000, tracker.getContourPts().size() / 10, 5f);
            
            //
            // present results
            BufferedImage res = contour;
            for (GeneralLine2 line : lines) {
                Drawing.line(res, line, RgbColor.create(0.5f, 0.5f, 1f), 2);
            }
            Images.save(res, outDir + "/" + fbase + "-out-final.png");
        }

        System.out.println("-------------------");
        System.out.println("JOB DONE!");
        System.out.println("-------------------");

    }

    /**
     * Projects vector.
     *
     * @param p projection matrix
     * @param mv model view matrix
     * @param vec vector to project
     * @return projected vector
     */
    private static Vector2 project(Matrix34 p, Vector4 vec) {
        return p.mul(vec).dehomogenize();
    }

}
