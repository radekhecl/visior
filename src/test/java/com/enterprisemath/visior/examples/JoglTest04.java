package com.enterprisemath.visior.examples;

import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.GL;
import static com.jogamp.opengl.GL.GL_ARRAY_BUFFER;
import static com.jogamp.opengl.GL.GL_COLOR_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_BUFFER_BIT;
import static com.jogamp.opengl.GL.GL_DEPTH_TEST;
import static com.jogamp.opengl.GL.GL_FLOAT;
import static com.jogamp.opengl.GL.GL_LEQUAL;
import static com.jogamp.opengl.GL.GL_NEAREST;
import static com.jogamp.opengl.GL.GL_RGB;
import static com.jogamp.opengl.GL.GL_STATIC_DRAW;
import static com.jogamp.opengl.GL.GL_TEXTURE0;
import static com.jogamp.opengl.GL.GL_TEXTURE_2D;
import static com.jogamp.opengl.GL.GL_TEXTURE_MAG_FILTER;
import static com.jogamp.opengl.GL.GL_TEXTURE_MIN_FILTER;
import static com.jogamp.opengl.GL.GL_TRIANGLES;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.Animator;
import java.nio.FloatBuffer;
import javax.swing.JFrame;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Test application.
 *
 * @author radek.hecl
 */
public final class JoglTest04 extends JFrame implements GLEventListener {

    /**
     * Canvas.
     */
    private GLCanvas canvas;

    /**
     * Program for rendering background.
     */
    private int bgProgram;

    /**
     * Vertex array objects.
     */
    private int vao[] = new int[1];

    /**
     * Vertex buffer objects.
     */
    private int vbo[] = new int[2];

    /**
     * Background texture.
     */
    private int bgTexture;

    /**
     * Executes program.
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        new JoglTest04();
    }

    /**
     * Creates new instance.
     */
    private JoglTest04() {
        setTitle("Test application");
        setSize(660, 520);
        setLocation(200, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        canvas = new GLCanvas();
        canvas.addGLEventListener(this);
        add(canvas);
        setVisible(true);
        Animator animtr = new Animator(canvas);
        animtr.start();
    }

    @Override
    public void display(GLAutoDrawable glad) {

        GL4 gl = (GL4) GLContext.getCurrentGL();
        gl.glClearColor(0f, 0f, 0f, 1f);
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render pixels to texture
        long pix = System.currentTimeMillis() / 1000 % 256;
        gl.glBindTexture(GL_TEXTURE_2D, bgTexture);
        int texWidth = 16;
        int texHeight = 16;
        float[] colors = new float[16 * 16 * 3];
        for (int i = 0; i < 16 * 16; ++i) {
            if (i == pix) {
                colors[i * 3] = 0.0f;
                colors[i * 3 + 1] = 0.0f;
                colors[i * 3 + 2] = 0.0f;
            }
            else {
                colors[i * 3] = 1.0f;
                colors[i * 3 + 1] = 0.5f;
                colors[i * 3 + 2] = 0.3f;
            }
        }
        FloatBuffer texBuffer = Buffers.newDirectFloatBuffer(colors);
        gl.glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texWidth, texHeight, 0, GL_RGB, GL.GL_FLOAT, texBuffer);

        // background
        gl.glUseProgram(bgProgram);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        gl.glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(0);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        gl.glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
        gl.glEnableVertexAttribArray(1);

        gl.glActiveTexture(GL_TEXTURE0);
        gl.glBindTexture(GL_TEXTURE_2D, bgTexture);

        gl.glEnable(GL_DEPTH_TEST);
        gl.glDepthFunc(GL_LEQUAL);

        gl.glDrawArrays(GL_TRIANGLES, 0, 6);

    }

    @Override
    public void init(GLAutoDrawable glad) {
        try {
            bgProgram = GLUtils.loadShaderProgram("src/test/resources/jogl/05-vertexShader.glsl", "src/test/resources/jogl/05-fragmentShader.glsl");
            setupVertices();
            // generate texture
            //bgTexture = GLUtils.loadTexture(srcImage);
            GL4 gl = (GL4) GLContext.getCurrentGL();
            int[] tao = new int[1];
            gl.glGenTextures(1, tao, 0);
            bgTexture = tao[0];
            gl.glBindTexture(GL_TEXTURE_2D, bgTexture);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        } catch (RuntimeException e) {
            System.err.print(ExceptionUtils.getStackTrace(e));
            System.exit(1);
        }
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
        GL4 gl = (GL4) GLContext.getCurrentGL();
        gl.glBindVertexArray(vao[0]);
        gl.glDeleteBuffers(vbo.length, vbo, 0);
        gl.glDeleteVertexArrays(vao.length, vao, 0);
        gl.glDeleteProgram(bgProgram);
        gl.glDeleteTextures(1, new int[]{bgTexture}, 0);
    }

    @Override
    public void reshape(GLAutoDrawable glad, int i, int i1, int i2, int i3) {
    }

    /**
     * Sets up vertices.
     */
    private void setupVertices() {
        GL4 gl = (GL4) GLContext.getCurrentGL();

        float[] bgPositions = {
            -1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f, -1.0f, 1.0f, 0.0f
        };

        float[] bgTextureCoords = {
            0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,
            1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f
        };

        gl.glGenVertexArrays(vao.length, vao, 0);
        gl.glBindVertexArray(vao[0]);
        gl.glGenBuffers(vbo.length, vbo, 0);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        FloatBuffer bgBuf = Buffers.newDirectFloatBuffer(bgPositions);
        gl.glBufferData(GL_ARRAY_BUFFER, bgBuf.limit() * 4, bgBuf, GL_STATIC_DRAW);

        gl.glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        FloatBuffer bgTexBuf = Buffers.newDirectFloatBuffer(bgTextureCoords);
        gl.glBufferData(GL_ARRAY_BUFFER, bgTexBuf.limit() * 4, bgTexBuf, GL_STATIC_DRAW);
    }

}
