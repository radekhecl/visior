package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Aabb2;
import com.enterprisemath.visior.Blobs;
import com.enterprisemath.visior.Contour2;
import com.enterprisemath.visior.Fitting;
import com.enterprisemath.visior.GeneralLine2;
import com.enterprisemath.visior.MaskBuffer;
import com.enterprisemath.visior.PlaneBuffer;
import com.enterprisemath.visior.Vector2;
import com.enterprisemath.visior.VolumeBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Tracker for a white marker.
 * This tracker detects the biggest white marker. Then it can track it over time.
 * If there is significant change, then the model is abandoned and new tracking starts.
 *
 * @author radek.hecl
 */
public class WhiteMarkerTracker implements MarkerTracker {

    /**
     * Builder object.
     */
    public static class Builder {

        /**
         * Threshold for color.
         */
        private float colorThres = 0.65f;

        /**
         * Minimal volume of the blob.
         */
        private int minBlobVolume = 5000;

        /**
         * Max iterations for RANSAC during detection phase.
         */
        private int detectionRansacMaxIterations = 2000;

        /**
         * Factor for defining the minimal number of inliers during RANSAC detection.
         */
        private float detectionRansacMinInFactor = 0.1f;

        /**
         * Distance threshold for during RANSAC detection.
         */
        private float detectionRansacDstThres = 2f;

        /**
         * Expected maximal move of points during tracking.
         */
        private float trackingPointMoveR = 50f;

        /**
         * Minimal number of required points within the area to be able to track the line.
         */
        private int trackingMinLinePoints = 100;

        /**
         * Maximum angle difference to consider line being the same.
         */
        private float trackingMaxAngleDiff = (float) Math.PI / 6;

        /**
         * Maximal RANSAC iterations during tracking.
         */
        private int trackingRansacMaxIterations = 500;

        /**
         * Minimal number of RANSAC fits for line to be considered as trackable.
         */
        private int trackingRansacMinFits = 4;

        /**
         * Factor for defining the minimal number of inliers during RANSAC tracking.
         */
        private float trackingRansacMinInFactor = 0.3f;

        /**
         * Distance threshold for during RANSAC tracking.
         */
        private float trackingRansacDstThres = 2f;

        /**
         * Sets color threshold.
         *
         * @param colorThres color threshold
         * @return this instance
         */
        public Builder setColorThres(float colorThres) {
            this.colorThres = colorThres;
            return this;
        }

        /**
         * Sets minimal blob volume.
         *
         * @param minBlobVolume minimal blob volume
         * @return this instance
         */
        public Builder setMinBlobVolume(int minBlobVolume) {
            this.minBlobVolume = minBlobVolume;
            return this;
        }

        /**
         * Sets maximum number of iterations during RANSAC detection.
         *
         * @param detectionRansacMaxIterations maximum number of iterations during RANSAC detection
         * @return this instance
         */
        public Builder setDetectionRansacMaxIterations(int detectionRansacMaxIterations) {
            this.detectionRansacMaxIterations = detectionRansacMaxIterations;
            return this;
        }

        /**
         * Sets ratio of required inliers during RANSAC detection.
         *
         * @param detectionRansacMinInFactor ratio of required inliers during RANSAC detection
         * @return this instance
         */
        public Builder setDetectionRansacMinInFactor(float detectionRansacMinInFactor) {
            this.detectionRansacMinInFactor = detectionRansacMinInFactor;
            return this;
        }

        /**
         * Sets inlier distance threshold for RANSAC detection.
         *
         * @param detectionRansacDstThres inlier distance threshold for RANSAC detection
         * @return this instance
         */
        public Builder setDetectionRansacDstThres(float detectionRansacDstThres) {
            this.detectionRansacDstThres = detectionRansacDstThres;
            return this;
        }

        /**
         * Sets expected maximal point move during tracking.
         *
         * @param trackingPointMoveR expected maximal point move during tracking
         * @return this instance
         */
        public Builder setTrackingPointMoveR(float trackingPointMoveR) {
            this.trackingPointMoveR = trackingPointMoveR;
            return this;
        }

        /**
         * Sets minimal number of required points to be able track line.
         *
         * @param trackingMinLinePoints minimal number of required points to be able track line
         * @return this instance
         */
        public Builder setTrackingMinLinePoints(int trackingMinLinePoints) {
            this.trackingMinLinePoints = trackingMinLinePoints;
            return this;
        }

        /**
         * Sets maximal difference in angle for line to be considered tracked.
         *
         * @param trackingMaxAngleDiff maximal difference in angle for line to be considered tracked
         * @return this instance
         */
        public Builder setTrackingMaxAngleDiff(float trackingMaxAngleDiff) {
            this.trackingMaxAngleDiff = trackingMaxAngleDiff;
            return this;
        }

        /**
         * Sets maximal number of iterations for RANSAC tracking.
         *
         * @param trackingRansacMaxIterations maximal number of iterations for RANSAC tracking
         * @return this instance
         */
        public Builder setTrackingRansacMaxIterations(int trackingRansacMaxIterations) {
            this.trackingRansacMaxIterations = trackingRansacMaxIterations;
            return this;
        }

        /**
         * Sets minimal number of RANSAC fits to be able track the line.
         *
         * @param trackingRansacMinFits minimal number of RANSAC fits to be able track the line
         * @return this instance
         */
        public Builder setTrackingRansacMinFits(int trackingRansacMinFits) {
            this.trackingRansacMinFits = trackingRansacMinFits;
            return this;
        }

        /**
         * Sets ratio of required inliers during RANSAC tracking.
         *
         * @param trackingRansacMinInFactor ratio of required inliers during RANSAC tracking
         * @return this instance
         */
        public Builder setTrackingRansacMinInFactor(float trackingRansacMinInFactor) {
            this.trackingRansacMinInFactor = trackingRansacMinInFactor;
            return this;
        }

        /**
         * Sets inlier distance threshold for RANSAC tracking.
         *
         * @param trackingRansacDstThres inlier distance threshold for RANSAC tracking
         * @return this instance
         */
        public Builder setTrackingRansacDstThres(float trackingRansacDstThres) {
            this.trackingRansacDstThres = trackingRansacDstThres;
            return this;
        }

        /**
         * Builds the result object.
         *
         * @return created object
         */
        public WhiteMarkerTracker build() {
            return new WhiteMarkerTracker(this);
        }
    }

    /**
     * Threshold for color.
     */
    private float colorThres;

    /**
     * Minimal volume of the blob.
     */
    private int minBlobVolume;

    /**
     * Max iterations for RANSAC during detection phase.
     */
    private int detectionRansacMaxIterations;

    /**
     * Factor for defining the minimal number of inliers during RANSAC detection.
     */
    private float detectionRansacMinInFactor;

    /**
     * Distance threshold for during RANSAC detection.
     */
    private float detectionRansacDstThres;

    /**
     * Expected maximal move of points during tracking.
     */
    private float trackingPointMoveR;

    /**
     * Minimal number of required points within the area to be able to track the line.
     */
    private int trackingMinLinePoints;

    /**
     * Maximum angle difference to consider line being the same.
     */
    private float trackingMaxAngleDiff;

    /**
     * Maximal RANSAC iterations during tracking.
     */
    private int trackingRansacMaxIterations;

    /**
     * Minimal number of RANSAC fits for line to be considered as trackable.
     */
    private int trackingRansacMinFits;

    /**
     * Factor for defining the minimal number of inliers during RANSAC tracking.
     */
    private float trackingRansacMinInFactor;

    /**
     * Distance threshold for during RANSAC tracking.
     */
    private float trackingRansacDstThres;

    /**
     * Mask.
     */
    private MaskBuffer mask;

    /**
     * Buffer for blobs.
     */
    private PlaneBuffer blobs;

    /**
     * Mask of the biggest blob.
     */
    private MaskBuffer biggestBlob;

    /**
     * Contour points.
     */
    private List<Vector2> contourPts;

    /**
     * Random number generator.
     */
    private Random random = new Random();

    /**
     * Marker. If this is not null, then tracking is happening.
     * Otherwise detection is happening.
     */
    private Contour2 marker = null;

    /**
     * Creates new instance.
     *
     * @param builder builder object
     */
    private WhiteMarkerTracker(Builder builder) {
        this.colorThres = builder.colorThres;
        this.minBlobVolume = builder.minBlobVolume;
        this.detectionRansacMaxIterations = builder.detectionRansacMaxIterations;
        this.detectionRansacMinInFactor = builder.detectionRansacMinInFactor;
        this.detectionRansacDstThres = builder.detectionRansacDstThres;
        this.trackingPointMoveR = builder.trackingPointMoveR;
        this.trackingMinLinePoints = builder.trackingMinLinePoints;
        this.trackingMaxAngleDiff = builder.trackingMaxAngleDiff;
        this.trackingRansacMaxIterations = builder.trackingRansacMaxIterations;
        this.trackingRansacMinFits = builder.trackingRansacMinFits;
        this.trackingRansacMinInFactor = builder.trackingRansacMinInFactor;
        this.trackingRansacDstThres = builder.trackingRansacDstThres;
        guardInvarinats();
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvarinats() {
    }

    @Override
    public Contour2 track(VolumeBuffer input) {
        if (mask == null || mask.getWidth() != input.getWidth() || mask.getHeight() != input.getHeight()) {
            mask = MaskBuffer.create(input.getWidth(), input.getHeight());
            blobs = PlaneBuffer.create(input.getWidth(), input.getHeight());
            biggestBlob = MaskBuffer.create(input.getWidth(), input.getHeight());
        }
        float[] rbuf = input.getPlane(0).getBuf();
        float[] gbuf = input.getPlane(1).getBuf();
        float[] bbuf = input.getPlane(2).getBuf();
        boolean[] maskBuf = mask.getBuf();
        int dim = rbuf.length;
        for (int i = 0; i < dim; ++i) {
            if (rbuf[i] < colorThres || gbuf[i] < colorThres || bbuf[i] < colorThres) {
                maskBuf[i] = false;
            }
            else {
                maskBuf[i] = true;
            }
        }
        int numBlobs = Blobs.blob4(mask, blobs);
        if (numBlobs == 0) {
            marker = null;
            return null;
        }
        Map<Integer, Integer> blobVols = getBlobVolumes(blobs, numBlobs);
        int biggestBlobIdx = 0;
        int biggestBlobVol = blobVols.get(0);
        for (int idx : blobVols.keySet()) {
            if (blobVols.get(idx) > biggestBlobVol) {
                biggestBlobIdx = idx;
                biggestBlobVol = blobVols.get(idx);
            }
        }
        if (biggestBlobVol < minBlobVolume) {
            marker = null;
            return null;
        }
        Blobs.pickBlob(blobs, biggestBlobIdx, biggestBlob);
        contourPts = getBlobContour(biggestBlob);

        // track marker if presented
        if (marker != null) {
            marker = trackMarker(contourPts, marker);
        }

        // run detection if marker is not presented.
        if (marker == null) {
            List<GeneralLine2> lines = Fitting.linesRansac(contourPts, 4, detectionRansacMaxIterations,
                    (int) (contourPts.size() * detectionRansacMinInFactor), detectionRansacDstThres);
            if (lines.size() != 4) {
                return null;
            }
            List<Vector2> pts = extractPoints(lines);
            if (pts == null) {
                return null;
            }
            marker = Contour2.create(pts);
            marker = trackMarker(contourPts, marker);
        }

        return marker;
    }

    /**
     * Returns mask.
     *
     * @return mask
     */
    public MaskBuffer getMask() {
        return mask;
    }

    /**
     * Returns blobs.
     *
     * @return blobs
     */
    public PlaneBuffer getBlobs() {
        return blobs;
    }

    /**
     * Returns biggest blob.
     *
     * @return biggest blob
     */
    public MaskBuffer getBiggestBlob() {
        return biggestBlob;
    }

    /**
     * Returns contour points.
     *
     * @return contour points
     */
    public List<Vector2> getContourPts() {
        return contourPts;
    }

    /**
     * Performs marker tracking.
     *
     * @param contourPts contour points
     * @param marker marker in the old position
     * @return updated marker, or null if marker has been lost
     */
    private Contour2 trackMarker(List<Vector2> contourPts, Contour2 marker) {
        GeneralLine2 line1 = trackLine(contourPts, marker.getPoint(0), marker.getPoint(1),
                trackingPointMoveR, trackingMinLinePoints, trackingMaxAngleDiff, trackingRansacMaxIterations, trackingRansacMinFits,
                trackingRansacMinInFactor, trackingRansacDstThres);
        if (line1 == null) {
            return null;
        }
        GeneralLine2 line2 = trackLine(contourPts, marker.getPoint(1), marker.getPoint(2),
                trackingPointMoveR, trackingMinLinePoints, trackingMaxAngleDiff, trackingRansacMaxIterations, trackingRansacMinFits,
                trackingRansacMinInFactor, trackingRansacDstThres);
        if (line2 == null) {
            return null;
        }
        GeneralLine2 line3 = trackLine(contourPts, marker.getPoint(2), marker.getPoint(3),
                trackingPointMoveR, trackingMinLinePoints, trackingMaxAngleDiff, trackingRansacMaxIterations, trackingRansacMinFits,
                trackingRansacMinInFactor, trackingRansacDstThres);
        if (line3 == null) {
            return null;
        }
        GeneralLine2 line4 = trackLine(contourPts, marker.getPoint(3), marker.getPoint(0),
                trackingPointMoveR, trackingMinLinePoints, trackingMaxAngleDiff, trackingRansacMaxIterations, trackingRansacMinFits,
                trackingRansacMinInFactor, trackingRansacDstThres);
        if (line4 == null) {
            return null;
        }

        Vector2 p1 = line4.intersection(line1);
        Vector2 p2 = line1.intersection(line2);
        Vector2 p3 = line2.intersection(line3);
        Vector2 p4 = line3.intersection(line4);
        if (p1 == null || p2 == null || p3 == null || p4 == null) {
            return null;
        }
        Contour2 candMarker = Contour2.create(p1, p2, p3, p4);
        if (!isReasonable(candMarker)) {
            return null;
        }

        return candMarker;
    }

    /**
     * Returns volumes of all blobs.
     *
     * @param in plane with blobs
     * @param numBlobs number of blobs
     * @return map with blob volumes, having keys 0, 1, 2, 3 ... numbBlobs - 1
     */
    private Map<Integer, Integer> getBlobVolumes(PlaneBuffer in, int numBlobs) {
        int[] sizes = new int[numBlobs];
        float[] inbuf = in.getBuf();
        for (int idx = 0; idx < inbuf.length; ++idx) {
            int f = (int) inbuf[idx];
            if (f != -1) {
                sizes[f] = sizes[f] + 1;
            }
        }
        Map<Integer, Integer> res = new HashMap<>();
        for (int i = 0; i < numBlobs; ++i) {
            res.put(i, sizes[i]);
        }
        return res;
    }

    /**
     * Returns list of blob contour points.
     * Contour point is the one which is true and has at least 1 of the 4 neighbors (up, down, left, right) false.
     * Border points (most top, bottom, left, right) of the input are marked as not evaluated.
     *
     * @param in input mask
     * @return list of points, order of points is not guaranteed
     */
    private List<Vector2> getBlobContour(MaskBuffer in) {
        List<Vector2> res = new ArrayList<>();
        boolean[] inbuf = in.getBuf();
        int w = in.getWidth();
        int h = in.getHeight();
        for (int y = 1, ii = w; y < h - 1; ++y) {
            ++ii;
            for (int x = 1; x < w - 1; ++x, ++ii) {
                if (inbuf[ii]) {
                    if (inbuf[ii - w] && inbuf[ii + w] && inbuf[ii - 1] && inbuf[ii + 1]) {
                        continue;
                    }
                    else {
                        res.add(Vector2.create(x, y));
                    }
                }
            }
            ++ii;
        }
        return res;
    }

    /**
     * Extracts points.
     *
     * @param lines array of lines, must have 4 elements
     * @return point forming the rectangle, or null if some validation is broken (e.g. some of the points are too close)
     */
    private static List<Vector2> extractPoints(List<GeneralLine2> lines) {
        GeneralLine2 l1 = lines.get(0);
        GeneralLine2 l2 = lines.get(1);
        GeneralLine2 l3 = lines.get(2);
        GeneralLine2 l4 = lines.get(3);
        float th1 = l1.angle(l2);
        float th2 = l1.angle(l3);
        float th3 = l1.angle(l4);
        Vector2 p1 = null;
        Vector2 p2 = null;
        Vector2 p3 = null;
        Vector2 p4 = null;
        if (th1 < th2 && th1 < th3) {
            p1 = l1.intersection(l3);
            p2 = l1.intersection(l4);
            p3 = l2.intersection(l3);
            p4 = l2.intersection(l4);
        }
        else if (th2 < th3) {
            p1 = l1.intersection(l2);
            p2 = l1.intersection(l4);
            p3 = l3.intersection(l2);
            p4 = l3.intersection(l4);
        }
        else {
            p1 = l1.intersection(l2);
            p2 = l1.intersection(l3);
            p3 = l4.intersection(l2);
            p4 = l4.intersection(l3);
        }
        if (p1 == null || p2 == null || p3 == null || p4 == null) {
            return null;
        }
        List<Vector2> pts = Arrays.asList(p1, p2, p3, p4);
        final Vector2 center = Vector2.avg(pts);
        List<ObjectScalarPair<Vector2>> scoredPts = pts.stream().map(a -> {
            Vector2 norm = a.sub(center).normalize();
            if (norm.getX() >= 0) {
                return ObjectScalarPair.create(a, -norm.getY());
            }
            else {
                return ObjectScalarPair.create(a, norm.getY() + 2);
            }
        }).collect(Collectors.toList());
        scoredPts.sort((a, b) -> Float.compare(a.getScalar(), b.getScalar()));
        p1 = scoredPts.get(0).getObj();
        p2 = scoredPts.get(1).getObj();
        p3 = scoredPts.get(2).getObj();
        p4 = scoredPts.get(3).getObj();
        // make sure the shorter edge goes first (corresponds to the shorter edge on A4 paper)
        if (p1.dist(p2) < p2.dist(p3)) {
            return Arrays.asList(p1, p2, p3, p4);
        }
        else {
            return Arrays.asList(p2, p3, p4, p1);
        }
    }

    /**
     * Tracks the line.
     *
     * @param pts points to select from
     * @param prevStart start point of the previous line
     * @param prevEnd end point of the previous line
     * @param aabbDiff acceptable tolerance for AABB boundary to picking up points
     * @param minAabb minimum number of points which needs to be in the created AABB box
     * @param maxAngleDiff acceptable difference in line angle
     * @param maxRansacIterations maximal number of iterations allowed to RANSAC, before giving up
     * @param minRansacFits minimum fits required by RANSAC, before stopping
     * @param minAcceptFactor factor to calculate the number of required inliers, this multiplies number of points within the constructed AABB
     * @param dstThres threshold of distance for point to be considered as on line
     * @return line in the new position, null if the line has been lost
     */
    private GeneralLine2 trackLine(List<Vector2> pts, Vector2 prevStart, Vector2 prevEnd, float aabbDiff, int minAabb, float maxAngleDiff,
            int maxRansacIterations, int minRansacFits, float minAcceptFactor, float dstThres) {
        GeneralLine2 orig = GeneralLine2.create(prevStart, prevEnd);
        Vector2 min = Vector2.create((float) Math.min(prevStart.getX(), prevEnd.getX()) - aabbDiff, (float) Math.min(prevStart.getY(), prevEnd.getY()) - aabbDiff);
        Vector2 max = Vector2.create((float) Math.max(prevStart.getX(), prevEnd.getX()) + aabbDiff, (float) Math.max(prevStart.getY(), prevEnd.getY()) + aabbDiff);
        Aabb2 aabb = Aabb2.create(min, max.sub(min));
        List<Vector2> inbox = new ArrayList<>();
        for (Vector2 pt : pts) {
            if (aabb.contains(pt)) {
                inbox.add(pt);
            }
        }
        if (inbox.size() < minAabb) {
            return null;
        }
        int minAccept = (int) (inbox.size() * minAcceptFactor);
        Set<Vector2> neighbours = new HashSet<>();
        int numRansacFits = 0;
        for (int i = 0; i < maxRansacIterations && numRansacFits < minRansacFits; ++i) {
            // pickup random points and make sure they are not the same
            Vector2 start = inbox.get(random.nextInt(inbox.size()));
            Vector2 end = inbox.get(random.nextInt(inbox.size()));
            while (start.equals(end)) {
                end = inbox.get(random.nextInt(inbox.size()));
            }
            // construct line and verify how many points are fitting into it
            GeneralLine2 prospect = GeneralLine2.create(start, end);
            if (prospect.angle(orig) > maxAngleDiff) {
                continue;
            }
            List<Vector2> inliers = new ArrayList<>();
            for (Vector2 pt : pts) {
                if (prospect.dist(pt) <= dstThres) {
                    inliers.add(pt);
                }
            }
            if (inliers.size() < minAccept) {
                continue;
            }
            ++numRansacFits;
            neighbours.addAll(inliers);
        }
        if (numRansacFits < minRansacFits) {
            return null;
        }
        GeneralLine2 line = Fitting.lineLeastSqr(neighbours);
        return line;
    }

    /**
     * Returns whether the specified marker seems to be reasonable or not.
     *
     * @param marker marker
     * @return true if marker is reasonable, false otherwise
     */
    private boolean isReasonable(Contour2 marker) {
        // check that the shape
        Vector2 p1p2 = marker.getPoint(1).sub(marker.getPoint(0));
        Vector2 p2p3 = marker.getPoint(2).sub(marker.getPoint(1));
        Vector2 p3p4 = marker.getPoint(3).sub(marker.getPoint(2));
        Vector2 p4p1 = marker.getPoint(0).sub(marker.getPoint(3));
        if (crossZ(p2p3, p1p2) <= 0 || crossZ(p3p4, p2p3) <= 0 || crossZ(p4p1, p3p4) <= 0 || crossZ(p1p2, p4p1) <= 0) {
            return false;
        }

        // check that the parallel lines are not too much different in length
        float ratio1 = p1p2.mag() / p3p4.mag();
        float ratio2 = p2p3.mag() / p4p1.mag();
        if (ratio1 < 0.5f || ratio1 > 2f || ratio2 < 0.5f || ratio2 > 2f) {
            return false;
        }

        return true;
    }

    /**
     * Calculates z component of the cross product, adding 0 as a third component to each vector.
     *
     * @param a vector a
     * @param b vector b
     * @return cross product size
     */
    private float crossZ(Vector2 a, Vector2 b) {
        return a.getX() * b.getY() - a.getY() * b.getX();
    }

    /**
     * Creates new instance.
     *
     * @return created instance
     */
    public static WhiteMarkerTracker create() {
        return new WhiteMarkerTracker.Builder().
                build();
    }
}
