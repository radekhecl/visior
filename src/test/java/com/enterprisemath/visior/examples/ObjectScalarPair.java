package com.enterprisemath.visior.examples;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Defines object together with the scalar value.
 *
 * @author radek.hecl
 * @param <T> object type
 */
public class ObjectScalarPair<T> {

    /**
     * Object.
     */
    private T obj;

    /**
     * Scalar value.
     */
    private float scalar;

    /**
     * Creates new instance.
     */
    private ObjectScalarPair() {
    }

    /**
     * Guards this object to be consistent.
     */
    private void guardInvariants() {
    }

    /**
     * Returns object.
     *
     * @return object
     */
    public T getObj() {
        return obj;
    }

    /**
     * Returns scalar.
     *
     * @return scalar
     */
    public float getScalar() {
        return scalar;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param <T> object type
     * @param obj object
     * @param scalar scalar
     * @return created instance
     */
    public static <T> ObjectScalarPair<T> create(T obj, float scalar) {
        ObjectScalarPair<T> res = new ObjectScalarPair<>();
        res.obj = obj;
        res.scalar = scalar;
        res.guardInvariants();
        return res;
    }

}
