package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Matrix44;
import com.jogamp.opengl.GL;
import static com.jogamp.opengl.GL.GL_LINEAR_MIPMAP_LINEAR;
import static com.jogamp.opengl.GL.GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT;
import static com.jogamp.opengl.GL.GL_NO_ERROR;
import static com.jogamp.opengl.GL.GL_TEXTURE_2D;
import static com.jogamp.opengl.GL.GL_TEXTURE_MAX_ANISOTROPY_EXT;
import static com.jogamp.opengl.GL.GL_TEXTURE_MIN_FILTER;
import static com.jogamp.opengl.GL2ES2.GL_COMPILE_STATUS;
import static com.jogamp.opengl.GL2ES2.GL_FRAGMENT_SHADER;
import static com.jogamp.opengl.GL2ES2.GL_INFO_LOG_LENGTH;
import static com.jogamp.opengl.GL2ES2.GL_LINK_STATUS;
import static com.jogamp.opengl.GL2ES2.GL_VERTEX_SHADER;
import com.jogamp.opengl.GL4;
import com.jogamp.opengl.GLContext;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.TextureIO;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;

/**
 * Utility class for working out with OpenGL.
 *
 * @author radek.hecl
 */
public class GLUtils {

    /**
     * Prevents construction.
     */
    private GLUtils() {
    }

    /**
     * Loads shader program.
     *
     * @param vertexShaderPath path to the vertex shader
     * @param fragmentShaderPath path to the fragment shader
     * @return reference to the program
     */
    public static int loadShaderProgram(String vertexShaderPath, String fragmentShaderPath) {
        GL4 gl = (GL4) GLContext.getCurrentGL();
        int vertexShader = loadShader(GL_VERTEX_SHADER, vertexShaderPath);
        int fragmentShader = loadShader(GL_FRAGMENT_SHADER, fragmentShaderPath);
        int program = gl.glCreateProgram();
        gl.glAttachShader(program, vertexShader);
        gl.glAttachShader(program, fragmentShader);
        gl.glLinkProgram(program);
        guardError("program linking");
        int[] linked = new int[1];
        gl.glGetProgramiv(program, GL_LINK_STATUS, linked, 0);
        if (linked[0] != 1) {
            throw new IllegalStateException("error in linking program: : " + getProgramLog(program));
        }
        gl.glDeleteShader(vertexShader);
        gl.glDeleteShader(fragmentShader);
        return program;
    }

    /**
     * Loads texture.
     *
     * @param path file path
     * @return texture reference
     */
    public static int loadTexture(String path) {
        GL4 gl = (GL4) GLContext.getCurrentGL();
        int finalTextureRef;
        Texture tex = null;
        try {
            tex = TextureIO.newTexture(new File(path), false);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        finalTextureRef = tex.getTextureObject(gl);

        // building a mipmap and use anisotropic filtering
        gl.glBindTexture(GL_TEXTURE_2D, finalTextureRef);
        gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        gl.glGenerateMipmap(GL.GL_TEXTURE_2D);
        if (gl.isExtensionAvailable("GL_EXT_texture_filter_anisotropic")) {
            float anisoset[] = new float[1];
            gl.glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, anisoset, 0);
            gl.glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisoset[0]);
        }
        return finalTextureRef;
    }

    /**
     * Creates perspective projection matrix.
     * This method doesn't support "infinite projection matrix". Details about this topic are at
     * {@link http://www.terathon.com/gdc07_lengyel.pdf}.
     *
     * @param fovy field of view on y (vertical axis), must be strictly within interval [0, PI]
     * @param aspect aspect ratio (defined as width / height), must be greater than zero
     * @param nearz near clipping plane distance
     * @param farz far clipping plane distance
     * @return created matrix
     */
    public static Matrix44 createPerspective(float fovy, float aspect, float nearz, float farz) {
        float h = (float) Math.tan(fovy * 0.5f);
        return Matrix44.create(
                1.0f / (h * aspect), 0.0f, 0.0f, 0.0f,
                0.0f, 1.0f / h, 0.0f, 0.0f,
                0.0f, 0.0f, (nearz + farz) / (nearz - farz), 2 * nearz * farz / (nearz - farz),
                0.0f, 0.0f, -1.0f, 0.0f);
    }

    /**
     * Creates translation matrix.
     *
     * @param x x direction
     * @param y y direction
     * @param z z direction
     * @return created matrix
     */
    public static Matrix44 createTranslation(float x, float y, float z) {
        return Matrix44.create(
                1.0f, 0.0f, 0.0f, x,
                0.0f, 1.0f, 0.0f, y,
                0.0f, 0.0f, 1.0f, z,
                0.0f, 0.0f, 0.0f, 1.0f);
    }

    /**
     * Load shader.
     *
     * @param type shader type
     * @param path file path
     * @return shader reference
     */
    private static int loadShader(int type, String path) {
        try {
            GL4 gl = (GL4) GLContext.getCurrentGL();
            String[] source = FileUtils.readLines(new File(path), "UTF-8").toArray(new String[0]);
            for (int i = 0; i < source.length; ++i) {
                source[i] = source[i] + "\n";
            }
            int shader = gl.glCreateShader(type);
            gl.glShaderSource(shader, source.length, source, null, 0);
            gl.glCompileShader(shader);
            guardError(path);
            int[] shaderCompiled = new int[1];
            gl.glGetShaderiv(shader, GL_COMPILE_STATUS, shaderCompiled, 0);
            if (shaderCompiled[0] != 1) {
                throw new IllegalStateException("error in compiling shader " + path + ": " + getShaderLog(shader));
            }
            return shader;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Guards for the error. Throws an exception if there was an OpenGL error.
     *
     * @param msg message
     */
    private static void guardError(String msg) {
        GL4 gl = (GL4) GLContext.getCurrentGL();
        GLU glu = new GLU();
        int glErr = gl.glGetError();
        while (glErr != GL_NO_ERROR) {
            glErr = gl.glGetError();
            throw new IllegalStateException("OpenGL error: " + msg + "; " + glu.gluErrorString(glErr));
        }
    }

    /**
     * Returns shader log.
     *
     * @param shader shader reference
     * @return shader log
     */
    private static String getShaderLog(int shader) {
        StringBuilder res = new StringBuilder();
        GL4 gl = (GL4) GLContext.getCurrentGL();
        int[] len = new int[1];
        int[] chWrittn = new int[1];
        byte[] log = null;
        gl.glGetShaderiv(shader, GL_INFO_LOG_LENGTH, len, 0);
        if (len[0] > 0) {
            log = new byte[len[0]];
            gl.glGetShaderInfoLog(shader, len[0], chWrittn, 0, log, 0);
            for (int i = 0; i < log.length; i++) {
                res.append((char) log[i]);
            }
        }
        return res.toString();
    }

    /**
     * Returns program log.
     *
     * @param program program reference
     * @return program log
     */
    private static String getProgramLog(int program) {
        StringBuilder res = new StringBuilder();
        GL4 gl = (GL4) GLContext.getCurrentGL();
        int[] len = new int[1];
        int[] chWrittn = new int[1];
        byte[] log = null;
        gl.glGetProgramiv(program, GL_INFO_LOG_LENGTH, len, 0);
        if (len[0] > 0) {
            log = new byte[len[0]];
            gl.glGetProgramInfoLog(program, len[0], chWrittn, 0, log, 0);
            for (int i = 0; i < log.length; i++) {
                res.append((char) log[i]);
            }
        }
        return res.toString();
    }

}
