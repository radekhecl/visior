package com.enterprisemath.visior.examples;

import com.enterprisemath.visior.Ar;
import com.enterprisemath.visior.Contour2;
import com.enterprisemath.visior.CorrespondingPair;
import com.enterprisemath.visior.Drawing;
import com.enterprisemath.visior.Images;
import com.enterprisemath.visior.Matrix33;
import com.enterprisemath.visior.Matrix34;
import com.enterprisemath.visior.RgbColor;
import com.enterprisemath.visior.Vector2;
import com.enterprisemath.visior.Vector4;
import com.enterprisemath.visior.VolumeBuffer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.io.FileUtils;

/**
 * Test application.
 *
 * @author radek.hecl
 */
public class CameraPoseVideoTestApp {

    /**
     * Test application.
     */
    private CameraPoseVideoTestApp() {
    }

    /**
     * Main function.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        String outDir = "tmp/markers";
        String fileName = "paper-1.mp4";
        File dir = new File(outDir);
        if (dir.isDirectory()) {
            for (File f : dir.listFiles()) {
                FileUtils.deleteQuietly(f);
            }
        }
        if (!dir.isDirectory()) {
            if (!dir.mkdirs()) {
                throw new RuntimeException("unable to set up parent dirctory");
            }
        }
        AtomicInteger last = new AtomicInteger(0);
        String inpath = "src/test/resources/markers/" + fileName;
        String outpath = outDir + "/" + fileName;
        final WhiteMarkerTracker tracker = WhiteMarkerTracker.create();
        VideoUtils.processVideoImages(inpath, outpath, (img, t) -> {
            //
            // preprocessing
            VolumeBuffer input = Images.create(img);
            Contour2 marker = tracker.track(input);
            BufferedImage res = Images.toBuffered(input);
            if (marker != null) {
                Drawing.contour(res, marker, RgbColor.GREEN, 5);
                float f = 800;
                float cx = input.getWidth() / 2f;
                float cy = input.getHeight() / 2f;
                float s = 0f;
                float sx3d = 29.7f;
                float sy3d = 21.0f;
                Matrix33 k = Matrix33.create(f, s, cx, 0, f, cy, 0, 0, 1);
                Matrix33 kinv = k.inv();
                List<CorrespondingPair<Vector2, Vector2>> corrs = Arrays.asList(
                        CorrespondingPair.create(Vector2.create(-sx3d / 2, -sy3d / 2), marker.getPoint(0)),
                        CorrespondingPair.create(Vector2.create(-sx3d / 2, sy3d / 2), marker.getPoint(1)),
                        CorrespondingPair.create(Vector2.create(sx3d / 2, sy3d / 2), marker.getPoint(2)),
                        CorrespondingPair.create(Vector2.create(sx3d / 2, -sy3d / 2), marker.getPoint(3)));
                Matrix34 mv = Ar.estimateMvMatrix(kinv, corrs);
                Matrix34 p = k.mul(mv);
                //
                // present results
                drawCube(res, p, (float) Math.min(sx3d, sy3d) / 2f);
            }

            if (t > last.get() + 1) {
                Images.save(res, outDir + "/image-" + last.get() + ".png");
                System.out.println("Processed " + last.incrementAndGet() + " seconds");
            }
            return res;
        });

        System.out.println("-------------------");
        System.out.println("JOB DONE!");
        System.out.println("-------------------");

    }

    /**
     * Draws cube.
     *
     * @param img image
     * @param p projection matrix
     * @param mv model view matrix
     */
    private static void drawCube(BufferedImage img, Matrix34 p, float size) {
        float halfs = size / 2;
        Vector2 v1 = project(p, Vector4.create(-halfs, -halfs, 0f, 1f));
        Vector2 v2 = project(p, Vector4.create(-halfs, halfs, 0f, 1f));
        Vector2 v3 = project(p, Vector4.create(halfs, halfs, 0f, 1f));
        Vector2 v4 = project(p, Vector4.create(halfs, -halfs, 0f, 1f));
        Vector2 v5 = project(p, Vector4.create(-halfs, -halfs, size, 1f));
        Vector2 v6 = project(p, Vector4.create(-halfs, halfs, size, 1f));
        Vector2 v7 = project(p, Vector4.create(halfs, halfs, size, 1f));
        Vector2 v8 = project(p, Vector4.create(halfs, -halfs, size, 1f));
        Drawing.line(img, v1, v2, RgbColor.BLUE, 4);
        Drawing.line(img, v2, v3, RgbColor.BLUE, 4);
        Drawing.line(img, v3, v4, RgbColor.BLUE, 4);
        Drawing.line(img, v4, v1, RgbColor.BLUE, 4);
        Drawing.line(img, v5, v6, RgbColor.BLUE, 4);
        Drawing.line(img, v6, v7, RgbColor.BLUE, 4);
        Drawing.line(img, v7, v8, RgbColor.BLUE, 4);
        Drawing.line(img, v8, v5, RgbColor.BLUE, 4);
        Drawing.line(img, v1, v5, RgbColor.BLUE, 4);
        Drawing.line(img, v2, v6, RgbColor.BLUE, 4);
        Drawing.line(img, v3, v7, RgbColor.BLUE, 4);
        Drawing.line(img, v4, v8, RgbColor.BLUE, 4);
    }

    /**
     * Projects vector.
     *
     * @param p projection matrix
     * @param mv model view matrix
     * @param vec vector to project
     * @return projected vector
     */
    private static Vector2 project(Matrix34 p, Vector4 vec) {
        return p.mul(vec).dehomogenize();
    }

}
