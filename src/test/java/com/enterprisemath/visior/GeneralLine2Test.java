package com.enterprisemath.visior;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Tests the general line.
 *
 * @author radek.hecl
 */
public class GeneralLine2Test {

    /**
     * Tests intersection between 2 lines.
     */
    @Test
    public void testIntersection() {
        GeneralLine2 line1 = null;
        GeneralLine2 line2 = null;
        Vector2 expected = null;
        Vector2 res = null;

        //
        line1 = GeneralLine2.create(Vector2.create(0, 1), Vector2.create(1, 0));
        line2 = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(1, 1));
        expected = Vector2.create(0.5f, 0.5f);
        res = line1.intersection(line2);
        assertTrue(res.toString(), expected.dist(res) < 0.001f);
        res = line2.intersection(line1);
        assertTrue(res.toString(), expected.dist(res) < 0.001f);

        assertNull(line1.intersection(line1));
        assertNull(line2.intersection(line2));

        //
        line1 = GeneralLine2.create(Vector2.create(0, 0.5f), Vector2.create(1, 0.5f));
        line2 = GeneralLine2.create(Vector2.create(0.5f, 0), Vector2.create(0.5f, 1));
        expected = Vector2.create(0.5f, 0.5f);
        res = line1.intersection(line2);
        assertTrue(res.toString(), expected.dist(res) < 0.001f);
        res = line2.intersection(line1);
        assertTrue(res.toString(), expected.dist(res) < 0.001f);

        assertNull(line1.intersection(line1));
        assertNull(line2.intersection(line2));

        //
        line1 = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(0, 1));
        line2 = GeneralLine2.create(Vector2.create(1, 0), Vector2.create(1, -1));

        assertNull(line1.intersection(line2));
        assertNull(line2.intersection(line1));
        assertNull(line1.intersection(line1));
        assertNull(line2.intersection(line2));

        //
        line1 = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(0, 1));
        line2 = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(0, -1));

        assertNull(line1.intersection(line2));
        assertNull(line2.intersection(line1));
        assertNull(line1.intersection(line1));
        assertNull(line2.intersection(line2));

        //
        line1 = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(1, 1));
        line2 = GeneralLine2.create(Vector2.create(0, 1), Vector2.create(1, 2));

        assertNull(line1.intersection(line2));
        assertNull(line2.intersection(line1));
        assertNull(line1.intersection(line1));
        assertNull(line2.intersection(line2));
    }

    /**
     * Tests angle between 2 lines.
     */
    @Test
    public void testAngle() {
        GeneralLine2 line1 = null;
        GeneralLine2 line2 = null;

        //
        line1 = GeneralLine2.create(Vector2.create(0, 1), Vector2.create(1, 0));
        line2 = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(1, 1));
        assertEquals(Math.PI / 2, line1.angle(line2), 0.001);
        assertEquals(Math.PI / 2, line2.angle(line1), 0.001);
        
        //
        line1 = GeneralLine2.create(Vector2.create(0, 0.5f), Vector2.create(1, 0.5f));
        line2 = GeneralLine2.create(Vector2.create(0.5f, 0), Vector2.create(0.5f, 1));
        assertEquals(Math.PI / 2, line1.angle(line2), 0.001);
        assertEquals(Math.PI / 2, line2.angle(line1), 0.001);

        //
        line1 = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(1, 1));
        line2 = GeneralLine2.create(Vector2.create(1, 1), Vector2.create(1, 2));
        assertEquals(Math.PI / 4, line1.angle(line2), 0.001);
        assertEquals(Math.PI / 4, line2.angle(line1), 0.001);
        
        //
        line1 = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(0, 1));
        line2 = GeneralLine2.create(Vector2.create(1, 0), Vector2.create(1, -1));
        assertEquals(0d, line1.angle(line2), 0.001);
        assertEquals(0d, line2.angle(line1), 0.001);
        
        //
        line1 = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(1, 1));
        line2 = GeneralLine2.create(Vector2.create(0, 1), Vector2.create(1, 2));
        assertEquals(0d, line1.angle(line2), 0.001);
        assertEquals(0d, line2.angle(line1), 0.001);
    }
    
    /**
     * Tests distance calculation.
     */
    @Test
    public void testDist() {
        GeneralLine2 line = null;

        line = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(1, 1));
        assertEquals(0.0d, line.dist(Vector2.create(0, 0)), 0.001);
        assertEquals(0.0d, line.dist(Vector2.create(1, 1)), 0.001);
        assertEquals(Math.sqrt(2) / 2d, line.dist(Vector2.create(1, 0)), 0.001);
        assertEquals(Math.sqrt(2) / 2d, line.dist(Vector2.create(0, 1)), 0.001);
    }

}
