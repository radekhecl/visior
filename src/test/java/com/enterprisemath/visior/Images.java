package com.enterprisemath.visior;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;

/**
 * Utility class to work with images.
 *
 * @author radek.hecl
 */
public class Images {

    /**
     * Prevents construction.
     */
    private Images() {
    }

    /**
     * Creates volume buffer from the source buffered image.
     * Volume will contain red, green and blue planes.
     *
     * @param img source image
     * @return created volume
     */
    public static VolumeBuffer create(BufferedImage img) {
        int w = img.getWidth();
        int h = img.getHeight();
        int dim = w * h;
        VolumeBuffer res = VolumeBuffer.create(3, w, h);
        float[] rbuf = res.getPlane(0).getBuf();
        float[] gbuf = res.getPlane(1).getBuf();
        float[] bbuf = res.getPlane(2).getBuf();
        if (img.getType() == BufferedImage.TYPE_3BYTE_BGR) {
            byte[] pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
            for (int idx = 0, pi = 0; idx < dim; ++idx) {
                // pixels are sorted as blue, green and red (that's why type is 3BYTE_BGR)
                bbuf[idx] = ((int) pixels[pi++] & 0xff) / 255f;
                gbuf[idx] = ((int) pixels[pi++] & 0xff) / 255f;
                rbuf[idx] = ((int) pixels[pi++] & 0xff) / 255f;
            }
        }
        else {
            for (int y = 0; y < h; ++y) {
                for (int x = 0; x < w; ++x) {
                    int idx = y * w + x;
                    int rgb = img.getRGB(x, y);
                    rbuf[idx] = ((rgb & 0x00FF0000) >> 16) / 255f;
                    gbuf[idx] = ((rgb & 0x0000FF00) >> 8) / 255f;
                    bbuf[idx] = (rgb & 0x000000FF) / 255f;
                }
            }
        }
        return res;
    }

    /**
     * Loads volume from byte array.
     *
     * @param bytes bytes
     * @return volume containing image data
     */
    public static VolumeBuffer load(byte[] bytes) {
        BufferedImage img = null;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes)) {
            img = ImageIO.read(bis);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return create(img);
    }

    /**
     * Loads volume from file.
     *
     * @param file file
     * @return volume containing image data
     */
    public static VolumeBuffer load(File file) {
        try {
            return load(FileUtils.readFileToByteArray(file));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Loads volume from file.
     *
     * @param fpath file path
     * @return volume containing image data
     */
    public static VolumeBuffer load(String fpath) {
        return load(new File(fpath));
    }

    /**
     * Saves image to file.
     *
     * @param image image, expected RGB image
     * @param file file
     */
    public static void save(BufferedImage image, File file) {
        try {
            ImageIO.write(image, "png", file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Saves image to file.
     *
     * @param image image, expected RGB image
     * @param fpath file path
     */
    public static void save(BufferedImage image, String fpath) {
        save(image, new File(fpath));
    }

    /**
     * Converts volume with RGB channels to the buffered image.
     *
     * @param volume volume having RGB planes
     * @return buffered image
     */
    public static BufferedImage toBuffered(VolumeBuffer volume) {
        int dim = volume.getPlane(0).getDimension();
        BufferedImage res = new BufferedImage(volume.getWidth(), volume.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        float[] rbuf = volume.getPlane(0).getBuf();
        float[] gbuf = volume.getPlane(1).getBuf();
        float[] bbuf = volume.getPlane(2).getBuf();
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixels = ((DataBufferByte) res.getRaster().getDataBuffer()).getData();
        for (int idx = 0, pi = 0; idx < dim; ++idx) {
            pixels[pi++] = (byte) (Math.min(255, Math.max(0, (int) (bbuf[idx] * 255))));
            pixels[pi++] = (byte) (Math.min(255, Math.max(0, (int) (gbuf[idx] * 255))));
            pixels[pi++] = (byte) (Math.min(255, Math.max(0, (int) (rbuf[idx] * 255))));
        }
        return res;
    }

    /**
     * Saves volume to file.
     *
     * @param volume volume having RGB planes
     * @param file file
     */
    public static void save(VolumeBuffer volume, File file) {
        try {
            ImageIO.write(toBuffered(volume), "png", file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Saves volume to file.
     *
     * @param volume volume having RGB planes
     * @param fpath file path
     */
    public static void save(VolumeBuffer volume, String fpath) {
        save(volume, new File(fpath));
    }

    /**
     * Creates empty buffered image.
     *
     * @param width width
     * @param height height
     * @return created image
     */
    public static BufferedImage createBuffered(int width, int height) {
        return new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
    }

    /**
     * Converts plane to the buffered image.
     *
     * @param plane plane, expected values are in [0,1]
     * @return buffered image
     */
    public static BufferedImage toBuffered(PlaneBuffer plane) {
        int dim = plane.getDimension();
        BufferedImage res = new BufferedImage(plane.getWidth(), plane.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        float[] buf = plane.getBuf();
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixels = ((DataBufferByte) res.getRaster().getDataBuffer()).getData();
        for (int idx = 0, pi = 0; idx < dim; ++idx) {
            pixels[pi++] = (byte) (Math.min(255, Math.max(0, (int) (buf[idx] * 255))));
            pixels[pi++] = (byte) (Math.min(255, Math.max(0, (int) (buf[idx] * 255))));
            pixels[pi++] = (byte) (Math.min(255, Math.max(0, (int) (buf[idx] * 255))));
        }
        return res;
    }

    /**
     * Saves plane to image.
     *
     * @param plane plane, expected values are in [0,1]
     * @param file file
     */
    public static void save(PlaneBuffer plane, File file) {
        try {
            ImageIO.write(toBuffered(plane), "png", file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Saves plane to image.
     *
     * @param plane plane, expected values are in [0,1]
     * @param fpath file path
     */
    public static void save(PlaneBuffer plane, String fpath) {
        save(plane, new File(fpath));
    }

    /**
     * Converts mask to the buffered image.
     *
     * @param mask mask
     * @return buffered image
     */
    public static BufferedImage toBuffered(MaskBuffer mask) {
        int dim = mask.getDimension();
        BufferedImage res = new BufferedImage(mask.getWidth(), mask.getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        boolean[] buf = mask.getBuf();
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixels = ((DataBufferByte) res.getRaster().getDataBuffer()).getData();
        for (int idx = 0, pi = 0; idx < dim; ++idx) {
            byte val = (byte) (buf[idx] ? 255 : 0);
            pixels[pi++] = val;
            pixels[pi++] = val;
            pixels[pi++] = val;
        }
        return res;
    }

    /**
     * Saves mask to image.
     *
     * @param mask mask
     * @param file file
     */
    public static void save(MaskBuffer mask, File file) {
        try {
            ImageIO.write(toBuffered(mask), "png", file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Saves mask to image.
     *
     * @param mask mask
     * @param fpath file path
     */
    public static void save(MaskBuffer mask, String fpath) {
        save(mask, new File(fpath));
    }

}
