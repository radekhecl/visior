package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.ToStringBuilder;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Test case for operators.
 *
 * @author radek.hecl
 */
public class OperatorsTest {

    /**
     * Creates new instance.
     */
    public OperatorsTest() {
    }

    /**
     * Tests averaging.
     */
    @Test
    public void testAvg() {
        VolumeBuffer img = null;
        PlaneBuffer res = null;

        //
        img = Images.load("src/test/resources/rgb-3x3.png");
        res = PlaneBuffer.create(3, 3);
        Operators.avg(img, res);
        assertEquals(Plane.createConst(3, 3, 1 / 3f), res.toPlane());
    }

    /**
     * Tests thresholding.
     */
    @Test
    public void testThres() {
        VolumeBuffer img = null;
        PlaneBuffer mat = null;
        MaskBuffer res = null;

        //
        img = Images.load("src/test/resources/grey-3x3.png");
        mat = PlaneBuffer.create(3, 3);
        Operators.avg(img, mat);
        res = MaskBuffer.create(3, 3);
        Operators.thres(mat, 0.5f, res);
        assertEquals(Mask.create(3, 3, false, false, true, false, false, true, false, false, true), res.toMask());

        //
        img = Images.load("src/test/resources/grey-3x3.png");
        mat = PlaneBuffer.create(3, 3);
        Operators.avg(img, mat);
        res = MaskBuffer.create(3, 3);
        Operators.thres(mat, 0.4f, res);
        assertEquals(Mask.create(3, 3, false, true, true, false, true, true, false, true, true), res.toMask());
    }

    /**
     * Tests convolution.
     */
    @Test
    public void testConv() {
        PlaneBuffer input = null;
        Matrix kernel = null;
        Plane expected = null;
        PlaneBuffer res = null;

        // 1 pixel kernel - this is just simple multiplication
        input = PlaneBuffer.create(3, 3,
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        kernel = Matrix.create(1, 1, 2);
        expected = Plane.create(3, 3,
                2, 4, 6,
                8, 10, 12,
                14, 16, 18);
        res = PlaneBuffer.create(3, 3);
        Operators.conv(input, kernel, BorderType.REFLECT, res);
        assertEquals(expected, res.toPlane());

        // generic algorithm with symmetric border
        input = PlaneBuffer.create(5, 5,
                1, 2, 3, 4, 5,
                6, 7, 8, 9, 10,
                11, 12, 13, 14, 15,
                16, 17, 18, 19, 20,
                21, 22, 23, 24, 25);
        kernel = Matrix.create(3, 3,
                1, 2, 3,
                4, 5, 6,
                7, 8, 9);
        expected = Plane.create(5, 5,
                183, 216, 261, 306, 333,
                378, 411, 456, 501, 528,
                603, 636, 681, 726, 753,
                828, 861, 906, 951, 978,
                933, 966, 1011, 1056, 1083);
        res = PlaneBuffer.create(5, 5);
        Operators.conv(input, kernel, BorderType.REFLECT, res);
        assertEquals(expected, res.toPlane());

        // horizontal 3 kernel with symmetric border
        input = PlaneBuffer.create(5, 2, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        kernel = Matrix.create(3, 1, 3, 2, 1);
        expected = Plane.create(5, 2, 7, 10, 16, 22, 27, 37, 40, 46, 52, 57);
        res = PlaneBuffer.create(5, 2);
        Operators.conv(input, kernel, BorderType.REFLECT, res);
        assertEquals(expected, res.toPlane());

        // horizontal 5 kernel with symmetric border
        input = PlaneBuffer.create(7, 2, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
        kernel = Matrix.create(5, 1, 5, 4, 3, 2, 1);
        expected = Plane.create(7, 2, 24, 25, 35, 50, 65, 79, 90, 129, 130, 140, 155, 170, 184, 195);
        res = PlaneBuffer.create(7, 2);
        Operators.conv(input, kernel, BorderType.REFLECT, res);
        assertEquals(expected, res.toPlane());

        // horizontal 7 kernel with symmetric border
        input = PlaneBuffer.create(9, 2, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
        kernel = Matrix.create(7, 1, 7, 6, 5, 4, 3, 2, 1);
        expected = Plane.create(9, 2, 58, 55, 63, 84, 112, 140, 167, 191, 210, 310, 307, 315, 336, 364, 392, 419, 443, 462);
        res = PlaneBuffer.create(9, 2);
        Operators.conv(input, kernel, BorderType.REFLECT, res);
        assertEquals(expected, res.toPlane());

        // vertical 3 kernel with symmetric border
        input = PlaneBuffer.create(2, 5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        kernel = Matrix.create(1, 3, 3, 2, 1);
        expected = Plane.create(2, 5, 8, 14, 14, 20, 26, 32, 38, 44, 48, 54);
        res = PlaneBuffer.create(2, 5);
        Operators.conv(input, kernel, BorderType.REFLECT, res);
        assertEquals(expected, res.toPlane());

        // vertical 5 kernel with symmetric border
        input = PlaneBuffer.create(2, 7, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
        kernel = Matrix.create(1, 5, 5, 4, 3, 2, 1);
        expected = Plane.create(2, 7, 33, 48, 35, 50, 55, 70, 85, 100, 115, 130, 143, 158, 165, 180);
        res = PlaneBuffer.create(2, 7);
        Operators.conv(input, kernel, BorderType.REFLECT, res);
        assertEquals(expected, res.toPlane());

        // horizontal 7 kernel with symmetric border
        input = PlaneBuffer.create(2, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
        kernel = Matrix.create(1, 7, 7, 6, 5, 4, 3, 2, 1);
        expected = Plane.create(2, 9, 88, 116, 82, 110, 98, 126, 140, 168, 196, 224, 252, 280, 306, 334, 354, 382, 392, 420);
        res = PlaneBuffer.create(2, 9);
        Operators.conv(input, kernel, BorderType.REFLECT, res);
        assertEquals(expected, res.toPlane());
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
