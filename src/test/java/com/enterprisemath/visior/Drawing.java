package com.enterprisemath.visior;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.Collection;

/**
 * Utility class for drawing to the image.
 *
 * @author radek.hecl
 */
public class Drawing {

    /**
     * Prevents construction.
     */
    private Drawing() {
    }

    /**
     * Draws line.
     *
     * @param img image to be drawn in
     * @param p1 first point
     * @param p2 second point
     * @param color color
     * @param lineWidth line width
     */
    public static void line(BufferedImage img, Vector2 p1, Vector2 p2, RgbColor color, int lineWidth) {
        Graphics2D graphics = img.createGraphics();
        graphics.setStroke(new BasicStroke(lineWidth));
        graphics.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), 1));
        graphics.drawLine((int) p1.getX(), (int) p1.getY(), (int) p2.getX(), (int) p2.getY());
    }

    /**
     * Draws line.
     *
     * @param img image to be drawn in
     * @param line line
     * @param color color
     * @param lineWidth line width
     */
    public static void line(BufferedImage img, GeneralLine2 line, RgbColor color, int lineWidth) {
        Vector2 normal = line.getNormal();
        if (Math.abs(normal.getX()) > Math.abs(normal.getY())) {
            // line is more vertical
            GeneralLine2 bottom = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(img.getWidth(), 0));
            GeneralLine2 top = GeneralLine2.create(Vector2.create(0, img.getHeight() - 1), Vector2.create(img.getWidth(), img.getHeight() - 1));
            Vector2 start = bottom.intersection(line);
            Vector2 end = top.intersection(line);
            line(img, start, end, color, lineWidth);
        }
        else {
            // line is more horizontal
            GeneralLine2 left = GeneralLine2.create(Vector2.create(0, 0), Vector2.create(0, img.getHeight()));
            GeneralLine2 right = GeneralLine2.create(Vector2.create(img.getWidth() - 1, 0), Vector2.create(img.getWidth() - 1, img.getHeight()));
            Vector2 start = left.intersection(line);
            Vector2 end = right.intersection(line);
            line(img, start, end, color, lineWidth);
        }
    }

    /**
     * Draws AABB box.
     *
     * @param img image to be drawn in
     * @param aabb AABB box
     * @param color color
     * @param lineWidth line width
     */
    public static void aabb(BufferedImage img, Aabb2 aabb, RgbColor color, int lineWidth) {
        Graphics2D graphics = img.createGraphics();
        graphics.setStroke(new BasicStroke(lineWidth));
        graphics.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), 1));
        graphics.drawRect((int) aabb.getMin().getX(), (int) aabb.getMin().getY(),
                (int) aabb.getSize().getX(), (int) aabb.getSize().getY());
    }

    /**
     * Fills rectangle.
     *
     * @param img image to be drawn in
     * @param x x
     * @param y y
     * @param width width
     * @param height height
     * @param color color
     */
    public static void fillRect(BufferedImage img, float x, float y, float width, float height, RgbColor color) {
        Graphics2D graphics = img.createGraphics();
        graphics.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), 1));
        graphics.fillRect((int) x, (int) y, (int) width, (int) height);
    }

    /**
     * Draws contour to the buffered image.
     *
     * @param img image to be drawn in
     * @param contour contour
     * @param color color
     * @param lineWidth line width
     */
    public static void contour(BufferedImage img, Contour2 contour, RgbColor color, int lineWidth) {
        if (contour.getNumPoints() < 2) {
            return;
        }
        Graphics2D graphics = img.createGraphics();
        graphics.setStroke(new BasicStroke(lineWidth));
        graphics.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), 1));
        for (int i = 0; i < contour.getNumPoints(); ++i) {
            Vector2 a = contour.getPoint(i);
            Vector2 b = contour.getPoint(i == contour.getNumPoints() - 1 ? 0 : i + 1);
            graphics.drawLine((int) a.getX(), (int) a.getY(), (int) b.getX(), (int) b.getY());
        }
    }

    /**
     * Draws mask to the buffered image.
     *
     * @param img image to be drawn in
     * @param mask mask which has to have same size as the image
     * @param color color
     */
    public static void mask(BufferedImage img, MaskBuffer mask, RgbColor color) {
        byte r = (byte) (color.getRed() * 255);
        byte g = (byte) (color.getGreen() * 255);
        byte b = (byte) (color.getBlue() * 255);
        int dim = mask.getDimension();
        boolean[] buf = mask.getBuf();
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
        for (int idx = 0, pi = 0; idx < dim; ++idx) {
            if (buf[idx]) {
                pixels[pi++] = b;
                pixels[pi++] = g;
                pixels[pi++] = r;
            }
            else {
                pi = pi + 3;
            }
        }
    }

    /**
     * Draws points to the buffered image.
     *
     * @param img image to be drawn in
     * @param points points to be drawn
     * @param color color
     */
    public static void points(BufferedImage img, Collection<Vector2> points, RgbColor color) {
        byte r = (byte) (color.getRed() * 255);
        byte g = (byte) (color.getGreen() * 255);
        byte b = (byte) (color.getBlue() * 255);
        int w = img.getWidth();
        @SuppressWarnings("MismatchedReadAndWriteOfArray")
        byte[] pixels = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
        for (Vector2 point : points) {
            int idx = 3 * ((int) point.getY() * w + (int) point.getX());
            pixels[idx++] = b;
            pixels[idx++] = g;
            pixels[idx++] = r;
        }
    }

    /**
     * Draws points to the buffered image as a small circles.
     *
     * @param img image to be drawn in
     * @param points points to be drawn
     * @param color color
     * @param radius radius
     * @param lineWidth line width
     */
    public static void points(BufferedImage img, Collection<Vector2> points, RgbColor color, int radius, int lineWidth) {
        Graphics2D graphics = img.createGraphics();
        graphics.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), 1));
        graphics.setStroke(new BasicStroke(lineWidth));
        for (Vector2 point : points) {
            graphics.drawOval((int) point.getX() - radius, (int) point.getY() - radius, 2 * radius, 2 * radius);
        }
    }

    /**
     * Writes text.
     *
     * @param img image to be drawn in
     * @param text text
     * @param x position x
     * @param y position y
     * @param color color
     * @param fontSize font size
     * @param bold whether font should be bold
     */
    public static void text(BufferedImage img, String text, float x, float y, RgbColor color, int fontSize, boolean bold) {
        Graphics2D graphics = img.createGraphics();
        graphics.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), 1));
        graphics.setFont(new Font(Font.SANS_SERIF, bold ? Font.BOLD : 0, fontSize));
        graphics.drawString(text, x, y);
    }

    /**
     * Draws image.
     *
     * @param img image to be drawn in
     * @param top image placed on the top
     * @param x x position
     * @param y y position
     * @param width width
     * @param height height
     */
    public static void image(BufferedImage img, BufferedImage top, float x, float y, float width, float height) {
        Graphics2D graphics = img.createGraphics();
        graphics.drawImage(top, (int) x, (int) y, (int) width, (int) height, null);
    }

}
