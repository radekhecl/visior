package com.enterprisemath.visior;

import java.util.Arrays;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Generic matrix object.
 *
 * <p>
 * Elements are accessed by (row, column) indicies.
 * Internally the elements are stored in the one dimensional array organized by rows (i.e. whole first row is at the beginning).
 *
 * @author radek.hecl
 */
public class Matrix {

    /**
     * Width.
     */
    private int width;

    /**
     * Height.
     */
    private int height;

    /**
     * Data buffer.
     */
    private float[] buf;

    /**
     * Creates matrix.
     */
    private Matrix() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Validate.isTrue(width >= 0, "width must be >= 0");
        Validate.isTrue(height >= 0, "height must be >= 0");
        Validate.isTrue(buf.length == width * height, "data buffer length must match the dimension");
    }

    /**
     * Returns width.
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns height.
     *
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns dimension. This is width multiplied by height.
     *
     * @return dimension
     */
    public int getDimension() {
        return width * height;
    }

    /**
     * Copies the buffer to the new array.
     * Result is the copied array so changes in this doesn't affect the original.
     *
     * @return copied array buffer
     */
    public float[] copyBuf() {
        return Arrays.copyOf(buf, buf.length);
    }

    /**
     * Copies data buffer to the specified array.
     *
     * @param dest destination array, must be enough in length to cover the buffer
     */
    public void copyBuf(float[] dest) {
        System.arraycopy(buf, 0, dest, 0, buf.length);
    }

    /**
     * Returns element on the specified index.
     *
     * @param idx index
     * @return element
     */
    public float get(int idx) {
        return buf[idx];
    }

    /**
     * Returns the value on the specified row and column.
     *
     * @param row row index
     * @param col column index
     * @return value on the specified row and column
     */
    public float get(int row, int col) {
        return buf[row * width + col];
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates zero matrix.
     *
     * @param w width
     * @param h height
     *
     * @return zero matrix
     */
    public static Matrix createZero(int w, int h) {
        return create(w, h, 0);
    }

    /**
     * Creates constant matrix.
     *
     * @param w width
     * @param h height
     * @param val constant value
     * @return created matrix
     */
    public static Matrix createConst(int w, int h, float val) {
        Matrix res = new Matrix();
        res.width = w;
        res.height = h;
        res.buf = new float[w * h];
        for (int i = 0; i < res.buf.length; ++i) {
            res.buf[i] = val;
        }
        res.guardInvariants();
        return res;
    }

    /**
     * Creates matrix.
     *
     * @param w width
     * @param h height
     * @param buf data buffer
     * @return created matrix
     */
    public static Matrix create(int w, int h, float... buf) {
        Matrix res = new Matrix();
        res.width = w;
        res.height = h;
        res.buf = Arrays.copyOf(buf, w * h);
        res.guardInvariants();
        return res;
    }

    /**
     * Creates Gaussian horizontal filter. CEnter is places in the middle of the patch.
     *
     * @param size kernel size, must be odd number
     * @param sigma sigma
     * @return horizontal filter
     */
    public static Matrix gausHor(int size, float sigma) {
        Validate.isTrue(size % 2 == 1, "size must be odd number");
        Matrix res = new Matrix();
        res.width = size;
        res.height = 1;
        res.buf = new float[size];
        NormalDistribution dist = NormalDistribution.create(size / 2, sigma);
        float sum = 0;
        for (int x = 0; x < size; ++x) {
            float val = dist.getValue(x);
            sum = sum + val;
            res.buf[x] = val;
        }
        for (int i = 0; i < res.buf.length; ++i) {
            res.buf[i] = res.buf[i] / sum;
        }
        res.guardInvariants();
        return res;
    }

    /**
     * Creates Gaussian vertical filter. CEnter is places in the middle of the patch.
     *
     * @param size kernel size, must be odd number
     * @param sigma sigma
     * @return vertical filter
     */
    public static Matrix gausVert(int size, float sigma) {
        Validate.isTrue(size % 2 == 1, "size must be odd number");
        Matrix res = new Matrix();
        res.width = 1;
        res.height = size;
        res.buf = new float[size];
        NormalDistribution dist = NormalDistribution.create(size / 2, sigma);
        float sum = 0;
        for (int x = 0; x < size; ++x) {
            float val = dist.getValue(x);
            sum = sum + val;
            res.buf[x] = val;
        }
        for (int i = 0; i < res.buf.length; ++i) {
            res.buf[i] = res.buf[i] / sum;
        }
        res.guardInvariants();
        return res;
    }

    /**
     * Normal 1 dimensional probability distribution.
     */
    private static class NormalDistribution {

        /**
         * Mi parameter of the distribution.
         */
        private float mi;

        /**
         * Sigma parameter of the distribution.
         */
        private float sigma;

        /**
         * This is cached constant calculated to speed up. Value is 1 / (sigma * sqrt(2pi)).
         */
        private float fraction;

        /**
         * Creates new instance.
         *
         * @param builder builder object
         */
        private NormalDistribution(float mi, float sigma) {
            this.mi = mi;
            this.sigma = sigma;
            fraction = 1 / (sigma * (float) Math.sqrt(2 * Math.PI));
        }

        /**
         * Returns the value of the parameter mi.
         *
         * @return value of the parameter mi
         */
        public float getMi() {
            return mi;
        }

        /**
         * Returns the value of the parameter sigma.
         *
         * @return value of the parameter sigma
         */
        public float getSigma() {
            return sigma;
        }

        /**
         * Returns value of the distribution.
         *
         * @param x point
         * @return value
         */
        public float getValue(float x) {
            return fraction * (float) Math.exp(-0.5 * sqr((x - mi) / sigma));
        }

        /**
         * Returns the x * x.
         *
         * @param x value x
         * @return x * x
         */
        private float sqr(float x) {
            return x * x;
        }

        @Override
        public int hashCode() {
            return HashCodeBuilder.reflectionHashCode(this);
        }

        @Override
        public boolean equals(Object obj) {
            return EqualsBuilder.reflectionEquals(this, obj);
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }

        /**
         * Creates normal distribution.
         *
         * @param mi mi value
         * @param sigma sigma value
         * @return created distribution
         */
        public static NormalDistribution create(float mi, float sigma) {
            return new NormalDistribution(mi, sigma);
        }

    }

}
