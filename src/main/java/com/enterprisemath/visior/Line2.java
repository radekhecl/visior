package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Line in 2D space. This definition takes in account the line which is
 * determined by start point and end point. This implies that orientation of line matters when testing equality.
 *
 * @author radek.hecl
 */
public class Line2 {

    /**
     * Start point.
     */
    private Vector2 start;

    /**
     * End point.
     */
    private Vector2 end;

    /**
     * Creates new instance.
     */
    private Line2() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    /**
     * Returns start point.
     *
     * @return start point
     */
    public Vector2 getStart() {
        return start;
    }

    /**
     * Returns end point.
     *
     * @return end point
     */
    public Vector2 getEnd() {
        return end;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /**
     * Creates new instance.
     *
     * @param start start point
     * @param end end point
     * @return created object
     */
    public static Line2 create(Vector2 start, Vector2 end) {
        Line2 res = new Line2();
        res.start = start;
        res.end = end;
        res.guardInvariants();
        return res;
    }

}
