package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Line in 2D space defined by the general equation.
 * General equation is ax + by + c = 0, where a * a + b * b = 1.
 * This makes infinite line in 2D space.
 *
 * @author radek.hecl
 */
public class GeneralLine2 {

    /**
     * Parameter a.
     */
    private float a;

    /**
     * Parameter b.
     */
    private float b;

    /**
     * Parameter c.
     */
    private float c;

    /**
     * Creates new instance.
     */
    private GeneralLine2() {
    }

    /**
     * Returns the normal vector.
     *
     * @return normal vector
     */
    public Vector2 getNormal() {
        return Vector2.create(a, b);
    }

    /**
     * Returns the intersection with the other line, if it's unique point.
     * If lines are parallel, then there is no intersection point, or lines are identical.
     * In such case returns null.
     *
     * @param other other line
     * @return intersect point is single point exists, null if lines are parallel
     */
    public Vector2 intersection(GeneralLine2 other) {
        float det = a * other.b - b * other.a;
        if (det == 0) {
            // lines are parallel
            return null;
        }
        float dx = b * other.c - c * other.b;
        float dy = c * other.a - a * other.c;
        return Vector2.create(dx / det, dy / det);
    }

    /**
     * Returns the angle between two lines.
     * The returned angle is in radians and always in [0, Pi/2] closed interval.
     *
     * @param other other line
     * @return angle which is in [0, Pi/2] interval.
     */
    public float angle(GeneralLine2 other) {
        float dot = Math.abs(a * other.a + b * other.b);
        return (float) Math.acos(dot);
    }

    /**
     * Returns distance of this line from the specified vector.
     *
     * @param vec vector
     * @return distance
     */
    public float dist(Vector2 vec) {
        return Math.abs(a * vec.getX() + b * vec.getY() + c);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /**
     * Creates new line from normal vector and c factor.
     *
     * @param n normal vector, must be unit vector, otherwise the behavior is not defined
     * @param c c value for the general line equation
     * @return created line
     */
    public static GeneralLine2 create(Vector2 n, float c) {
        GeneralLine2 res = new GeneralLine2();
        res.a = n.getX();
        res.b = n.getY();
        res.c = c;
        return res;
    }

    /**
     * Creates new instance.
     *
     * @param start start point
     * @param end end point, which must be different than the start point
     * @return created object
     */
    public static GeneralLine2 create(Vector2 start, Vector2 end) {
        Vector2 dir = end.sub(start).normalize();
        GeneralLine2 res = new GeneralLine2();
        res.a = -dir.getY();
        res.b = dir.getX();
        res.c = start.getX() * dir.getY() - start.getY() * dir.getX();
        return res;
    }

}
