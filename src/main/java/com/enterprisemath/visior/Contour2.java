package com.enterprisemath.visior;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Contour definition in 2D space. Contour is simply a curve of continuous points defining a shape.
 * Typically this is a curve around the object.
 * This definition is for closed contour (start and end points are always same point).
 *
 * @author radek.hecl
 */
public class Contour2 {

    /**
     * Points.
     */
    private List<Vector2> points;

    /**
     * Creates new instance.
     */
    private Contour2() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    /**
     * Returns number of points.
     *
     * @return number of points
     */
    public int getNumPoints() {
        return points.size();
    }

    /**
     * Returns point.
     *
     * @param idx index
     * @return point
     */
    public Vector2 getPoint(int idx) {
        return points.get(idx);
    }

    /**
     * Returns points.
     *
     * @return points
     */
    public List<Vector2> getPoints() {
        return points;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /**
     * Creates new instance from a list of points.
     *
     * @param points points
     * @return contour
     */
    public static Contour2 create(Vector2... points) {
        Contour2 res = new Contour2();
        res.points = Collections.unmodifiableList(Arrays.asList(points));
        res.guardInvariants();
        return res;
    }

    /**
     * Creates new instance from a list of points.
     *
     * @param points points
     * @return contour
     */
    public static Contour2 create(List<Vector2> points) {
        Contour2 res = new Contour2();
        res.points = Collections.unmodifiableList(new ArrayList<>(points));
        res.guardInvariants();
        return res;
    }

}
