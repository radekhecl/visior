package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Color containing red, green and blue channels.
 *
 * @author radek.hecl
 */
public class RgbColor {

    /**
     * Red color.
     */
    public static RgbColor RED = create(1, 0, 0);

    /**
     * Green color.
     */
    public static RgbColor GREEN = create(0, 1, 0);

    /**
     * Blue color.
     */
    public static RgbColor BLUE = create(0, 0, 1);

    /**
     * White color.
     */
    public static RgbColor WHITE = create(1, 1, 1);

    /**
     * Black color.
     */
    public static RgbColor BLACK = create(0, 0, 0);

    /**
     * Red channel.
     */
    private float red;

    /**
     * Green channel.
     */
    private float green;

    /**
     * Blue channel.
     */
    private float blue;

    /**
     * Creates new instance.
     */
    private RgbColor() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        if (red < 0f || red > 1f) {
            throw new IllegalArgumentException("red must be in [0, 1]");
        }
        if (green < 0f || green > 1f) {
            throw new IllegalArgumentException("green must be in [0, 1]");
        }
        if (blue < 0f || blue > 1f) {
            throw new IllegalArgumentException("blue must be in [0, 1]");
        }
    }

    /**
     * Returns red channel.
     *
     * @return red channel
     */
    public float getRed() {
        return red;
    }

    /**
     * Returns green channel.
     *
     * @return green channel
     */
    public float getGreen() {
        return green;
    }

    /**
     * Returns blue channel.
     *
     * @return blue channel
     */
    public float getBlue() {
        return blue;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param red red channel
     * @param green green channel
     * @param blue blue channel
     * @return created color
     */
    public static RgbColor create(float red, float green, float blue) {
        RgbColor res = new RgbColor();
        res.red = red;
        res.green = green;
        res.blue = blue;
        res.guardInvariants();
        return res;
    }
}
