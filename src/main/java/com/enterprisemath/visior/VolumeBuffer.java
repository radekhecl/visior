package com.enterprisemath.visior;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Volume buffer object. Volume means the stacked planes together so that each plane is a layer in the volume.
 * For example red, green and blue planes can form an image.
 *
 * @author radek.hecl
 */
public class VolumeBuffer {

    /**
     * Width.
     */
    private int width;

    /**
     * Height.
     */
    private int height;

    /**
     * Planes.
     */
    private List<PlaneBuffer> planes;

    /**
     * Creates matrix.
     */
    private VolumeBuffer() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Validate.isTrue(width >= 0, "width must be >= 0");
        Validate.isTrue(height >= 0, "height must be >= 0");
        for (PlaneBuffer plane : planes) {
            Validate.isTrue(plane.getWidth() == width, "all planes must be same in width");
            Validate.isTrue(plane.getHeight() == height, "all planes must be same in height");
        }
    }

    /**
     * Returns width.
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns height.
     *
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns number of planes.
     *
     * @return number of planes
     */
    public int getNumPlanes() {
        return planes.size();
    }

    /**
     * Returns specified plane.
     *
     * @param idx index
     * @return plane
     */
    public PlaneBuffer getPlane(int idx) {
        return planes.get(idx);
    }

    /**
     * Returns specified element.
     *
     * @param pidx plane index
     * @param eidx element index within a plane
     * @return element
     */
    public float get(int pidx, int eidx) {
        return planes.get(pidx).get(eidx);
    }

    /**
     * Returns specified element.
     *
     * @param pidx plane index
     * @param x x position within a plane
     * @param y y position within a plane
     * @return element value
     */
    public float get(int pidx, int x, int y) {
        return planes.get(pidx).get(x, y);
    }

    /**
     * Sets specified element.
     *
     * @param pidx plane index
     * @param eidx element index within a plane
     * @param val value
     */
    public void set(int pidx, int eidx, float val) {
        planes.get(pidx).set(eidx, val);
    }

    /**
     * Sets specified element.
     *
     * @param pidx plane index
     * @param x x position within a plane
     * @param y y position within a plane
     * @param val value
     */
    public void set(int pidx, int x, int y, float val) {
        planes.get(pidx).set(x, y, val);
    }

    /**
     * Sets all points from the specified source buffer.
     * The whole planes are copied.
     *
     * @param src source buffer, must have same dimensions as this buffer
     */
    public void setAll(VolumeBuffer src) {
        for (int i = 0; i < planes.size(); ++i) {
            planes.get(i).setAll(src.planes.get(i));
        }
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param numPlanes number of channels
     * @param w width
     * @param h height
     *
     * @return created instance
     */
    public static VolumeBuffer create(int numPlanes, int w, int h) {
        VolumeBuffer res = new VolumeBuffer();
        res.width = w;
        res.height = h;
        res.planes = new ArrayList<>();
        for (int i = 0; i < numPlanes; ++i) {
            res.planes.add(PlaneBuffer.create(w, h));
        }
        res.planes = Collections.unmodifiableList(res.planes);
        res.guardInvariants();
        return res;
    }

}
