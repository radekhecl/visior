package com.enterprisemath.visior;

/**
 * Enumerates possible border types.
 * 
 * @author radek.hecl
 */
public enum BorderType {
    
    /**
     * Border is reflected as dcba|abcd|dcba.
     */
    REFLECT;
}
