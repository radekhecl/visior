package com.enterprisemath.visior;

import java.util.HashMap;
import java.util.Map;

/**
 * Class to work with blobs.
 *
 * @author radek.hecl
 */
public class Blobs {

    /**
     * Prevents construction.
     */
    private Blobs() {
    }

    /**
     * Extracts blobs which are connected by the 4 type connectivity.
     * Blobs are areas with true values.
     * Output plane will have value -1 for the places where this buffer is false.
     * Then blobs are indexed from 0 and last index is number of blobs - 1.
     *
     * @param in input mask
     * @param out output plane buffer, must have same width and height as input mask
     * @return the total number of blobs, which is equal to the maximal number in the output matrix
     */
    public static int blob4(MaskBuffer in, PlaneBuffer out) {
        int width = in.getWidth();
        int height = in.getHeight();
        boolean[] inbuf = in.getBuf();
        out.resetAll(-1);
        float[] rbuf = out.getBuf();
        int next = 0;
        Map<Float, Float> lblmap = new HashMap<>();

        //
        // first pass
        if (inbuf[0]) {
            rbuf[0] = next++;
        }
        for (int x = 1; x < width; ++x) {
            if (inbuf[x]) {
                if (rbuf[x - 1] == -1f) {
                    rbuf[x] = next++;
                }
                else {
                    rbuf[x] = rbuf[x - 1];
                }
            }
        }
        for (int y = 1, idx = width; y < height; ++y) {
            if (inbuf[idx]) {
                if (rbuf[idx - width] == -1f) {
                    rbuf[idx] = next++;
                }
                else {
                    rbuf[idx] = rbuf[idx - width];
                }
            }
            ++idx;
            for (int x = 1; x < width; ++x, ++idx) {
                if (inbuf[idx]) {
                    if (rbuf[idx - 1] == -1f) {
                        if (rbuf[idx - width] == -1f) {
                            rbuf[idx] = next++;
                        }
                        else {
                            rbuf[idx] = rbuf[idx - width];
                        }
                    }
                    else {
                        if (rbuf[idx - width] == -1f) {
                            rbuf[idx] = rbuf[idx - 1];
                        }
                        else if (rbuf[idx - 1] == rbuf[idx - width]) {
                            rbuf[idx] = rbuf[idx - 1];
                        }
                        else {
                            float min = Math.min(rbuf[idx - 1], rbuf[idx - width]);
                            float max = Math.max(rbuf[idx - 1], rbuf[idx - width]);
                            lblmap.put(max, min);
                            rbuf[idx] = min;
                        }
                    }
                }
            }
        }

        // finish if no blob has been found
        if (next == 0) {
            return 0;
        }

        //
        // relabeling - have to build new map, because of gaps between numbers
        Map<Float, Float> map = new HashMap<>();
        map.put(-1f, -1f);
        map.put(0f, 0f);
        float maxblobs = 1;
        for (float i = 1; i < next; ++i) {
            if (lblmap.containsKey(i)) {
                float nn = lblmap.get(i);
                if (map.containsKey(nn)) {
                    nn = map.get(nn);
                }
                map.put(i, nn);
            }
            else {
                map.put(i, maxblobs++);
            }
        }
        for (int i = 0; i < inbuf.length; ++i) {
            rbuf[i] = map.get(rbuf[i]);
        }
        return (int) maxblobs;
    }

    /**
     * Picks blob with the specified value.
     *
     * @param in input plane, where values 0, 1, 2... are marking the blobs
     * @param val value of the blob which will be returned
     * @param out mask which will be written into, must have same dimensions as input plane
     */
    public static void pickBlob(PlaneBuffer in, float val, MaskBuffer out) {
        float[] inbuf = in.getBuf();
        boolean[] outbuf = out.getBuf();
        for (int i = 0; i < inbuf.length; ++i) {
            outbuf[i] = inbuf[i] == val;
        }
    }
}
