package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Matrix of 3 rows and 4 columns.
 *
 * @author radek.hecl
 */
public class Matrix34 {

    /**
     * Elements of the first row.
     */
    private float m00, m01, m02, m03;

    /**
     * Elements of the second row.
     */
    private float m10, m11, m12, m13;

    /**
     * Elements of the third row.
     */
    private float m20, m21, m22, m23;

    /**
     * Creates matrix.
     */
    private Matrix34() {
    }

    /**
     * Returns first row first column.
     *
     * @return first row first column
     */
    public float m00() {
        return m00;
    }

    /**
     * Returns first row second column.
     *
     * @return first row second column
     */
    public float m01() {
        return m01;
    }

    /**
     * Returns first row third column.
     *
     * @return first row third column
     */
    public float m02() {
        return m02;
    }

    /**
     * Returns first row fourth column.
     *
     * @return first row fourth column
     */
    public float m03() {
        return m03;
    }

    /**
     * Returns second row first column.
     *
     * @return second row first column
     */
    public float m10() {
        return m10;
    }

    /**
     * Returns second row second column.
     *
     * @return second row second column
     */
    public float m11() {
        return m11;
    }

    /**
     * Returns second row third column.
     *
     * @return second row third column
     */
    public float m12() {
        return m12;
    }

    /**
     * Returns second row fourth column.
     *
     * @return second row fourth column
     */
    public float m13() {
        return m13;
    }

    /**
     * Returns third row first column.
     *
     * @return third row first column
     */
    public float m20() {
        return m20;
    }

    /**
     * Returns third row second column.
     *
     * @return third row second column
     */
    public float m21() {
        return m21;
    }

    /**
     * Returns third row third column.
     *
     * @return third row third column
     */
    public float m22() {
        return m22;
    }

    /**
     * Returns third row fourth column.
     *
     * @return third row fourth column
     */
    public float m23() {
        return m23;
    }

    /**
     * Multiplies the vector as Ax.
     *
     * @param vec vector on the right side
     * @return result of Ax
     */
    public Vector3 mul(Vector4 vec) {
        float x = vec.getX() * m00 + vec.getY() * m01 + vec.getZ() * m02 + vec.getW() * m03;
        float y = vec.getX() * m10 + vec.getY() * m11 + vec.getZ() * m12 + vec.getW() * m13;
        float z = vec.getX() * m20 + vec.getY() * m21 + vec.getZ() * m22 + vec.getW() * m23;
        return Vector3.create(x, y, z);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates matrix.
     *
     * @param m00 first row, first column
     * @param m01 first row, second column
     * @param m02 first row, third column
     * @param m03 first row, fourth column
     * @param m10 second row, first column
     * @param m11 second row, second column
     * @param m12 second row, third column
     * @param m13 second row, fourth column
     * @param m20 third row, first column
     * @param m21 third row, second column
     * @param m22 third row, third column
     * @param m23 third row, fourth column
     * @return created matrix
     */
    public static Matrix34 create(float m00, float m01, float m02, float m03,
            float m10, float m11, float m12, float m13,
            float m20, float m21, float m22, float m23) {
        Matrix34 res = new Matrix34();
        res.m00 = m00;
        res.m01 = m01;
        res.m02 = m02;
        res.m03 = m03;
        res.m10 = m10;
        res.m11 = m11;
        res.m12 = m12;
        res.m13 = m13;
        res.m20 = m20;
        res.m21 = m21;
        res.m22 = m22;
        res.m23 = m23;
        return res;
    }

}
