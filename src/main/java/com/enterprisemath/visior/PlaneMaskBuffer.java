package com.enterprisemath.visior;

import java.util.Arrays;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Mask buffer. Contains raw access to the mask data.
 *
 * @author radek.hecl
 */
public class PlaneMaskBuffer {

    /**
     * Width.
     */
    private int width;

    /**
     * Height.
     */
    private int height;

    /**
     * Data buffer.
     */
    private boolean[] buf;

    /**
     * Creates matrix.
     */
    private PlaneMaskBuffer() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Validate.isTrue(width >= 0, "width must be >= 0");
        Validate.isTrue(height >= 0, "height must be >= 0");
        Validate.isTrue(buf.length == width * height, "data buffer length must match the dimension");
    }

    /**
     * Returns width.
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns height.
     *
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns dimension. This is width multiplied by height.
     *
     * @return dimension
     */
    public int getDimension() {
        return buf.length;
    }

    /**
     * Returns the underline data.
     * Changes in the returned array will affect this object.
     *
     * @return raw data
     */
    public boolean[] getRawData() {
        return buf;
    }

    /**
     * Returns element on the specified index.
     *
     * @param idx index
     * @return element
     */
    public boolean get(int idx) {
        return buf[idx];
    }

    /**
     * Returns the value on the specified row and column.
     *
     * @param x x position, column in the image notation
     * @param y y position, row in the image notation
     * @return value on the specified row and column
     */
    public boolean get(int x, int y) {
        return buf[y * width + x];
    }

    /**
     * Sets element on the specified index.
     *
     * @param idx index
     * @param val value
     */
    public void set(int idx, boolean val) {
        buf[idx] = val;
    }

    /**
     * Sets the value on the specified row and column.
     *
     * @param x x position, column in the image notation
     * @param y y position, row in the image notation
     * @param val value
     */
    public void set(int x, int y, boolean val) {
        buf[y * width + x] = val;
    }

    /**
     * Converts this buffer into safe mask.
     *
     * @return safe mask
     */
    public Mask toMask() {
        return Mask.create(width, height, buf);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param w width
     * @param h height
     *
     * @return created instance
     */
    public static PlaneMaskBuffer create(int w, int h) {
        PlaneMaskBuffer res = new PlaneMaskBuffer();
        res.width = w;
        res.height = h;
        res.buf = new boolean[w * h];
        res.guardInvariants();
        return res;
    }

    /**
     * Creates new instance.
     *
     * @param w width
     * @param h height
     * @param buf initial values for data buffer
     *
     * @return created instance
     */
    public static PlaneMaskBuffer create(int w, int h, boolean... buf) {
        PlaneMaskBuffer res = new PlaneMaskBuffer();
        res.width = w;
        res.height = h;
        res.buf = Arrays.copyOf(buf, w * h);
        res.guardInvariants();
        return res;
    }

}
