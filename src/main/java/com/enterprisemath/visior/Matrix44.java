package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Matrix of 4 rows and 4 columns.
 *
 * @author radek.hecl
 */
public class Matrix44 {

    /**
     * Elements of the first row.
     */
    private float m00, m01, m02, m03;

    /**
     * Elements of the second row.
     */
    private float m10, m11, m12, m13;

    /**
     * Elements of the third row.
     */
    private float m20, m21, m22, m23;

    /**
     * Elements of the fourth row.
     */
    private float m30, m31, m32, m33;

    /**
     * Creates matrix.
     */
    private Matrix44() {
    }

    /**
     * Returns first row first column.
     *
     * @return first row first column
     */
    public float m00() {
        return m00;
    }

    /**
     * Returns first row second column.
     *
     * @return first row second column
     */
    public float m01() {
        return m01;
    }

    /**
     * Returns first row third column.
     *
     * @return first row third column
     */
    public float m02() {
        return m02;
    }

    /**
     * Returns first row fourth column.
     *
     * @return first row fourth column
     */
    public float m03() {
        return m03;
    }

    /**
     * Returns second row first column.
     *
     * @return second row first column
     */
    public float m10() {
        return m10;
    }

    /**
     * Returns second row second column.
     *
     * @return second row second column
     */
    public float m11() {
        return m11;
    }

    /**
     * Returns second row third column.
     *
     * @return second row third column
     */
    public float m12() {
        return m12;
    }

    /**
     * Returns second row fourth column.
     *
     * @return second row fourth column
     */
    public float m13() {
        return m13;
    }

    /**
     * Returns third row first column.
     *
     * @return third row first column
     */
    public float m20() {
        return m20;
    }

    /**
     * Returns third row second column.
     *
     * @return third row second column
     */
    public float m21() {
        return m21;
    }

    /**
     * Returns third row third column.
     *
     * @return third row third column
     */
    public float m22() {
        return m22;
    }

    /**
     * Returns third row fourth column.
     *
     * @return third row fourth column
     */
    public float m23() {
        return m23;
    }

    /**
     * Multiplies the vector as Ax.
     *
     * @param vec vector on the right side
     * @return result of Ax
     */
    public Vector4 mul(Vector4 vec) {
        float x = vec.getX() * m00 + vec.getY() * m01 + vec.getZ() * m02 + vec.getW() * m03;
        float y = vec.getX() * m10 + vec.getY() * m11 + vec.getZ() * m12 + vec.getW() * m13;
        float z = vec.getX() * m20 + vec.getY() * m21 + vec.getZ() * m22 + vec.getW() * m23;
        float w = vec.getX() * m30 + vec.getY() * m31 + vec.getZ() * m32 + vec.getW() * m33;
        return Vector4.create(x, y, z, w);
    }

    /**
     * Multiplies matrix the matrix.
     *
     * @param right matrix on the right side
     * @return result matrix
     */
    public Matrix44 mul(Matrix44 right) {
        Matrix44 res = new Matrix44();
        res.m00 = m00 * right.m00 + m01 * right.m10 + m02 * right.m20 + m03 * right.m30;
        res.m01 = m00 * right.m01 + m01 * right.m11 + m02 * right.m21 + m03 * right.m31;
        res.m02 = m00 * right.m02 + m01 * right.m12 + m02 * right.m22 + m03 * right.m32;
        res.m03 = m00 * right.m03 + m01 * right.m13 + m02 * right.m23 + m03 * right.m33;
        res.m10 = m10 * right.m00 + m11 * right.m10 + m12 * right.m20 + m13 * right.m30;
        res.m11 = m10 * right.m01 + m11 * right.m11 + m12 * right.m21 + m13 * right.m31;
        res.m12 = m10 * right.m02 + m11 * right.m12 + m12 * right.m22 + m13 * right.m32;
        res.m13 = m10 * right.m03 + m11 * right.m13 + m12 * right.m23 + m13 * right.m33;
        res.m20 = m20 * right.m00 + m21 * right.m10 + m22 * right.m20 + m23 * right.m30;
        res.m21 = m20 * right.m01 + m21 * right.m11 + m22 * right.m21 + m23 * right.m31;
        res.m22 = m20 * right.m02 + m21 * right.m12 + m22 * right.m22 + m23 * right.m32;
        res.m23 = m20 * right.m03 + m21 * right.m13 + m22 * right.m23 + m23 * right.m33;
        res.m30 = m30 * right.m00 + m31 * right.m10 + m32 * right.m20 + m33 * right.m30;
        res.m31 = m30 * right.m01 + m31 * right.m11 + m32 * right.m21 + m33 * right.m31;
        res.m32 = m30 * right.m02 + m31 * right.m12 + m32 * right.m22 + m33 * right.m32;
        res.m33 = m30 * right.m03 + m31 * right.m13 + m32 * right.m23 + m33 * right.m33;
        return res;
    }

    /**
     * Writes the elements to the prepared array.
     * Elements are done by the column major ordering (first column goes first).
     *
     * @param buf buffer with at least 16 elements
     * @return the buffer given at the input
     */
    public float[] toBufCol(float[] buf) {
        buf[0] = m00;
        buf[1] = m10;
        buf[2] = m20;
        buf[3] = m30;
        buf[4] = m01;
        buf[5] = m11;
        buf[6] = m21;
        buf[7] = m31;
        buf[8] = m02;
        buf[9] = m12;
        buf[10] = m22;
        buf[11] = m32;
        buf[12] = m03;
        buf[13] = m13;
        buf[14] = m23;
        buf[15] = m33;
        return buf;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates matrix.
     *
     * @param m00 first row, first column
     * @param m01 first row, second column
     * @param m02 first row, third column
     * @param m03 first row, fourth column
     * @param m10 second row, first column
     * @param m11 second row, second column
     * @param m12 second row, third column
     * @param m13 second row, fourth column
     * @param m20 third row, first column
     * @param m21 third row, second column
     * @param m22 third row, third column
     * @param m23 third row, fourth column
     * @param m30 fourth row, first column
     * @param m31 fourth row, second column
     * @param m32 fourth row, third column
     * @param m33 fourth row, fourth column
     * @return created matrix
     */
    public static Matrix44 create(float m00, float m01, float m02, float m03,
            float m10, float m11, float m12, float m13,
            float m20, float m21, float m22, float m23,
            float m30, float m31, float m32, float m33) {
        Matrix44 res = new Matrix44();
        res.m00 = m00;
        res.m01 = m01;
        res.m02 = m02;
        res.m03 = m03;
        res.m10 = m10;
        res.m11 = m11;
        res.m12 = m12;
        res.m13 = m13;
        res.m20 = m20;
        res.m21 = m21;
        res.m22 = m22;
        res.m23 = m23;
        res.m30 = m30;
        res.m31 = m31;
        res.m32 = m32;
        res.m33 = m33;
        return res;
    }

}
