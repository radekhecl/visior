package com.enterprisemath.visior;

/**
 * Operators.
 *
 * @author radek.hecl
 */
public class Operators {

    /**
     * Prevents construction.
     */
    private Operators() {
    }

    /**
     * Averages volume into a single plane.
     *
     * @param in input volume
     * @param out output plane
     */
    public static void avg(VolumeBuffer in, PlaneBuffer out) {
        if (in.getNumPlanes() == 1) {
            float[] ch1 = in.getPlane(0).getBuf();
            float[] buf = out.getBuf();
            for (int i = 0; i < buf.length; ++i) {
                buf[i] = ch1[i];
            }
        }
        else if (in.getNumPlanes() == 2) {
            float[] ch1 = in.getPlane(0).getBuf();
            float[] ch2 = in.getPlane(1).getBuf();
            float[] buf = out.getBuf();
            for (int i = 0; i < buf.length; ++i) {
                buf[i] = (ch1[i] + ch2[i]) / 2f;
            }
        }
        else if (in.getNumPlanes() == 3) {
            float[] ch1 = in.getPlane(0).getBuf();
            float[] ch2 = in.getPlane(1).getBuf();
            float[] ch3 = in.getPlane(2).getBuf();
            float[] buf = out.getBuf();
            for (int i = 0; i < buf.length; ++i) {
                buf[i] = (ch1[i] + ch2[i] + ch3[i]) / 3f;
            }
        }
        else {
            float[] buf = out.getBuf();
            for (int i = 0; i < buf.length; ++i) {
                buf[i] = 0;
                for (int ch = 0; ch < in.getNumPlanes(); ++i) {
                    buf[i] = buf[i] + in.get(ch, i);
                }
                buf[i] = buf[i] / in.getNumPlanes();
            }
        }
    }

    /**
     * Performs binary threshold.
     *
     * @param in input plane
     * @param thres threshold value, points greater to that will be true, others false
     * @param out threshold output
     */
    public static void thres(PlaneBuffer in, float thres, MaskBuffer out) {
        for (int i = 0; i < in.getDimension(); ++i) {
            out.set(i, in.get(i) > thres);
        }
    }

    /**
     * Performs convolution on the plane. Anchor point of the kernel is placed in the middle of kernel.
     * Mathematically this is not a convolution, because the kernel is not flipped
     * (in mathematics kernel should be flipped horizontally and vertically).
     *
     * @param in input plane, expected to be at least twice size than kernel in both direction
     * @param kernel kernel, width and height must be odd numbers
     * @param border border type used during convolution
     * @param out matrix to write output, have same size as input
     */
    public static void conv(PlaneBuffer in, Matrix kernel, BorderType border, PlaneBuffer out) {
        if (border.equals(BorderType.REFLECT)) {
            int w = in.getWidth();
            int h = in.getHeight();
            int kw = kernel.getWidth();
            int kh = kernel.getHeight();
            if (kw == 1 && kh == 1) {
                // 1 x 1 kernel
                float fact = kernel.get(0);
                int dim = w * h;
                for (int i = 0; i < dim; ++i) {
                    out.set(i, in.get(i) * fact);
                }
            }
            else if (kh == 1 && kw == 3) {
                // single row, 3 column kernel
                float k1 = kernel.get(0);
                float k2 = kernel.get(1);
                float k3 = kernel.get(2);
                int wm1 = w - 1;
                float[] buf = in.getBuf();
                float[] obuf = out.getBuf();
                for (int y = 0, idx = 0; y < h; ++y) {
                    obuf[idx] = buf[idx] * k1 + buf[idx] * k2 + buf[idx + 1] * k3;
                    ++idx;
                    for (int x = 1; x < wm1; ++x, ++idx) {
                        obuf[idx] = buf[idx - 1] * k1 + buf[idx] * k2 + buf[idx + 1] * k3;
                    }
                    obuf[idx] = buf[idx - 1] * k1 + buf[idx] * k2 + buf[idx] * k3;
                    ++idx;
                }
            }
            else if (kw == 1 && kh == 3) {
                // single column, 3 rows kernel
                float k1 = kernel.get(0);
                float k2 = kernel.get(1);
                float k3 = kernel.get(2);
                int hm1 = h - 1;
                int hlp1 = hm1 * w;
                int hlp2 = hlp1 - w;
                float[] buf = in.getBuf();
                float[] obuf = out.getBuf();
                for (int x = 0; x < w; ++x) {
                    obuf[x] = buf[x] * k1 + buf[x] * k2 + buf[w + x] * k3;
                    for (int y = 1, idx = w; y < hm1; ++y, idx += w) {
                        obuf[idx + x] = buf[idx - w + x] * k1 + buf[idx + x] * k2 + buf[idx + w + x] * k3;
                    }
                    obuf[hlp1 + x] = buf[hlp2 + x] * k1 + buf[hlp1 + x] * k2 + buf[hlp1 + x] * k3;
                }
            }
            else if (kh == 1) {
                // single row kernel
                int kwhalf = kw / 2;
                for (int y = 0; y < h; ++y) {
                    for (int x = 0; x < w; ++x) {
                        float val = 0;
                        for (int kx = 0; kx < kw; ++kx) {
                            val = val + getReflect(in, x + kx - kwhalf, y) * kernel.get(kx);
                        }
                        out.set(x, y, val);
                    }
                }
            }
            else if (kw == 1) {
                // single column kernel
                int khhalf = kh / 2;
                for (int y = 0; y < h; ++y) {
                    for (int x = 0; x < w; ++x) {
                        float val = 0;
                        for (int ky = 0; ky < kh; ++ky) {
                            val = val + getReflect(in, x, y + ky - khhalf) * kernel.get(ky);
                        }
                        out.set(x, y, val);
                    }
                }
            }
            else {
                // generic kernel - works for everything, other cases are there because of performance
                int kwhalf = kw / 2;
                int khhalf = kh / 2;
                for (int y = 0; y < h; ++y) {
                    for (int x = 0; x < w; ++x) {
                        float val = 0;
                        for (int ky = 0; ky < kh; ++ky) {
                            for (int kx = 0; kx < kw; ++kx) {
                                val = val + getReflect(in, x + kx - kwhalf, y + ky - khhalf) * kernel.get(ky, kx);
                            }
                        }
                        out.set(x, y, val);
                    }
                }
            }
        }
        else {
            throw new IllegalArgumentException("unsupported border type: " + border);
        }
    }

    /**
     * Returns value in in the REFLECT border manner.
     *
     * @param plane plane
     * @param x x position
     * @param y y position
     * @return value
     */
    private static float getReflect(PlaneBuffer plane, int x, int y) {
        if (x < 0) {
            x = -x - 1;
        }
        if (x >= plane.getWidth()) {
            x = plane.getWidth() + plane.getWidth() - x - 1;
        }
        if (y < 0) {
            y = -y - 1;
        }
        if (y >= plane.getHeight()) {
            y = plane.getHeight() + plane.getHeight() - y - 1;
        }
        return plane.get(x, y);
    }
}
