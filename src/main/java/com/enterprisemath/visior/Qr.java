package com.enterprisemath.visior;

import java.util.List;

import com.enterprisemath.visior.qrcode.Detector;

/**
 * Utility to work with a QR codes.
 *
 * @author radek.hecl
 */
public class Qr {

    /**
     * Prevents construction.
     */
    private Qr() {
    }

    /**
     * Detects QR codes.
     *
     * @param mask mask to detect from
     * @return QR codes
     */
    public static List<Contour2> detect(MaskBuffer mask) {
        Detector detector = new Detector(mask);
        return detector.detect();
    }
}
