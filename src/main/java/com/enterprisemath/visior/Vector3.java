package com.enterprisemath.visior;

/**
 * 3D vector definition.
 *
 * @author radek.hecl
 */
public class Vector3 {

    /**
     * X coordinate.
     */
    private float x;

    /**
     * Y coordinate.
     */
    private float y;

    /**
     * Z coordinate.
     */
    private float z;

    /**
     * Creates new instance.
     */
    private Vector3() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    /**
     * Returns x coordinate.
     *
     * @return x coordinate
     */
    public float getX() {
        return x;
    }

    /**
     * Returns y coordinate.
     *
     * @return y coordinate
     */
    public float getY() {
        return y;
    }

    /**
     * Returns z coordinate.
     *
     * @return z coordinate
     */
    public float getZ() {
        return z;
    }

    /**
     * Returns magnitude of this vector.
     *
     * @return vector magnitude
     */
    public float mag() {
        return (float) Math.sqrt(x * x + y * y + z * z);
    }

    /**
     * Normalizes the vector.
     *
     * @return normalized vector
     */
    public Vector3 normalize() {
        float m = mag();
        return Vector3.create(x / m, y / m, z / m);
    }

    /**
     * Scales elements of this vector by the specified value.
     *
     * @param s scale factor
     * @return scaled rectangle
     */
    public Vector3 scale(float s) {
        return Vector3.create(x * s, y * s, z * s);
    }

    /**
     * Adds another vector to this one.
     *
     * @param vec other vector
     * @return result after operation
     */
    public Vector3 add(Vector3 vec) {
        return Vector3.create(x + vec.x, y + vec.y, z + vec.z);
    }

    /**
     * Subtracts another vector from this one.
     *
     * @param vec other vector
     * @return result after operation
     */
    public Vector3 sub(Vector3 vec) {
        return Vector3.create(x - vec.x, y - vec.y, z - vec.z);
    }

    /**
     * Returns distance from the other vector.
     *
     * @param vec vector
     * @return distance
     */
    public float dist(Vector3 vec) {
        float dx = x - vec.x;
        float dy = y - vec.y;
        float dz = z - vec.z;
        return (float) Math.sqrt(dx * dx + dy * dy + dz * dz);
    }

    /**
     * Converts this vector out of homogenous coordinates.
     *
     * @return de homogenous vector
     */
    public Vector2 dehomogenize() {
        return Vector2.create(x / z, y / z);
    }

    @Override
    public int hashCode() {
        return (int) x + 13 * (int) y + 17 * (int) z;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        else if (!(obj instanceof Vector3)) {
            return false;
        }
        Vector3 ob = (Vector3) obj;
        return ob.getX() == this.getX() && ob.getY() == this.getY() && ob.getZ() == this.getZ();
    }

    @Override
    public String toString() {
        return new StringBuilder().append("(").append(x).append(", ").append(y).append(", ").append(z).append(")").toString();
    }

    /**
     * Creates new instance.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     * @return created vector
     */
    public static Vector3 create(float x, float y, float z) {
        Vector3 res = new Vector3();
        res.x = x;
        res.y = y;
        res.z = z;
        res.guardInvariants();
        return res;
    }

    /**
     * Calculates cross product.
     *
     * @param a vector a
     * @param b vector b
     * @return cross product
     */
    public static Vector3 cross(Vector3 a, Vector3 b) {
        return Vector3.create(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
    }

}
