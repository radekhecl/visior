package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * AABB box in 2D.
 *
 * @author radek.hecl
 */
public class Aabb2 {

    /**
     * Minimum.
     */
    private Vector2 min;

    /**
     * Size.
     */
    private Vector2 size;

    /**
     * Creates new instance.
     */
    private Aabb2() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    /**
     * Returns minimum vector.
     *
     * @return minimum vector
     */
    public Vector2 getMin() {
        return min;
    }

    /**
     * Returns size vector.
     *
     * @return size vector
     */
    public Vector2 getSize() {
        return size;
    }

    /**
     * Returns whether this box contains the specified point.
     * Border values are included
     *
     * @param vec tested vector
     * @return true if this box contains given vector, false otherwise.
     */
    public boolean contains(Vector2 vec) {
        if (vec.getX() < min.getX()) {
            return false;
        }
        if (vec.getY() < min.getY()) {
            return false;
        }
        if (vec.getX() > min.getX() + size.getX()) {
            return false;
        }
        if (vec.getY() > min.getY() + size.getY()) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    /**
     * Creates new instance.
     *
     * @param points points
     * @return shape
     */
    /**
     * Creates new instance.
     *
     * @param min minimum
     * @param size size
     * @return created object
     */
    public static Aabb2 create(Vector2 min, Vector2 size) {
        Aabb2 res = new Aabb2();
        res.min = min;
        res.size = size;
        res.guardInvariants();
        return res;
    }

}
