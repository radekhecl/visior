package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Pair of corresponding objects.
 *
 * @author radek.hecl
 * @param <S> source object type
 * @param <T> target object type
 */
public class CorrespondingPair<S, T> {

    /**
     * Source object.
     */
    public S source;

    /**
     * Target object.
     */
    public T target;

    /**
     * Creates new instance.
     */
    private CorrespondingPair() {
    }

    /**
     * Returns source object.
     *
     * @return source object
     */
    public S getSource() {
        return source;
    }

    /**
     * Returns target object.
     *
     * @return target object
     */
    public T getTarget() {
        return target;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param <S> source type
     * @param <T> target type
     * @param source source object
     * @param target target object
     * @return created object
     */
    public static <S, T> CorrespondingPair<S, T> create(S source, T target) {
        CorrespondingPair<S, T> res = new CorrespondingPair<>();
        res.source = source;
        res.target = target;
        return res;
    }

}
