package com.enterprisemath.visior;

import java.util.List;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularValueDecomposition;

/**
 * Augmented reality utility class.
 *
 * @author radek.hecl
 */
public class Ar {

    /**
     * Prevents construction.
     */
    private Ar() {
    }

    /**
     * Estimates homography matrix from 2 sets of corresponding points.
     *
     * @param pairs corresponding points
     * @return homography transformation matrix
     */
    public static Matrix33 estimateHomography(List<CorrespondingPair<Vector2, Vector2>> pairs) {
        RealMatrix rmat = MatrixUtils.createRealMatrix(pairs.size() * 2, 8);
        double[] rvectBuf = new double[pairs.size() * 2];

        for (int i = 0; i < pairs.size(); ++i) {
            Vector2 s = pairs.get(i).getSource();
            Vector2 t = pairs.get(i).getTarget();
            rmat.setEntry(i * 2, 0, s.getX());
            rmat.setEntry(i * 2, 1, s.getY());
            rmat.setEntry(i * 2, 2, 1);
            rmat.setEntry(i * 2, 3, 0);
            rmat.setEntry(i * 2, 4, 0);
            rmat.setEntry(i * 2, 5, 0);
            rmat.setEntry(i * 2, 6, -t.getX() * s.getX());
            rmat.setEntry(i * 2, 7, -t.getX() * s.getY());
            rvectBuf[i * 2] = t.getX();

            rmat.setEntry(i * 2 + 1, 0, 0);
            rmat.setEntry(i * 2 + 1, 1, 0);
            rmat.setEntry(i * 2 + 1, 2, 0);
            rmat.setEntry(i * 2 + 1, 3, s.getX());
            rmat.setEntry(i * 2 + 1, 4, s.getY());
            rmat.setEntry(i * 2 + 1, 5, 1);
            rmat.setEntry(i * 2 + 1, 6, -t.getY() * s.getX());
            rmat.setEntry(i * 2 + 1, 7, -t.getY() * s.getY());
            rvectBuf[i * 2 + 1] = t.getY();
        }

        SingularValueDecomposition svd = new SingularValueDecomposition(rmat);
        RealVector v = svd.getSolver().solve(MatrixUtils.createRealVector(rvectBuf));
        return Matrix33.create(
                (float) v.getEntry(0), (float) v.getEntry(1), (float) v.getEntry(2),
                (float) v.getEntry(3), (float) v.getEntry(4), (float) v.getEntry(5),
                (float) v.getEntry(6), (float) v.getEntry(7), (float) 1);
    }

    /**
     * Estimates model view matrix.
     *
     * @param kinv inversion of camera intristic matrix
     * @param corrPts corresponding points, at least 4 points, none of them can be in the line,
     * source is point in the 3d space with z coordinate being 0, target is point on the image
     * @return model view matrix
     */
    public static Matrix34 estimateMvMatrix(Matrix33 kinv, List<CorrespondingPair<Vector2, Vector2>> corrPts) {
        Matrix33 h = Ar.estimateHomography(corrPts);
        h = h.mulel(-1);
        Matrix33 g = kinv.mul(h);
        Vector3 g1 = g.getCol(0);
        Vector3 g2 = g.getCol(1);
        Vector3 g3 = g.getCol(2);
        float l = (float) Math.sqrt(g1.mag() * g2.mag());
        float linv = 1 / l;
        Vector3 r1 = g1.scale(linv);
        Vector3 r2 = g2.scale(linv);
        Vector3 trans = g3.scale(linv);

        Vector3 c = r1.add(r2);
        Vector3 p = Vector3.cross(r1, r2);
        Vector3 d = Vector3.cross(c, p);

        r1 = c.scale(1 / c.mag()).add(d.scale(1 / d.mag())).scale(1 / (float) Math.sqrt(2));
        r2 = c.scale(1 / c.mag()).sub(d.scale(1 / d.mag())).scale(1 / (float) Math.sqrt(2));
        Vector3 r3 = Vector3.cross(r1, r2);

        return Matrix34.create(
                r1.getX(), r2.getX(), r3.getX(), trans.getX(),
                r1.getY(), r2.getY(), r3.getY(), trans.getY(),
                r1.getZ(), r2.getZ(), r3.getZ(), trans.getZ());
    }

    /**
     * Converts calibration matrix to OpenGL projection matrix.
     *
     * @param k calibration matrix
     * @param near near clipping plane
     * @param far far clipping plane
     * @param width width of the original image in pixels
     * @param height height of the original image in pixels
     * @return OpenGL projection matrix
     */
    public static Matrix44 toGlProj(Matrix33 k, float near, float far, float width, float height) {
        Matrix44 pers = Matrix44.create(
                k.m00(), k.m01(), -k.m02(), 0.0f,
                0.0f, k.m11(), -k.m12(), 0.0f,
                0.0f, 0.0f, near + far, near * far,
                0.0f, 0.0f, -1.0f, 0.0f);
        Matrix44 ndc = Matrix44.create(
                -2f / width, 0.0f, 0.0f, 1.0f,
                0.0f, 2f / height, 0.0f, -1.0f,
                0.0f, 0.0f, 2f / (near - far), (far + near) / (near - far),
                0.0f, 0.0f, 0.0f, 1.0f);
        return ndc.mul(pers);
    }

    /**
     * Converts view matrix to OpenGL view matrix.
     *
     * @param v view matrix
     * @return open GL view matrix
     */
    public static Matrix44 toGlV(Matrix34 v) {
        return Matrix44.create(
                v.m00(), v.m01(), v.m02(), v.m03(),
                v.m10(), v.m11(), v.m12(), v.m13(),
                v.m20(), v.m21(), v.m22(), v.m23(),
                0.0f, 0.0f, 0.0f, 1.0f);
    }

}
