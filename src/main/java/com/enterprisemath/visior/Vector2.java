package com.enterprisemath.visior;

import java.util.Collection;

/**
 * 2D vector definition.
 *
 * @author radek.hecl
 */
public class Vector2 {

    /**
     * X coordinate.
     */
    private float x;

    /**
     * Y coordinate.
     */
    private float y;

    /**
     * Creates new instance.
     */
    private Vector2() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    /**
     * Returns x coordinate.
     *
     * @return x coordinate
     */
    public float getX() {
        return x;
    }

    /**
     * Returns y coordinate.
     *
     * @return y coordinate
     */
    public float getY() {
        return y;
    }

    /**
     * Returns magnitude of this vector.
     *
     * @return vector magnitude
     */
    public float mag() {
        return (float) Math.sqrt(x * x + y * y);
    }

    /**
     * Normalizes the vector.
     *
     * @return normalized vector
     */
    public Vector2 normalize() {
        float m = mag();
        return Vector2.create(x / m, y / m);
    }

    /**
     * Scales elements of this vector by the specified value.
     *
     * @param s scale factor
     * @return scaled rectangle
     */
    public Vector2 scale(float s) {
        return Vector2.create(x * s, y * s);
    }

    /**
     * Adds another vector to this one.
     *
     * @param vec other vector
     * @return result after operation
     */
    public Vector2 add(Vector2 vec) {
        return Vector2.create(x + vec.x, y + vec.y);
    }

    /**
     * Subtracts another vector from this one.
     *
     * @param vec other vector
     * @return result after operation
     */
    public Vector2 sub(Vector2 vec) {
        return Vector2.create(x - vec.x, y - vec.y);
    }

    /**
     * Returns dot product.
     *
     * @param vec other vector
     * @return dot product
     */
    public float dot(Vector2 vec) {
        return x * vec.x + y * vec.y;
    }

    /**
     * Returns distance from the other vector.
     *
     * @param vec vector
     * @return distance
     */
    public float dist(Vector2 vec) {
        float dx = x - vec.x;
        float dy = y - vec.y;
        return (float) Math.sqrt(dx * dx + dy * dy);
    }

    @Override
    public int hashCode() {
        return (int) x + 13 * (int) y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        else if (!(obj instanceof Vector2)) {
            return false;
        }
        Vector2 ob = (Vector2) obj;
        return ob.getX() == this.getX() && ob.getY() == this.getY();
    }

    @Override
    public String toString() {
        return new StringBuilder().append("(").append(x).append(", ").append(y).append(")").toString();
    }

    /**
     * Creates new instance.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @return created vector
     */
    public static Vector2 create(float x, float y) {
        Vector2 res = new Vector2();
        res.x = x;
        res.y = y;
        res.guardInvariants();
        return res;
    }

    /**
     * Calculates average of the given vectors.
     *
     * @param vecs vectors, must have at least 1 vector
     * @return average vector
     */
    public static Vector2 avg(Collection<Vector2> vecs) {
        float x = 0;
        float y = 0;
        for (Vector2 vec : vecs) {
            x = x + vec.x;
            y = y + vec.y;
        }
        Vector2 res = new Vector2();
        res.x = x / vecs.size();
        res.y = y / vecs.size();
        res.guardInvariants();
        return res;
    }
}
