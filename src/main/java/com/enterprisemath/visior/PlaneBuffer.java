package com.enterprisemath.visior;

import java.util.Arrays;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Plane buffer object. Plane is a 2D array representing image channel.
 * Typical example is red, green and blue. Other example might be derivatives, convolutions or height map.
 * Plane itself doesn't have a context.
 * <p>
 * Points are accessed by (x, y) coordinates, where x is the column and y is the row.
 * Internally the elements are stored in the one dimensional array organized by rows (i.e. whole first row is at the beginning).
 *
 * @author radek.hecl
 */
public class PlaneBuffer {

    /**
     * Width.
     */
    private int width;

    /**
     * Height.
     */
    private int height;

    /**
     * Data buffer.
     */
    private float[] buf;

    /**
     * Creates matrix.
     */
    private PlaneBuffer() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Validate.isTrue(width >= 0, "width must be >= 0");
        Validate.isTrue(height >= 0, "height must be >= 0");
        Validate.isTrue(buf.length == width * height, "data buffer length must match the dimension");
    }

    /**
     * Returns width.
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns height.
     *
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns dimension. This is width multiplied by height.
     *
     * @return dimension
     */
    public int getDimension() {
        return buf.length;
    }

    /**
     * Returns the underline buffer.
     * Changes in the returned array will affect this object.
     *
     * @return raw data
     */
    public float[] getBuf() {
        return buf;
    }

    /**
     * Returns element on the specified index.
     *
     * @param idx index
     * @return element
     */
    public float get(int idx) {
        return buf[idx];
    }

    /**
     * Returns the value on the specified row and column.
     *
     * @param x x position, column in the image notation
     * @param y y position, row in the image notation
     * @return value on the specified row and column
     */
    public float get(int x, int y) {
        return buf[y * width + x];
    }

    /**
     * Sets element on the specified index.
     *
     * @param idx index
     * @param val value
     */
    public void set(int idx, float val) {
        buf[idx] = val;
    }

    /**
     * Sets the value on the specified row and column.
     *
     * @param x x position, column in the image notation
     * @param y y position, row in the image notation
     * @param val value
     */
    public void set(int x, int y, float val) {
        buf[y * width + x] = val;
    }

    /**
     * Sets all points from the specified source buffer.
     * The whole array from source is copied to this buffer.
     *
     * @param src source buffer, must have same dimensions as this buffer
     */
    public void setAll(PlaneBuffer src) {
        System.arraycopy(src.buf, 0, buf, 0, buf.length);
    }

    /**
     * Resets all elements to the same value.
     *
     * @param val value
     */
    public void resetAll(float val) {
        Arrays.fill(buf, val);
    }

    /**
     * Converts this buffer into safe plane.
     *
     * @return safe plane
     */
    public Plane toPlane() {
        return Plane.create(width, height, buf);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates new instance.
     *
     * @param w width
     * @param h height
     *
     * @return created instance
     */
    public static PlaneBuffer create(int w, int h) {
        PlaneBuffer res = new PlaneBuffer();
        res.width = w;
        res.height = h;
        res.buf = new float[w * h];
        res.guardInvariants();
        return res;
    }

    /**
     * Creates new instance.
     *
     * @param w width
     * @param h height
     * @param buf initial values for data buffer
     *
     * @return created instance
     */
    public static PlaneBuffer create(int w, int h, float... buf) {
        PlaneBuffer res = new PlaneBuffer();
        res.width = w;
        res.height = h;
        res.buf = Arrays.copyOf(buf, w * h);
        res.guardInvariants();
        return res;
    }

}
