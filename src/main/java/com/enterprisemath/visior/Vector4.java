package com.enterprisemath.visior;

/**
 * 4D vector definition.
 *
 * @author radek.hecl
 */
public class Vector4 {

    /**
     * X coordinate.
     */
    private float x;

    /**
     * Y coordinate.
     */
    private float y;

    /**
     * Z coordinate.
     */
    private float z;

    /**
     * W coordinate.
     */
    private float w;

    /**
     * Creates new instance.
     */
    private Vector4() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
    }

    /**
     * Returns x coordinate.
     *
     * @return x coordinate
     */
    public float getX() {
        return x;
    }

    /**
     * Returns y coordinate.
     *
     * @return y coordinate
     */
    public float getY() {
        return y;
    }

    /**
     * Returns z coordinate.
     *
     * @return z coordinate
     */
    public float getZ() {
        return z;
    }

    /**
     * Returns w coordinate.
     *
     * @return w coordinate
     */
    public float getW() {
        return w;
    }

    /**
     * Returns magnitude of this vector.
     *
     * @return vector magnitude
     */
    public float mag() {
        return (float) Math.sqrt(x * x + y * y + z * z + w * w);
    }

    /**
     * Scales elements of this vector by the specified value.
     *
     * @param s scale factor
     * @return scaled rectangle
     */
    public Vector4 scale(float s) {
        return Vector4.create(x * s, y * s, z * s, w * s);
    }

    /**
     * Adds another vector to this one.
     *
     * @param vec other vector
     * @return result after operation
     */
    public Vector4 add(Vector4 vec) {
        return Vector4.create(x + vec.x, y + vec.y, z + vec.z, w + vec.w);
    }

    /**
     * Subtracts another vector from this one.
     *
     * @param vec other vector
     * @return result after operation
     */
    public Vector4 sub(Vector4 vec) {
        return Vector4.create(x - vec.x, y - vec.y, z - vec.z, w - vec.w);
    }

    /**
     * Returns distance from the other vector.
     *
     * @param vec vector
     * @return distance
     */
    public float dist(Vector4 vec) {
        float dx = x - vec.x;
        float dy = y - vec.y;
        float dz = z - vec.z;
        float dw = w - vec.w;
        return (float) Math.sqrt(dx * dx + dy * dy + dz * dz + dw * dw);
    }

    /**
     * Converts this vector out of homogenous coordinates.
     *
     * @return de homogenous vector
     */
    public Vector3 dehomogenize() {
        return Vector3.create(x / w, y / w, z / w);
    }

    @Override
    public int hashCode() {
        return (int) x + 13 * (int) y + 17 * (int) z + 19 * (int) w;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        else if (!(obj instanceof Vector4)) {
            return false;
        }
        Vector4 ob = (Vector4) obj;
        return ob.getX() == this.getX() && ob.getY() == this.getY() && ob.getZ() == this.getZ() && ob.getW() == this.getW();
    }

    @Override
    public String toString() {
        return new StringBuilder().append("(").append(x).append(", ").append(y).append(", ").append(z).append(", ").append(w).append(")").toString();
    }

    /**
     * Creates new instance.
     *
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     * @param w w coordinate
     * @return created vector
     */
    public static Vector4 create(float x, float y, float z, float w) {
        Vector4 res = new Vector4();
        res.x = x;
        res.y = y;
        res.z = z;
        res.w = w;
        res.guardInvariants();
        return res;
    }

}
