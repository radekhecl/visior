package com.enterprisemath.visior;

import java.util.Arrays;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Mask is an immutable and safe equivalent of the PlaneBuffer.
 * Planek has well defined hash code and equality methods
 *
 * @see PlaneBuffer
 * @author radek.hecl
 */
public class Plane {

    /**
     * Width.
     */
    private int width;

    /**
     * Height.
     */
    private int height;

    /**
     * Data buffer.
     */
    private float[] buf;

    /**
     * Creates matrix.
     */
    private Plane() {
    }

    /**
     * Guards this object to be consistent. Throws exception if this is not the case.
     */
    private void guardInvariants() {
        Validate.isTrue(width >= 0, "width must be >= 0");
        Validate.isTrue(height >= 0, "height must be >= 0");
        Validate.isTrue(buf.length == width * height, "data buffer length must match the dimension");
    }

    /**
     * Returns width.
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns height.
     *
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns dimension. This is width multiplied by height.
     *
     * @return dimension
     */
    public int getDimension() {
        return width * height;
    }

    /**
     * Copies the buffer to the new array.
     * Result is the copied array so changes in this doesn't affect the original.
     *
     * @return copied array buffer
     */
    public float[] copyBuf() {
        return Arrays.copyOf(buf, buf.length);
    }

    /**
     * Copies data buffer to the specified array.
     *
     * @param dest destination array, must be enough in length to cover the buffer
     */
    public void copyBuf(float[] dest) {
        System.arraycopy(buf, 0, dest, 0, buf.length);
    }

    /**
     * Returns element on the specified index.
     *
     * @param idx index
     * @return element
     */
    public float get(int idx) {
        return buf[idx];
    }

    /**
     * Returns the value on the specified row and column.
     *
     * @param x x position, column in the image notation
     * @param y y position, row in the image notation
     * @return value on the specified row and column
     */
    public float get(int x, int y) {
        return buf[y * width + x];
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates zero matrix.
     *
     * @param w width
     * @param h height
     *
     * @return zero matrix
     */
    public static Plane createZero(int w, int h) {
        return create(w, h, 0);
    }

    /**
     * Creates constant matrix.
     *
     * @param w width
     * @param h height
     * @param val constant value
     * @return created matrix
     */
    public static Plane createConst(int w, int h, float val) {
        Plane res = new Plane();
        res.width = w;
        res.height = h;
        res.buf = new float[w * h];
        for (int i = 0; i < res.buf.length; ++i) {
            res.buf[i] = val;
        }
        res.guardInvariants();
        return res;
    }

    /**
     * Creates matrix.
     *
     * @param w width
     * @param h height
     * @param buf data buffer
     * @return created matrix
     */
    public static Plane create(int w, int h, float... buf) {
        Plane res = new Plane();
        res.width = w;
        res.height = h;
        res.buf = Arrays.copyOf(buf, w * h);
        res.guardInvariants();
        return res;
    }

}
