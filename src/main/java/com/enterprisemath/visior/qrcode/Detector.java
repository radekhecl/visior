package com.enterprisemath.visior.qrcode;

import com.enterprisemath.visior.Contour2;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.enterprisemath.visior.MaskBuffer;
import com.enterprisemath.visior.Vector2;

public class Detector {

    private MaskBuffer image;

    public Detector(MaskBuffer image) {
        this.image = image;
    }

    public List<Contour2> detect() {
        FinderPatternFinder finder = new FinderPatternFinder(image);
        List<FinderPattern> finderPatterns = finder.find();
        if (finderPatterns.isEmpty()) {
            return Collections.emptyList();
        }

        Vector2 topLeft = finderPatterns.get(1).getCenterPoint();
        Vector2 topRight = finderPatterns.get(2).getCenterPoint();
        Vector2 bottomLeft = finderPatterns.get(0).getCenterPoint();

        float moduleSize = calculateModuleSize(topLeft, topRight, bottomLeft);
        int dimension = computeDimension(topLeft, topRight, bottomLeft, moduleSize);
        int modulesBetweenFPCenters = dimension - 7;

        AlignmentPattern alignmentPattern = null;

        // Guess where a "bottom right" finder pattern would have been
        float bottomRightX = topRight.getX() - topLeft.getX() + bottomLeft.getX();
        float bottomRightY = topRight.getY() - topLeft.getY() + bottomLeft.getY();

        // Estimate that alignment pattern is closer by 3 modules
        // from "bottom right" to known top left location
        float correctionToTopLeft = 1.0f - 3.0f / modulesBetweenFPCenters;
        int estAlignmentX
                = (int) (topLeft.getX() + correctionToTopLeft * (bottomRightX - topLeft.getX()));
        int estAlignmentY
                = (int) (topLeft.getY() + correctionToTopLeft * (bottomRightY - topLeft.getY()));

        // Kind of arbitrary -- expand search radius before giving up
        for (int i = 4; i <= 16 && alignmentPattern == null; i <<= 1) {
            alignmentPattern = findAlignmentInRegion(moduleSize, estAlignmentX, estAlignmentY, i);
        }

        Vector2 alignmentCenter = alignmentPattern != null ? alignmentPattern.getCenterPoint() : null;
        PerspectiveTransform transform
                = createTransform(topLeft, topRight, bottomLeft, alignmentCenter, dimension);

        List<Vector2> firstColEndPoints = calculateEndPointsForColumn(transform, dimension, 0);
        List<Vector2> lastColEndPoints
                = calculateEndPointsForColumn(transform, dimension, dimension - 1);

        Vector2 middle = firstColEndPoints.get(0);
        Vector2 thirdAfterMiddle = firstColEndPoints.get(1);

        Vector2 firstAfterMiddle = lastColEndPoints.get(0);
        Vector2 secondAfterMiddle = lastColEndPoints.get(1);

        return Arrays.asList(Contour2.create(middle, firstAfterMiddle, secondAfterMiddle, thirdAfterMiddle));
    }

    /**
     * Calculates top and bottom end points of a QR Code column. Top side is where middle finder
     * pattern resides.
     *
     * @param transform perspective transform that will be applied to find the endpoints
     * @param dimension dimension of the QR Code
     * @param colIndex column index
     * @return list of top and bottom points in order
     */
    private List<Vector2> calculateEndPointsForColumn(
            PerspectiveTransform transform, int dimension, int colIndex) {
        float[] points = new float[2 * dimension];
        int max = points.length;
        float iValue = colIndex + 0.5f;
        for (int x = 0; x < max; x += 2) {
            points[x] = (float) (x / 2) + 0.5f;
            points[x + 1] = iValue;
        }
        transform.transformPoints(points);
        return Arrays.asList(
                Vector2.create(points[0], points[1]), Vector2.create(points[max - 2], points[max - 1]));
    }

    /**
     * Computes the dimension (number of modules on a size) of the QR Code based on the position of
     * the finder patterns and estimated module size.
     */
    private static int computeDimension(
            Vector2 topLeft, Vector2 topRight, Vector2 bottomLeft, float moduleSize) {
        int tltrCentersDimension = Math.round(topLeft.dist(topRight) / moduleSize);
        int tlblCentersDimension = Math.round(topLeft.dist(bottomLeft) / moduleSize);
        int dimension = ((tltrCentersDimension + tlblCentersDimension) / 2) + 7;
        switch (dimension & 0x03) { // mod 4
        case 0:
            dimension++;
            break;
        // 1? do nothing
        case 2:
            dimension--;
            break;
        case 3:
            throw new RuntimeException("Failed to compute dimension");
        }

        if (!isValidDimension(dimension)) {
            throw new RuntimeException("Invalid dimension: " + dimension);
        }
        return dimension;
    }

    /**
     * Checks if dimension is a valid QR Code dimension
     *
     * @param dimension dimension in modules
     * @return true if dimension is valid, false otherwise
     */
    private static boolean isValidDimension(int dimension) {
        if (dimension % 4 != 1) {
            return false;
        }
        int versionNumber = (dimension - 17) / 4;
        return (versionNumber >= 1 && versionNumber <= 40);
    }

    /**
     * Computes an average estimated module size based on estimated derived from the positions of the
     * three finder patterns.
     *
     * @param topLeft detected top-left finder pattern center
     * @param topRight detected top-right finder pattern center
     * @param bottomLeft detected bottom-left finder pattern center
     * @return estimated module size
     */
    protected final float calculateModuleSize(Vector2 topLeft, Vector2 topRight, Vector2 bottomLeft) {
        // Take the average
        return (calculateModuleSizeOneWay(topLeft, topRight) +
                calculateModuleSizeOneWay(topLeft, bottomLeft)) /
                2.0f;
    }

    /**
     * Estimates module size based on two finder patterns -- it uses {@link
     * #sizeOfBlackWhiteBlackRunBothWays(int, int, int, int)} to figure the width of each, measuring
     * along the axis between their centers.
     */
    private float calculateModuleSizeOneWay(Vector2 pattern, Vector2 otherPattern) {
        float moduleSizeEst1
                = sizeOfBlackWhiteBlackRunBothWays(
                        (int) pattern.getX(),
                        (int) pattern.getY(),
                        (int) otherPattern.getX(),
                        (int) otherPattern.getY());
        float moduleSizeEst2
                = sizeOfBlackWhiteBlackRunBothWays(
                        (int) otherPattern.getX(),
                        (int) otherPattern.getY(),
                        (int) pattern.getX(),
                        (int) pattern.getY());
        if (Float.isNaN(moduleSizeEst1)) {
            return moduleSizeEst2 / 7.0f;
        }
        if (Float.isNaN(moduleSizeEst2)) {
            return moduleSizeEst1 / 7.0f;
        }
        // Average them, and divide by 7 since we've counted the width of 3 black modules,
        // and 1 white and 1 black module on either side. Ergo, divide sum by 14.
        return (moduleSizeEst1 + moduleSizeEst2) / 14.0f;
    }

    /**
     * See {@link #sizeOfBlackWhiteBlackRun(int, int, int, int)}; computes the total width of a finder
     * pattern by looking for a black-white-black run from the center in the direction of another
     * point (another finder pattern center), and in the opposite direction too.
     */
    private float sizeOfBlackWhiteBlackRunBothWays(int fromX, int fromY, int toX, int toY) {

        float result = sizeOfBlackWhiteBlackRun(fromX, fromY, toX, toY);

        // Now count other way -- don't run off image though of course
        float scale = 1.0f;
        int otherToX = fromX - (toX - fromX);
        if (otherToX < 0) {
            scale = fromX / (float) (fromX - otherToX);
            otherToX = 0;
        }
        else if (otherToX >= image.getWidth()) {
            scale = (image.getWidth() - 1 - fromX) / (float) (otherToX - fromX);
            otherToX = image.getWidth() - 1;
        }
        int otherToY = (int) (fromY - (toY - fromY) * scale);

        scale = 1.0f;
        if (otherToY < 0) {
            scale = fromY / (float) (fromY - otherToY);
            otherToY = 0;
        }
        else if (otherToY >= image.getHeight()) {
            scale = (image.getHeight() - 1 - fromY) / (float) (otherToY - fromY);
            otherToY = image.getHeight() - 1;
        }
        otherToX = (int) (fromX + (otherToX - fromX) * scale);

        result += sizeOfBlackWhiteBlackRun(fromX, fromY, otherToX, otherToY);

        // Middle pixel is double-counted this way; subtract 1
        return result - 1.0f;
    }

    /**
     * This method traces a line from a point in the image, in the direction towards another point. It
     * begins in a black region, and keeps going until it finds white, then black, then white again.
     * It reports the distance from the start to this point.
     *
     * <p>
     * This is used when figuring out how wide a finder pattern is, when the finder pattern may be
     * skewed or rotated.
     */
    private float sizeOfBlackWhiteBlackRun(int fromX, int fromY, int toX, int toY) {
        // Mild variant of Bresenham's algorithm;
        // see http://en.wikipedia.org/wiki/Bresenham's_line_algorithm
        boolean steep = Math.abs(toY - fromY) > Math.abs(toX - fromX);
        if (steep) {
            int temp = fromX;
            fromX = fromY;
            fromY = temp;
            temp = toX;
            toX = toY;
            toY = temp;
        }

        int dx = Math.abs(toX - fromX);
        int dy = Math.abs(toY - fromY);
        int error = -dx / 2;
        int xstep = fromX < toX ? 1 : -1;
        int ystep = fromY < toY ? 1 : -1;

        // In black pixels, looking for white, first or second time.
        int state = 0;
        // Loop up until x == toX, but not beyond
        int xLimit = toX + xstep;
        for (int x = fromX, y = fromY; x != xLimit; x += xstep) {
            int realX = steep ? y : x;
            int realY = steep ? x : y;

            // Does current pixel mean we have moved white to black or vice versa?
            // Scanning black in state 0,2 and white in state 1, so if we find the wrong
            // color, advance to next state or end if we are in state 2 already
            if ((state == 1) == isImageBitBlack(realX, realY)) {
                if (state == 2) {
                    return Vector2.create(x, y).dist(Vector2.create(fromX, fromY));
                }
                state++;
            }

            error += dy;
            if (error > 0) {
                if (y == toY) {
                    break;
                }
                y += ystep;
                error -= dx;
            }
        }
        // Found black-white-black; give the benefit of the doubt that the next pixel outside the image
        // is "white" so this last point at (toX+xStep,toY) is the right ending. This is really a
        // small approximation; (toX+xStep,toY+yStep) might be really correct. Ignore this.
        if (state == 2) {
            return Vector2.create(toX + xstep, toY).dist(Vector2.create(fromX, fromY));
        }
        // else we didn't find even black-white-black; no estimate is really possible
        return Float.NaN;
    }

    /**
     * Attempts to locate an alignment pattern in a limited region of the image, which is guessed to
     * contain it. This method uses {@link AlignmentPattern}.
     *
     * @param overallEstModuleSize estimated module size so far
     * @param estAlignmentX x coordinate of center of area probably containing alignment pattern
     * @param estAlignmentY y coordinate of above
     * @param allowanceFactor number of pixels in all directions to search from the center
     * @return {@link AlignmentPattern} if found, or null otherwise
     */
    protected final AlignmentPattern findAlignmentInRegion(
            float overallEstModuleSize, int estAlignmentX, int estAlignmentY, float allowanceFactor) {
        // Look for an alignment pattern (3 modules in size) around where it
        // should be
        int allowance = (int) (allowanceFactor * overallEstModuleSize);
        int alignmentAreaLeftX = Math.max(0, estAlignmentX - allowance);
        int alignmentAreaRightX = Math.min(image.getWidth() - 1, estAlignmentX + allowance);
        if (alignmentAreaRightX - alignmentAreaLeftX < overallEstModuleSize * 3) {
            return null;
        }

        int alignmentAreaTopY = Math.max(0, estAlignmentY - allowance);
        int alignmentAreaBottomY = Math.min(image.getHeight() - 1, estAlignmentY + allowance);
        if (alignmentAreaBottomY - alignmentAreaTopY < overallEstModuleSize * 3) {
            return null;
        }

        AlignmentPatternFinder alignmentFinder
                = new AlignmentPatternFinder(
                        image,
                        alignmentAreaLeftX,
                        alignmentAreaTopY,
                        alignmentAreaRightX - alignmentAreaLeftX,
                        alignmentAreaBottomY - alignmentAreaTopY,
                        overallEstModuleSize);
        return alignmentFinder.find();
    }

    private static PerspectiveTransform createTransform(
            Vector2 topLeft,
            Vector2 topRight,
            Vector2 bottomLeft,
            Vector2 alignmentPattern,
            int dimension) {
        float dimMinusThree = dimension - 3.5f;
        float bottomRightX;
        float bottomRightY;
        float sourceBottomRightX;
        float sourceBottomRightY;
        if (alignmentPattern != null) {
            bottomRightX = alignmentPattern.getX();
            bottomRightY = alignmentPattern.getY();
            sourceBottomRightX = dimMinusThree - 3.0f;
            sourceBottomRightY = sourceBottomRightX;
        }
        else {
            // Don't have an alignment pattern, just make up the bottom-right point
            bottomRightX = (topRight.getX() - topLeft.getX()) + bottomLeft.getX();
            bottomRightY = (topRight.getY() - topLeft.getY()) + bottomLeft.getY();
            sourceBottomRightX = dimMinusThree;
            sourceBottomRightY = dimMinusThree;
        }

        return PerspectiveTransform.quadrilateralToQuadrilateral(
                3.5f,
                3.5f,
                dimMinusThree,
                3.5f,
                sourceBottomRightX,
                sourceBottomRightY,
                3.5f,
                dimMinusThree,
                topLeft.getX(),
                topLeft.getY(),
                topRight.getX(),
                topRight.getY(),
                bottomRightX,
                bottomRightY,
                bottomLeft.getX(),
                bottomLeft.getY());
    }

    private boolean isImageBitBlack(int x, int y) {
        return !image.get(x, y);
    }
}
