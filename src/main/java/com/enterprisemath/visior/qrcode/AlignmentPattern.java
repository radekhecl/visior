/*
 * Copyright 2007 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.enterprisemath.visior.qrcode;

import com.enterprisemath.visior.Vector2;

/**
 * Alignment pattern definition. These are smaller square patterns found in all but the simplest QR Codes.
 *
 * @author sean.owen
 */
public final class AlignmentPattern {

    /**
     * Center point.
     */
    private Vector2 centerPoint;

    /**
     * Estimated module size.
     */
    private final float estimatedModuleSize;

    /**
     * Creates new instance.
     *
     * @param posX position x
     * @param posY position y
     * @param estimatedModuleSize
     */
    public AlignmentPattern(float posX, float posY, float estimatedModuleSize) {
        this.centerPoint = Vector2.create(posX, posY);
        this.estimatedModuleSize = estimatedModuleSize;
    }

    /**
     * Returns center point.
     *
     * @return center point
     */
    public Vector2 getCenterPoint() {
        return centerPoint;
    }

    /**
     * Returns whether alignment pattern is similar to the given values.
     * Mean nearly same center with nearly same size.
     *
     * @param moduleSize module size
     * @param i center x
     * @param j center y
     * @return true if alignment pattern is about same, false otherwise
     */
    public boolean aboutEquals(float moduleSize, float i, float j) {
        if (Math.abs(i - centerPoint.getY()) <= moduleSize && Math.abs(j - centerPoint.getX()) <= moduleSize) {
            float moduleSizeDiff = Math.abs(moduleSize - estimatedModuleSize);
            return moduleSizeDiff <= 1.0f || moduleSizeDiff <= estimatedModuleSize;
        }
        return false;
    }

    /**
     * Combines with another estimation.
     *
     * @param i other x
     * @param j other y
     * @param newModuleSize other module size
     * @return combined pattern
     */
    public AlignmentPattern combineEstimate(float i, float j, float newModuleSize) {
        float combinedX = (centerPoint.getX() + j) / 2.0f;
        float combinedY = (centerPoint.getY() + i) / 2.0f;
        float combinedModuleSize = (estimatedModuleSize + newModuleSize) / 2.0f;
        return new AlignmentPattern(combinedX, combinedY, combinedModuleSize);
    }

}
