/*
 * Copyright 2007 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.enterprisemath.visior.qrcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.enterprisemath.visior.MaskBuffer;
import com.enterprisemath.visior.Vector2;

/**
 * This class attempts to find finder patterns in a QR Code. Finder patterns are the square markers
 * at three corners of a QR Code.
 *
 * <p>
 * This class is thread-safe but not reentrant. Each thread must allocate its own object.
 *
 * @author Sean Owen
 */
public class FinderPatternFinder {

    private static final int CENTER_QUORUM = 2;

    protected static final int MIN_SKIP = 3; // 1 pixel/module times 3 modules/center

    private final MaskBuffer image;

    private final List<FinderPattern> possibleCenters;

    private boolean hasSkipped;

    private final int[] crossCheckStateCount;

    /**
     * Creates a finder that will search the image for three finder patterns.
     *
     * @param image image to search
     */
    public FinderPatternFinder(MaskBuffer image) {
        this.image = image;
        this.possibleCenters = new ArrayList<>();
        this.crossCheckStateCount = new int[5];
    }

    protected final MaskBuffer getImage() {
        return image;
    }

    protected final List<FinderPattern> getPossibleCenters() {
        return possibleCenters;
    }

    public List<FinderPattern> find() {
        int maxI = image.getHeight();
        int maxJ = image.getWidth();
        // We are looking for black/white/black/white/black modules in
        // 1:1:3:1:1 ratio; this tracks the number of such modules seen so far

        int iSkip = MIN_SKIP;
        boolean done = false;
        int[] stateCount = new int[5];
        for (int i = iSkip - 1; i < maxI && !done; i += iSkip) {
            // Get a row of black/white values
            clearCounts(stateCount);
            int currentState = 0;
            for (int j = 0; j < maxJ; j++) {
                if (isImageBitBlack(j, i)) {
                    // Black pixel
                    if ((currentState & 1) == 1) { // Counting white pixels
                        currentState++;
                    }
                    stateCount[currentState]++;
                }
                else { // White pixel
                    if ((currentState & 1) == 0) { // Counting black pixels
                        if (currentState == 4) { // A winner?
                            if (foundPatternCross(stateCount)) { // Yes
                                boolean confirmed = handlePossibleCenter(stateCount, i, j);
                                if (confirmed) {
                                    // Start examining every other line. Checking each line turned out to be too
                                    // expensive and didn't improve performance.
                                    iSkip = 2;
                                    if (hasSkipped) {
                                        done = haveMultiplyConfirmedCenters();
                                    }
                                    else {
                                        int rowSkip = findRowSkip();
                                        if (rowSkip > stateCount[2]) {
                                            // Skip rows between row of lower confirmed center
                                            // and top of presumed third confirmed center
                                            // but back up a bit to get a full chance of detecting
                                            // it, entire width of center of finder pattern

                                            // Skip by rowSkip, but back off by stateCount[2] (size of last center
                                            // of pattern we saw) to be conservative, and also back off by iSkip which
                                            // is about to be re-added
                                            i += rowSkip - stateCount[2] - iSkip;
                                            j = maxJ - 1;
                                        }
                                    }
                                }
                                else {
                                    shiftCounts2(stateCount);
                                    currentState = 3;
                                    continue;
                                }
                                // Clear state to start looking again
                                currentState = 0;
                                clearCounts(stateCount);
                            }
                            else { // No, shift counts back by two
                                shiftCounts2(stateCount);
                                currentState = 3;
                            }
                        }
                        else {
                            stateCount[++currentState]++;
                        }
                    }
                    else { // Counting white pixels
                        stateCount[currentState]++;
                    }
                }
            }
            if (foundPatternCross(stateCount)) {
                boolean confirmed = handlePossibleCenter(stateCount, i, maxJ);
                if (confirmed) {
                    iSkip = stateCount[0];
                    if (hasSkipped) {
                        // Found a third one
                        done = haveMultiplyConfirmedCenters();
                    }
                }
            }
        }

        List<FinderPattern> finderPatterns = selectBestPatterns();
        if (!finderPatterns.isEmpty()) {
            orderBestPatterns(finderPatterns);
        }
        return finderPatterns;
    }

    /**
     * Given a count of black/white/black/white/black pixels just seen and an end position, figures
     * the location of the center of this run.
     */
    private static float centerFromEnd(int[] stateCount, int end) {
        return (end - stateCount[4] - stateCount[3]) - stateCount[2] / 2.0f;
    }

    /**
     * @param stateCount count of black/white/black/white/black pixels just read
     * @return true iff the proportions of the counts is close enough to the 1/1/3/1/1 ratios used by
     * finder patterns to be considered a match
     */
    protected static boolean foundPatternCross(int[] stateCount) {
        int totalModuleSize = 0;
        for (int i = 0; i < 5; i++) {
            int count = stateCount[i];
            if (count == 0) {
                return false;
            }
            totalModuleSize += count;
        }
        if (totalModuleSize < 7) {
            return false;
        }
        float moduleSize = totalModuleSize / 7.0f;
        float maxVariance = moduleSize / 2.0f;
        // Allow less than 50% variance from 1-1-3-1-1 proportions
        return Math.abs(moduleSize - stateCount[0]) < maxVariance &&
                Math.abs(moduleSize - stateCount[1]) < maxVariance &&
                Math.abs(3.0f * moduleSize - stateCount[2]) < 3 * maxVariance &&
                Math.abs(moduleSize - stateCount[3]) < maxVariance &&
                Math.abs(moduleSize - stateCount[4]) < maxVariance;
    }

    /**
     * @param stateCount count of black/white/black/white/black pixels just read
     * @return true iff the proportions of the counts is close enough to the 1/1/3/1/1 ratios used by
     * finder patterns to be considered a match
     */
    protected static boolean foundPatternDiagonal(int[] stateCount) {
        int totalModuleSize = 0;
        for (int i = 0; i < 5; i++) {
            int count = stateCount[i];
            if (count == 0) {
                return false;
            }
            totalModuleSize += count;
        }
        if (totalModuleSize < 7) {
            return false;
        }
        float moduleSize = totalModuleSize / 7.0f;
        float maxVariance = moduleSize / 1.333f;
        // Allow less than 75% variance from 1-1-3-1-1 proportions
        return Math.abs(moduleSize - stateCount[0]) < maxVariance &&
                Math.abs(moduleSize - stateCount[1]) < maxVariance &&
                Math.abs(3.0f * moduleSize - stateCount[2]) < 3 * maxVariance &&
                Math.abs(moduleSize - stateCount[3]) < maxVariance &&
                Math.abs(moduleSize - stateCount[4]) < maxVariance;
    }

    private int[] getCrossCheckStateCount() {
        clearCounts(crossCheckStateCount);
        return crossCheckStateCount;
    }

    protected final void clearCounts(int[] counts) {
        for (int x = 0; x < counts.length; x++) {
            counts[x] = 0;
        }
    }

    protected final void shiftCounts2(int[] stateCount) {
        stateCount[0] = stateCount[2];
        stateCount[1] = stateCount[3];
        stateCount[2] = stateCount[4];
        stateCount[3] = 1;
        stateCount[4] = 0;
    }

    /**
     * After a vertical and horizontal scan finds a potential finder pattern, this method
     * "cross-cross-cross-checks" by scanning down diagonally through the center of the possible
     * finder pattern to see if the same proportion is detected.
     *
     * @param centerI row where a finder pattern was detected
     * @param centerJ center of the section that appears to cross a finder pattern
     * @return true if proportions are withing expected limits
     */
    private boolean crossCheckDiagonal(int centerI, int centerJ) {
        int[] stateCount = getCrossCheckStateCount();

        // Start counting up, left from center finding black center mass
        int i = 0;

        while (centerI >= i && centerJ >= i && isImageBitBlack(centerJ - i, centerI - i)) {
            stateCount[2]++;
            i++;
        }
        if (stateCount[2] == 0) {
            return false;
        }

        // Continue up, left finding white space
        while (centerI >= i && centerJ >= i && !isImageBitBlack(centerJ - i, centerI - i)) {
            stateCount[1]++;
            i++;
        }
        if (stateCount[1] == 0) {
            return false;
        }

        // Continue up, left finding black border
        while (centerI >= i && centerJ >= i && isImageBitBlack(centerJ - i, centerI - i)) {
            stateCount[0]++;
            i++;
        }
        if (stateCount[0] == 0) {
            return false;
        }

        int maxI = image.getHeight();
        int maxJ = image.getWidth();

        // Now also count down, right from center
        i = 1;
        while (centerI + i < maxI && centerJ + i < maxJ && isImageBitBlack(centerJ + i, centerI + i)) {
            stateCount[2]++;
            i++;
        }

        while (centerI + i < maxI && centerJ + i < maxJ && !isImageBitBlack(centerJ + i, centerI + i)) {
            stateCount[3]++;
            i++;
        }
        if (stateCount[3] == 0) {
            return false;
        }

        while (centerI + i < maxI && centerJ + i < maxJ && isImageBitBlack(centerJ + i, centerI + i)) {
            stateCount[4]++;
            i++;
        }
        if (stateCount[4] == 0) {
            return false;
        }

        return foundPatternDiagonal(stateCount);
    }

    /**
     * After a horizontal scan finds a potential finder pattern, this method "cross-checks" by
     * scanning down vertically through the center of the possible finder pattern to see if the same
     * proportion is detected.
     *
     * @param startI row where a finder pattern was detected
     * @param centerJ center of the section that appears to cross a finder pattern
     * @param maxCount maximum reasonable number of modules that should be observed in any reading
     * state, based on the results of the horizontal scan
     * @return vertical center of finder pattern, or {@link Float#NaN} if not found
     */
    private float crossCheckVertical(
            int startI, int centerJ, int maxCount, int originalStateCountTotal) {
        MaskBuffer image = this.image;

        int maxI = image.getHeight();
        int[] stateCount = getCrossCheckStateCount();

        // Start counting up from center
        int i = startI;
        while (i >= 0 && isImageBitBlack(centerJ, i)) {
            stateCount[2]++;
            i--;
        }
        if (i < 0) {
            return Float.NaN;
        }
        while (i >= 0 && !isImageBitBlack(centerJ, i) && stateCount[1] <= maxCount) {
            stateCount[1]++;
            i--;
        }
        // If already too many modules in this state or ran off the edge:
        if (i < 0 || stateCount[1] > maxCount) {
            return Float.NaN;
        }
        while (i >= 0 && isImageBitBlack(centerJ, i) && stateCount[0] <= maxCount) {
            stateCount[0]++;
            i--;
        }
        if (stateCount[0] > maxCount) {
            return Float.NaN;
        }

        // Now also count down from center
        i = startI + 1;
        while (i < maxI && isImageBitBlack(centerJ, i)) {
            stateCount[2]++;
            i++;
        }
        if (i == maxI) {
            return Float.NaN;
        }
        while (i < maxI && !isImageBitBlack(centerJ, i) && stateCount[3] < maxCount) {
            stateCount[3]++;
            i++;
        }
        if (i == maxI || stateCount[3] >= maxCount) {
            return Float.NaN;
        }
        while (i < maxI && isImageBitBlack(centerJ, i) && stateCount[4] < maxCount) {
            stateCount[4]++;
            i++;
        }
        if (stateCount[4] >= maxCount) {
            return Float.NaN;
        }

        // If we found a finder-pattern-like section, but its size is more than 40% different than
        // the original, assume it's a false positive
        int stateCountTotal
                = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
        if (5 * Math.abs(stateCountTotal - originalStateCountTotal) >= 2 * originalStateCountTotal) {
            return Float.NaN;
        }

        return foundPatternCross(stateCount) ? centerFromEnd(stateCount, i) : Float.NaN;
    }

    /**
     * Like {@link #crossCheckVertical(int, int, int, int)}, and in fact is basically identical,
     * except it reads horizontally instead of vertically. This is used to cross-cross check a
     * vertical cross check and locate the real center of the alignment pattern.
     */
    private float crossCheckHorizontal(
            int startJ, int centerI, int maxCount, int originalStateCountTotal) {
        MaskBuffer image = this.image;

        int maxJ = image.getWidth();
        int[] stateCount = getCrossCheckStateCount();

        int j = startJ;
        while (j >= 0 && isImageBitBlack(j, centerI)) {
            stateCount[2]++;
            j--;
        }
        if (j < 0) {
            return Float.NaN;
        }
        while (j >= 0 && !isImageBitBlack(j, centerI) && stateCount[1] <= maxCount) {
            stateCount[1]++;
            j--;
        }
        if (j < 0 || stateCount[1] > maxCount) {
            return Float.NaN;
        }
        while (j >= 0 && isImageBitBlack(j, centerI) && stateCount[0] <= maxCount) {
            stateCount[0]++;
            j--;
        }
        if (stateCount[0] > maxCount) {
            return Float.NaN;
        }

        j = startJ + 1;
        while (j < maxJ && isImageBitBlack(j, centerI)) {
            stateCount[2]++;
            j++;
        }
        if (j == maxJ) {
            return Float.NaN;
        }
        while (j < maxJ && !isImageBitBlack(j, centerI) && stateCount[3] < maxCount) {
            stateCount[3]++;
            j++;
        }
        if (j == maxJ || stateCount[3] >= maxCount) {
            return Float.NaN;
        }
        while (j < maxJ && isImageBitBlack(j, centerI) && stateCount[4] < maxCount) {
            stateCount[4]++;
            j++;
        }
        if (stateCount[4] >= maxCount) {
            return Float.NaN;
        }

        // If we found a finder-pattern-like section, but its size is significantly different than
        // the original, assume it's a false positive
        int stateCountTotal
                = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
        if (5 * Math.abs(stateCountTotal - originalStateCountTotal) >= originalStateCountTotal) {
            return Float.NaN;
        }

        return foundPatternCross(stateCount) ? centerFromEnd(stateCount, j) : Float.NaN;
    }

    /**
     * @param stateCount reading state module counts from horizontal scan
     * @param i row where finder pattern may be found
     * @param j end of possible finder pattern in row
     * @param pureBarcode ignored
     * @return true if a finder pattern candidate was found this time
     * @deprecated only exists for backwards compatibility
     * @see #handlePossibleCenter(int[], int, int)
     */
    @Deprecated
    protected final boolean handlePossibleCenter(
            int[] stateCount, int i, int j, boolean pureBarcode) {
        return handlePossibleCenter(stateCount, i, j);
    }

    /**
     * This is called when a horizontal scan finds a possible alignment pattern. It will cross check
     * with a vertical scan, and if successful, will, ah, cross-cross-check with another horizontal
     * scan. This is needed primarily to locate the real horizontal center of the pattern in cases of
     * extreme skew. And then we cross-cross-cross check with another diagonal scan.
     *
     * <p>
     * If that succeeds the finder pattern location is added to a list that tracks the number of
     * times each location has been nearly-matched as a finder pattern. Each additional find is more
     * evidence that the location is in fact a finder pattern center
     *
     * @param stateCount reading state module counts from horizontal scan
     * @param i row where finder pattern may be found
     * @param j end of possible finder pattern in row
     * @return true if a finder pattern candidate was found this time
     */
    protected final boolean handlePossibleCenter(int[] stateCount, int i, int j) {
        int stateCountTotal
                = stateCount[0] + stateCount[1] + stateCount[2] + stateCount[3] + stateCount[4];
        float centerJ = centerFromEnd(stateCount, j);
        float centerI = crossCheckVertical(i, (int) centerJ, stateCount[2], stateCountTotal);
        if (!Float.isNaN(centerI)) {
            // Re-cross check
            centerJ = crossCheckHorizontal((int) centerJ, (int) centerI, stateCount[2], stateCountTotal);
            if (!Float.isNaN(centerJ) && crossCheckDiagonal((int) centerI, (int) centerJ)) {
                float estimatedModuleSize = stateCountTotal / 7.0f;
                boolean found = false;
                for (int index = 0; index < possibleCenters.size(); index++) {
                    FinderPattern center = possibleCenters.get(index);
                    // Look for about the same center and module size:
                    if (center.aboutEquals(estimatedModuleSize, centerI, centerJ)) {
                        possibleCenters.set(
                                index, center.combineEstimate(centerI, centerJ, estimatedModuleSize));
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    FinderPattern point = new FinderPattern(centerJ, centerI, estimatedModuleSize);
                    possibleCenters.add(point);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * @return number of rows we could safely skip during scanning, based on the first two finder
     * patterns that have been located. In some cases their position will allow us to infer that
     * the third pattern must lie below a certain point farther down in the image.
     */
    private int findRowSkip() {
        int max = possibleCenters.size();
        if (max <= 1) {
            return 0;
        }
        FinderPattern firstConfirmedCenter = null;
        for (FinderPattern center : possibleCenters) {
            if (center.getCount() >= CENTER_QUORUM) {
                if (firstConfirmedCenter == null) {
                    firstConfirmedCenter = center;
                }
                else {
                    // We have two confirmed centers
                    // How far down can we skip before resuming looking for the next
                    // pattern? In the worst case, only the difference between the
                    // difference in the x / y coordinates of the two centers.
                    // This is the case where you find top left last.
                    hasSkipped = true;
                    return (int) (Math.abs(
                            firstConfirmedCenter.getCenterPoint().getX() -
                            center.getCenterPoint().getX()) -
                            Math.abs(
                                    firstConfirmedCenter.getCenterPoint().getY() -
                                    center.getCenterPoint().getY())) /
                            2;
                }
            }
        }
        return 0;
    }

    /**
     * @return true iff we have found at least 3 finder patterns that have been detected at least
     * {@link #CENTER_QUORUM} times each, and, the estimated module size of the candidates is
     * "pretty similar"
     */
    private boolean haveMultiplyConfirmedCenters() {
        int confirmedCount = 0;
        float totalModuleSize = 0.0f;
        int max = possibleCenters.size();
        for (FinderPattern pattern : possibleCenters) {
            if (pattern.getCount() >= CENTER_QUORUM) {
                confirmedCount++;
                totalModuleSize += pattern.getEstimatedModuleSize();
            }
        }
        if (confirmedCount < 3) {
            return false;
        }
        // OK, we have at least 3 confirmed centers, but, it's possible that one is a "false positive"
        // and that we need to keep looking. We detect this by asking if the estimated module sizes
        // vary too much. We arbitrarily say that when the total deviation from average exceeds
        // 5% of the total module size estimates, it's too much.
        float average = totalModuleSize / max;
        float totalDeviation = 0.0f;
        for (FinderPattern pattern : possibleCenters) {
            totalDeviation += Math.abs(pattern.getEstimatedModuleSize() - average);
        }
        return totalDeviation <= 0.05f * totalModuleSize;
        //    return totalDeviation <= 0.15f * totalModuleSize;
    }

    /**
     * @return the 3 best {@link FinderPattern}s from our list of candidates. The "best" are those
     * that have been detected at least {@link #CENTER_QUORUM} times, and whose module size
     * differs from the average among those patterns the least
     * @throws NotFoundException if 3 such finder patterns do not exist
     */
    private List<FinderPattern> selectBestPatterns() {

        int startSize = possibleCenters.size();
        if (startSize < 3) {
            // Couldn't find enough finder patterns
            return Collections.emptyList();
        }

        // Filter outlier possibilities whose module size is too different
        if (startSize > 3) {
            // But we can only afford to do so if we have at least 4 possibilities to choose from
            double totalModuleSize = 0.0;
            double square = 0.0;
            for (FinderPattern center : possibleCenters) {
                float size = center.getEstimatedModuleSize();
                totalModuleSize += size;
                square += (double) size * size;
            }
            double average = totalModuleSize / startSize;
            float stdDev = (float) Math.sqrt(square / startSize - average * average);

            Collections.sort(possibleCenters, new FurthestFromAverageComparator((float) average));

            float limit = Math.max(0.2f * (float) average, stdDev);

            for (int i = 0; i < possibleCenters.size() && possibleCenters.size() > 3; i++) {
                FinderPattern pattern = possibleCenters.get(i);
                if (Math.abs(pattern.getEstimatedModuleSize() - average) > limit) {
                    possibleCenters.remove(i);
                    i--;
                }
            }
        }

        if (possibleCenters.size() > 3) {
            // Throw away all but those first size candidate points we found.

            float totalModuleSize = 0.0f;
            for (FinderPattern possibleCenter : possibleCenters) {
                totalModuleSize += possibleCenter.getEstimatedModuleSize();
            }

            float average = totalModuleSize / possibleCenters.size();

            Collections.sort(possibleCenters, new CenterComparator(average));

            possibleCenters.subList(3, possibleCenters.size()).clear();
        }

        return Arrays.asList(possibleCenters.get(0), possibleCenters.get(1), possibleCenters.get(2));
    }

    private boolean isImageBitBlack(int x, int y) {
        return !image.get(x, y);
    }

    /**
     * Orders an array of three ResultPoints in an order [A,B,C] such that AB is less than AC and BC
     * is less than AC, and the angle between BC and BA is less than 180 degrees.
     *
     * @param patterns array of three {@code ResultPoint} to order
     */
    public static void orderBestPatterns(List<FinderPattern> patterns) {
        // Find distances between pattern centers
        float zeroOneDistance = patterns.get(0).getCenterPoint().dist(patterns.get(1).getCenterPoint());
        float oneTwoDistance = patterns.get(1).getCenterPoint().dist(patterns.get(2).getCenterPoint());
        float zeroTwoDistance = patterns.get(0).getCenterPoint().dist(patterns.get(2).getCenterPoint());

        FinderPattern pointA;
        FinderPattern pointB;
        FinderPattern pointC;
        // Assume one closest to other two is B; A and C will just be guesses at first
        if (oneTwoDistance >= zeroOneDistance && oneTwoDistance >= zeroTwoDistance) {
            pointB = patterns.get(0);
            pointA = patterns.get(1);
            pointC = patterns.get(2);
        }
        else if (zeroTwoDistance >= oneTwoDistance && zeroTwoDistance >= zeroOneDistance) {
            pointB = patterns.get(1);
            pointA = patterns.get(0);
            pointC = patterns.get(2);
        }
        else {
            pointB = patterns.get(2);
            pointA = patterns.get(0);
            pointC = patterns.get(1);
        }

        // Use cross product to figure out whether A and C are correct or flipped.
        // This asks whether BC x BA has a positive z component, which is the arrangement
        // we want for A, B, C. If it's negative, then we've got it flipped around and
        // should swap A and C.
        if (crossProductZ(pointA.getCenterPoint(), pointB.getCenterPoint(), pointC.getCenterPoint()) < 0.0f) {
            FinderPattern temp = pointA;
            pointA = pointC;
            pointC = temp;
        }

        patterns.set(0, pointA);
        patterns.set(1, pointB);
        patterns.set(2, pointC);
    }

    /** Returns the z component of the cross product between vectors BC and BA. */
    private static float crossProductZ(Vector2 pointA, Vector2 pointB, Vector2 pointC) {
        float bX = pointB.getX();
        float bY = pointB.getY();
        return ((pointC.getX() - bX) * (pointA.getY() - bY)) -
                ((pointC.getY() - bY) * (pointA.getX() - bX));
    }

    /** Orders by furthest from average */
    private static final class FurthestFromAverageComparator implements Comparator<FinderPattern> {

        private final float average;

        private FurthestFromAverageComparator(float f) {
            average = f;
        }

        @Override
        public int compare(FinderPattern center1, FinderPattern center2) {
            return Float.compare(
                    Math.abs(center2.getEstimatedModuleSize() - average),
                    Math.abs(center1.getEstimatedModuleSize() - average));
        }
    }

    /** Orders by {@link FinderPattern#getCount()}, descending. */
    private static final class CenterComparator implements Comparator<FinderPattern> {

        private final float average;

        private CenterComparator(float f) {
            average = f;
        }

        @Override
        public int compare(FinderPattern center1, FinderPattern center2) {
            int countCompare = Integer.compare(center2.getCount(), center1.getCount());
            if (countCompare == 0) {
                return Float.compare(
                        Math.abs(center1.getEstimatedModuleSize() - average),
                        Math.abs(center2.getEstimatedModuleSize() - average));
            }
            return countCompare;
        }
    }
}
