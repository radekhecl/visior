package com.enterprisemath.visior;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Class for common fitting algorithms.
 *
 * @author radek.hecl
 */
public class Fitting {

    /**
     * Random number generator.
     */
    private static final Random RANDOM = new Random();

    /**
     * Prevents construction.
     */
    private Fitting() {
    }

    /**
     * Fits line through the least square method.
     *
     * @param points points, must be at minimum 2 non equal points
     * @return fitted line
     */
    public static GeneralLine2 lineLeastSqr(Collection<Vector2> points) {
        float mx = 0;
        float my = 0;
        for (Vector2 p : points) {
            mx = mx + p.getX();
            my = my + p.getY();
        }
        mx = mx / points.size();
        my = my / points.size();
        float mxx = 0;
        float myy = 0;
        float mxy = 0;
        for (Vector2 p : points) {
            mxx = mxx + (p.getX() - mx) * (p.getX() - mx);
            myy = myy + (p.getY() - my) * (p.getY() - my);
            mxy = mxy + (p.getX() - mx) * (p.getY() - my);
        }
        if (mxx > myy) {
            float m = mxy / mxx;
            float b = my - m * mx;
            Vector2 n = Vector2.create(-m, 1).normalize();
            float c = -n.getY() * b;
            return GeneralLine2.create(n, c);
        }
        else {
            float m = mxy / myy;
            float a = mx - m * my;
            Vector2 n = Vector2.create(1, -m).normalize();
            float c = -n.getX() * a;
            return GeneralLine2.create(n, c);
        }
    }

    /**
     * Fits the lines by RANSAC method.
     *
     * @param pts initial input points
     * @param maxLines maximum number of lines, which are fitted
     * @param maxIterations maximal number of iterations
     * @param minAccept minimal number of points which must be on line to make the guess accepted
     * @param dstThres threshold of distance for point to be considered as on line (inclusive)
     * @return fitted lines
     */
    public static List<GeneralLine2> linesRansac(List<Vector2> pts, int maxLines, int maxIterations, int minAccept, float dstThres) {
        List<GeneralLine2> res = new ArrayList<>();
        for (int i = 0; i < maxIterations && res.size() < maxLines; ++i) {
            // pickup random points and make sure they are not the same
            Vector2 start = pts.get(RANDOM.nextInt(pts.size()));
            Vector2 end = pts.get(RANDOM.nextInt(pts.size()));
            while (start.equals(end)) {
                end = pts.get(RANDOM.nextInt(pts.size()));
            }
            // construct line and verify how many points are fitting into it
            GeneralLine2 line = GeneralLine2.create(start, end);
            List<Vector2> outliers = new ArrayList<>();
            for (Vector2 pt : pts) {
                if (line.dist(pt) > dstThres) {
                    outliers.add(pt);
                }
            }
            int in = pts.size() - outliers.size();
            if (in >= minAccept) {
                res.add(line);
                pts = outliers;
            }
        }
        return res;
    }

}
