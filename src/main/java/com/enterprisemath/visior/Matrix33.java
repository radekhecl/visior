package com.enterprisemath.visior;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Matrix of 3 rows and 3 columns.
 *
 * @author radek.hecl
 */
public class Matrix33 {

    /**
     * Identity matrix.
     */
    public static final Matrix33 IDENTITY = create(1, 0, 0, 0, 1, 0, 0, 0, 1);

    /**
     * Elements of the first row.
     */
    private float m00, m01, m02;

    /**
     * Elements of the second row.
     */
    private float m10, m11, m12;

    /**
     * Elements of the third row.
     */
    private float m20, m21, m22;

    /**
     * Creates matrix.
     */
    private Matrix33() {
    }

    /**
     * Returns first row first column.
     *
     * @return first row first column
     */
    public float m00() {
        return m00;
    }

    /**
     * Returns first row second column.
     *
     * @return first row second column
     */
    public float m01() {
        return m01;
    }

    /**
     * Returns first row third column.
     *
     * @return first row third column
     */
    public float m02() {
        return m02;
    }

    /**
     * Returns second row first column.
     *
     * @return second row first column
     */
    public float m10() {
        return m10;
    }

    /**
     * Returns second row second column.
     *
     * @return second row second column
     */
    public float m11() {
        return m11;
    }

    /**
     * Returns second row third column.
     *
     * @return second row third column
     */
    public float m12() {
        return m12;
    }

    /**
     * Returns third row first column.
     *
     * @return third row first column
     */
    public float m20() {
        return m20;
    }

    /**
     * Returns third row second column.
     *
     * @return third row second column
     */
    public float m21() {
        return m21;
    }

    /**
     * Returns third row third column.
     *
     * @return third row third column
     */
    public float m22() {
        return m22;
    }

    /**
     * Returns column as a vector.
     *
     * @param idx column index, can be 0, 1, 2
     * @return column
     */
    public Vector3 getCol(int idx) {
        switch (idx) {
        case 0:
            return Vector3.create(m00, m10, m20);
        case 1:
            return Vector3.create(m01, m11, m21);
        case 2:
            return Vector3.create(m02, m12, m22);
        default:
            throw new IllegalArgumentException("idx can be only 0, 1 or 2: " + idx);
        }
    }

    /**
     * Returns determinant.
     *
     * @return determinant
     */
    public float det() {
        float v1 = (m11 * m22 - m12 * m21) * m00;
        float v2 = (m01 * m22 - m02 * m21) * m10;
        float v3 = (m01 * m12 - m02 * m11) * m20;
        return v1 - v2 + v3;
    }

    /**
     * Returns inversion matrix.
     *
     * @return inversion matrix, returns null if matrix does not have inversion
     */
    public Matrix33 inv() {
        float det = det();
        if (det < 1e-7f) {
            return null;
        }
        Matrix33 res = new Matrix33();
        res.m00 = (m11 * m22 - m21 * m12) / det;
        res.m01 = (m21 * m02 - m01 * m22) / det;
        res.m02 = (m01 * m12 - m11 * m02) / det;
        res.m10 = (m20 * m12 - m10 * m22) / det;
        res.m11 = (m00 * m22 - m20 * m02) / det;
        res.m12 = (m10 * m02 - m00 * m12) / det;
        res.m20 = (m10 * m21 - m20 * m11) / det;
        res.m21 = (m20 * m01 - m00 * m21) / det;
        res.m22 = (m00 * m11 - m10 * m01) / det;
        return res;
    }

    /**
     * Performs element wise multiplication.
     *
     * @param x factor x
     * @return element wise multiplication
     */
    public Matrix33 mulel(float x) {
        return Matrix33.create(
                m00 * x, m01 * x, m02 * x,
                m10 * x, m11 * x, m12 * x,
                m20 * x, m21 * x, m22 * x);
    }

    /**
     * Multiplies the vector as Ax.
     *
     * @param vec vector
     * @return result of Ax
     */
    public Vector3 mul(Vector3 vec) {
        float x = vec.getX() * m00 + vec.getY() * m01 + vec.getZ() * m02;
        float y = vec.getX() * m10 + vec.getY() * m11 + vec.getZ() * m12;
        float z = vec.getX() * m20 + vec.getY() * m21 + vec.getZ() * m22;
        return Vector3.create(x, y, z);
    }

    /**
     * Multiplies matrix the matrix.
     *
     * @param right matrix on the right side
     * @return result matrix
     */
    public Matrix33 mul(Matrix33 right) {
        Matrix33 res = new Matrix33();
        res.m00 = m00 * right.m00 + m01 * right.m10 + m02 * right.m20;
        res.m01 = m00 * right.m01 + m01 * right.m11 + m02 * right.m21;
        res.m02 = m00 * right.m02 + m01 * right.m12 + m02 * right.m22;
        res.m10 = m10 * right.m00 + m11 * right.m10 + m12 * right.m20;
        res.m11 = m10 * right.m01 + m11 * right.m11 + m12 * right.m21;
        res.m12 = m10 * right.m02 + m11 * right.m12 + m12 * right.m22;
        res.m20 = m20 * right.m00 + m21 * right.m10 + m22 * right.m20;
        res.m21 = m20 * right.m01 + m21 * right.m11 + m22 * right.m21;
        res.m22 = m20 * right.m02 + m21 * right.m12 + m22 * right.m22;
        return res;
    }

    /**
     * Multiplies matrix the matrix.
     *
     * @param right matrix on the right side
     * @return result matrix
     */
    public Matrix34 mul(Matrix34 right) {
        return Matrix34.create(
                m00 * right.m00() + m01 * right.m10() + m02 * right.m20(),
                m00 * right.m01() + m01 * right.m11() + m02 * right.m21(),
                m00 * right.m02() + m01 * right.m12() + m02 * right.m22(),
                m00 * right.m03() + m01 * right.m13() + m02 * right.m23(),
                m10 * right.m00() + m11 * right.m10() + m12 * right.m20(),
                m10 * right.m01() + m11 * right.m11() + m12 * right.m21(),
                m10 * right.m02() + m11 * right.m12() + m12 * right.m22(),
                m10 * right.m03() + m11 * right.m13() + m12 * right.m23(),
                m20 * right.m00() + m21 * right.m10() + m22 * right.m20(),
                m20 * right.m01() + m21 * right.m11() + m22 * right.m21(),
                m20 * right.m02() + m21 * right.m12() + m22 * right.m22(),
                m20 * right.m03() + m21 * right.m13() + m22 * right.m23()
        );
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Creates matrix.
     *
     * @param m00 first row, first column
     * @param m01 first row, second column
     * @param m02 first row, third column
     * @param m10 second row, first column
     * @param m11 second row, second column
     * @param m12 second row, third column
     * @param m20 third row, first column
     * @param m21 third row, second column
     * @param m22 third row, third column
     * @return created matrix
     */
    public static Matrix33 create(float m00, float m01, float m02, float m10, float m11, float m12, float m20, float m21, float m22) {
        Matrix33 res = new Matrix33();
        res.m00 = m00;
        res.m01 = m01;
        res.m02 = m02;
        res.m10 = m10;
        res.m11 = m11;
        res.m12 = m12;
        res.m20 = m20;
        res.m21 = m21;
        res.m22 = m22;
        return res;
    }

}
